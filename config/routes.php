<?php


use Cake\Http\Middleware\CsrfProtectionMiddleware;
use Cake\Routing\Route\DashedRoute;
use Cake\Routing\RouteBuilder;


/** @var \Cake\Routing\RouteBuilder $routes */
$routes->setRouteClass(DashedRoute::class);

$routes->scope('/', function (RouteBuilder $builder) {

    // Register scoped middleware for in scopes.
    $builder->registerMiddleware('csrf', new CsrfProtectionMiddleware([
        'httpOnly' => true,
    ]));

    /*
     * Apply a middleware to the current route scope.
     * Requires middleware to be registered through `Application::routes()` with `registerMiddleware()`
     */
    $builder->applyMiddleware('csrf');

    $builder->connect('/', ['controller' => 'Home', 'action' => 'index', 'index']);
    $builder->connect('/', ['controller' => 'Book', 'action' => 'index', 'index']);
    $builder->connect('/', ['controller' => 'Checkout', 'action' => 'index', 'index']);
    $builder->connect('/', ['controller' => 'Confirmorder', 'action' => 'index', 'index']);
    $builder->connect('/', ['controller' => 'Feedback', 'action' => 'index', 'index']);
    $builder->connect('/', ['controller' => 'Faq', 'action' => 'index', 'index']);
    $builder->connect('/', ['controller' => 'Support', 'action' => 'index', 'index']);
    $builder->connect('/', ['controller' => 'Riderjoin', 'action' => 'index', 'index']);
    $builder->connect('/', ['controller' => 'Order_not_processed', 'action' => 'index', 'index']);
    $builder->connect('/', ['controller' => 'Receivedorder', 'action' => 'index', 'index']);
    $builder->connect('/', ['controller' => 'Order', 'action' => 'index', 'index']);
    $builder->connect('/', ['controller' => 'Processedorder', 'action' => 'index', 'index']);
    $builder->connect('/', ['controller' => 'Processingorder', 'action' => 'index', 'index']);
    $builder->connect('/', ['controller' => 'Restaurants', 'action' => 'index', 'index']);
    $builder->connect('/', ['controller' => 'Termscondition', 'action' => 'index', 'index']);

 
    /*
     * ...and connect the rest of 'Pages' controller's URLs.
     */
   // $builder->connect('/pages/*', ['controller' => 'Pages', 'action' => 'display']);

   
    $builder->fallbacks();
});

