<section class="menu cid-rNDrya5QF1" once="menu" id="menu2-24">

        <nav class="navbar navbar-expand beta-menu navbar-dropdown align-items-center navbar-fixed-top navbar-toggleable-sm bg-color transparent"style="background-color: #58017D">
            <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <div class="hamburger">
                    <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                </div>
            </button>
            <div class="menu-logo">
                <div class="navbar-brand">
                    <span class="navbar-logo">
                    <a href="index.html">
                        <img src="assets/images/logo.png" alt="Homechow" class="image-media" title="" style="height: 30px;">
                    </a>
                </span>

                </div>
                <i class=""></i>
            </div>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">

                <div style="color: white;">
                    <h4 style="font-size: 18px;font-family: 'avertaregular';" id="kitchenId">
                       <a href="#features15-2u"> Street-food</a>
                    </h4>
                </div>

                <div style="color: white;">
                    <h4 style="font-size: 18px;font-family: 'avertaregular';" id="restaurantsId">
                       <a href="#features2-2f"> Restaurants</a>
                    </h4>
                </div>

                <div style="margin-left: 45px;color: white;">
                    <h4 style="font-size: 18px;font-family: 'avertaregular';" id="groupOrdersId">
                        Group orders
                    </h4>
                </div>

                <div style="margin-left: 40px;color: white;">
                    <h4 style="font-size: 18px;font-family: 'avertaregular';" id="drinksId">
                        <a href="#features2-2fs">Drinks</a>
                    </h4>
                </div>

                <div style="margin-left: 40px;color: white;">
                    <h4 style="font-size: 18px;font-family: 'avertaregular';" id="partyBookingId">
                        <a href="book-for-event.html">Events booking</a>
                    </h4>
                </div>

                <div id="newVendor" style="width: 117px;height: 35px;background-color: #1fbf58;border-radius: 25px;margin-left: 40px">
                <button style="border-style: none;color: white; background-color: #ffffff00;margin-top: 4px;margin-left: 6px;">
                    New Vendor
                </button>
                </div>

                <div class="nav-icons nav-media-ios" style="margin-left: 20px;">
                    <span style="color: white;font-size: 30px; padding-right: 4px;" class="iconify icon-app-store-ios-brands" data-icon="fa-brands:app-store" data-inline="false"></span>
                </div>

                <div class="nav-icons nav-media-google-play" style="margin-left: 15px;">
                    <span style="color: white;font-size: 30px;" class="iconify icon-app-store-ios-brands" data-icon="mdi:google-play" data-inline="false"></span>
                </div>

                <div class="nav-icons nav-media-avator" style="margin-left: 45px;">
                    <a data-toggle="modal" data-target="#modalSignUp">
                        <span style="color: white;font-size: 25px;padding-left: 2px;" class="iconify" data-icon="bx:bx-user" data-inline="false"></span>
                    </a>
                </div>

                <div class="nav-menu-media" style="margin-left: 15px;">

                    <a data-toggle="modal" data-target="#modalMenuLogin">

                        <img src="assets/images/webapp-menu-icon.svg">
                    </a>
                </div>

            </div>
        </nav>
    </section>

    <section class="features1 cid-rNDsef0Rkz mbr-fullscreen" id="features1-28">

        <div class="container">

            <h2 class="join-rider-media-div" style="font-family: 'avertabold';font-size: 40px; text-align: center;margin-top: -30px; color: #140E0F;">
               Join Homechow <br>
               As A Rider.
            </h2>

            <div class="media-container-row">

                <div class="card p-12 col-12 col-md-12 col-lg-12">
                    <div class="card-img pb-3">
                        <!-- <span class="mbri-bootstrap mbr-iconfont"></span> -->
                    </div>
                    <div class="card-box">
                        <!-- <h4 class="card-title py-3 mbr-fonts-style display-5">Enter Your<br>location</h4> -->
                        <p class="mbr-text mbr-fonts-style display-7 but-div-media" style="font-size: 20px;text-align: center;font-family: 'avertaregular';width: 720px;margin-left: 240px;padding-top: 30px; color: #707070;">
                            But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a
                        </p>

                        <button class="button-readmore-rider">
                            Read More
                        </button>

                    </div>
                </div>

            </div>

        </div>

    </section>

    <section class="features1 cid-rNDsef0Rkz mbr-fullscreen" id="features1-298">

        <div class="container">

            <div class="media-container-row" style="width: 900px;margin-left: 195px;">

                <div class="card p-4 col-12 col-md-4 col-lg-4 process-one-div-media">
                    
                    <div class="card-box" style="margin-left: -30px;width: 250px; color: #707070;">
                        <h4 class="card-title py-3 mbr-fonts-style display-5" style="font-family: 'avertabold';font-size: 25px;">Process one</h4>
                        <p class="mbr-text mbr-fonts-style display-7 p-process-div" style="font-family: 'avertaregular';font-size: 17px;color: #909090;">
                            But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to
                        </p>
                    </div>
                </div>

                <div class="card p-4 col-12 col-md-4 col-lg-4 process-two-div-media">
                    
                    <div class="card-box">
                        <h4 class="card-title py-3 mbr-fonts-style display-5" style="font-family: 'avertabold';font-size: 25px; color: #707070;">
                        Process two</h4>
                        <p class="mbr-text mbr-fonts-style display-7 p-process-div" style="font-family: 'avertaregular';font-size: 17px;color: #909090;">
                            But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to
                        </p>
                    </div>
                </div>

                <div class="card p-4 col-12 col-md-4 col-lg-4 process-three-div-media">
                    <div class="card-img pb-3">
                    </div>
                    <div class="card-box" style="margin-left: 5px;width: 250px;">
                        <h4 class="card-title py-3 mbr-fonts-style display-5" style="font-family: 'avertabold';font-size: 25px;color: #707070;">
                        Process three</h4>
                        <p class="mbr-text mbr-fonts-style display-7 p-process-div" style="font-family: 'avertaregular';font-size: 17px;color: #909090;">
                            But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to
                        </p>
                    </div>
                </div>

            </div>

        </div>

    </section>

    <section class="features1 cid-rNDsef0Rkz mbr-fullscreen" id="features1-218">

        <div class="container">

            <h2 class="all-div-media" style="font-family: 'avertabold';font-size: 80px; text-align: center;margin-top: -10px; color: #000000;">
               “All I have to do is <br>
                ride my bike & get <br>
                 paid.”
            </h2>

            <div class="media-container-row">

                <div class="card p-12 col-12 col-md-12 col-lg-12">
                    <div class="card-img pb-3">
                        <!-- <span class="mbri-bootstrap mbr-iconfont"></span> -->
                    </div>
                    <div class="card-box">
                        <!-- <h4 class="card-title py-3 mbr-fonts-style display-5">Enter Your<br>location</h4> -->
                        <p class="mbr-text mbr-fonts-style display-7 name-div-media1" style="font-size: 20px;text-align: center;font-family: 'avertaregular';width: 720px;margin-left: 180px;padding-top: 30px;color: #140E0F;">
                            Mohammed
                        </p>

                    </div>
                </div>

            </div>

        </div>

    </section>

    <section class="features11 cid-rNDtFNiRhT mbr-fullscreen" id="features11-21i" style="padding-top: 200px;">

        <div class="container">
            <div class="col-md-12">

                <div class="background-color-media-div" style="height: 400px;width: 400px;background-color: #EFEFEF;margin-left: 560px;margin-bottom: -464px;">
                </div>

                <div class="media-container-row">
                    <div class="mbr-figure m-auto" style="width: 45%;">
                        <img class="background-color-div-media" src="assets/images/rider-image.png" alt="Mobirise" title="" style="margin-left: -170px;">
                    </div>
                    <div class="align-left aside-content" style="margin-left: 50px;">
                        <h2 class="mbr-title  mbr-fonts-style display-2  h2-requirement-media" style="color: #707070;font-size: 25px;font-family: 'avertabold';">

                        Requirements

                    </h2>

                        <div class="block-content">
                            <div class="card">

                                <div class="card-box requirements-div-media">
                                    <p class="block-text mbr-fonts-style display-7" style="font-size: 25px;margin-top: -130px;font-family: 'avertaregular';line-height: 2;">
                                        <br>
                                        <br>
                                        <br> But I must explain to you how
                                        <br> But I must explain to you how
                                        <br> But I must explain to you how
                                        <br> But I must explain to you how
                                        <br> But I must explain to you how
                                        <br> But I must explain to you how
                                        <br> But I must explain to you how
                                    </p>
                                </div>

                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="are-ready-to-join-media" class="features1 cid-rNDsef0Rkz mbr-fullscreen" id="features1-28" style="padding-top: 180px">

        <div class="container">

            <h2 class="are-media-div" style="font-family: 'avertabold';font-size: 25px; text-align: center;margin-top: -30px; color: #000000;">
               Are you ready to join us? <br>
            Fill the form below and we will get back to you.
            </h2>

            <div class="form-join-media-div" style="width: 470px;margin-left: 290px;">

            <form action="#" >

                    <label style="margin-left: 55px;color: #707070;margin-top: 20px;">Name</label>

                    <div class="form-row" style="margin-left: 40px;margin-right: 40px;">
                        <div class="col">
                            <input type="text" class="form-control" style="border-radius: 10px;border-color: black;margin-right: 40px;"  name="email">
                        </div>

                    </div>

                    <label style="margin-left: 55px;color: #707070;margin-top: 20px;">Mobile number</label>

                    <div class="form-row" style="margin-left: 40px;margin-right: 40px;">
                        <div class="col">
                            <input type="text" class="form-control" style="border-radius: 10px;border-color: black;margin-right: 40px;"  name="phone">
                        </div>

                    </div>

                    <label style="margin-left: 55px;color: #707070;margin-top: 20px;">Email</label>

                    <div class="form-row" style="margin-left: 40px;margin-right: 40px;">
                        <div class="col">
                            <input type="text" class="form-control" style="border-radius: 10px;border-color: black;margin-right: 40px;"  name="email">
                        </div>

                    </div>

                    <label style="margin-left: 55px;color: #707070;margin-top: 20px;">Location</label>

                    <div class="form-row" style="margin-left: 40px;margin-right: 40px;">
                        <div class="col">
                            <select type="text" class="form-control" style="border-radius: 10px; border-color: black; margin-right: 40px;"  name="location">
                            <option style="color: #707070">Nairobi</option>
                            </select>
                        </div>

                    </div>
                    

                    
                    <div class="rider-button-submit" style="margin-left: 170px;padding-bottom: 10px;padding-top: 10px;">
                        <button type="button" class="btn btn-default button-continue" data-toggle="modal" data-target="#modalSendRequest">Submit</button>
                    </div>

                    

                </form>
        </div>

        </div>

    </section>

   <section class="features11 cid-rNDtFNiRhT mbr-fullscreen" id="features11-2i" style="background-color: #F6F6F6;">

        <div class="container">
            <div class="col-md-12">
                <div class="media-container-row">
                    <div class="mbr-figure m-auto phone-mockup-media" style="width: 45%;">
                        <img src="assets/images/Phone mockups.png" alt="Mobirise" title="" style="margin-top: -50px;">
                    </div>
                    <div class=" align-left aside-content the-best-food-media" style="margin-left: 160px;">
                        <h2 class="mbr-title  mbr-fonts-style display-2 heading-best-food-media" style="color: #58017D;" style="font-size: 60px;">
                         <strong style="font-family: 'avertaregular';">
                        The Best Food <br>
                        Delivery App
                        </strong>
                    </h2>

                        <div class="block-content">
                            <div class="card">

                                <div class="card-box good-food-div-media" style="width: 460px;">
                                    <div >
                                    <p style="font-family: 'avertaregular';" class="block-text mbr-fonts-style display-7" style="font-size: 25px;margin-top: -80px;">
                                        <br>
                                        <br>
                                        <br> 
                                        Good food is meant to be shared and you can order from your favorite restaurant and enjoy tasty meals on the go using Homechow.
                                    </p>
                                    </div>
                                </div>

                                <img class="google-play-media" src="assets/images/google-play.png" style="width: 140px;">
                                <img class="apple-store-media" src="assets/images/apple-store.png" style="width: 140px;margin-left: 260px;margin-top: -51px;">
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="cid-rNDtMJWRWX" id="footer1-2j">

        <div class="container">
            <div class="media-container-row content text-white">
                <div class="col-12 col-md-2 mbr-fonts-style display-7">
                    <h5 style="font-family: 'avertaregular';" class="pb-2">
                        <br>
                    Support
                      </h5>
                    <p style="font-family: 'avertaregular';" class="mbr-text">
                        FAQ
                        <br>Help & Support
                        <br>Ride with us
                        <br> Careers
                        <br>
                        <br>
                    </p>
                    <h5 style="font-family: 'avertaregular';padding-top: 50px;">
                        Legal
                        <br>
                    </h5>
                    <p style="font-family: 'avertaregular';" class="mbr-text">
                        <br>Terms & Condition
                        <br>Privacy Policy
                        <br>RVendor Agreement
                    </p>
                </div>
                <div class="col-12 col-md-2 mbr-fonts-style display-7">
                    <h5 style="font-family: 'avertaregular';" class="pb-2">
                        <br>
                    Services
                </h5>
                    <p style="font-family: 'avertaregular';" class="mbr-text">
                        Booking
                        <br>Kitchen
                        <br>Marketplace
                        <br>Drinks
                    </p>

                    <h4 style="font-family: 'avertaregular';" class="comming-soon soon-media-market-place"> soon </h4>
                </div>
                <div class="col-12 col-md-2 mbr-fonts-style display-7">
                    <h5 style="font-family: 'avertaregular';" class="pb-2">
                    Popular<br>
                    categories
                </h5>
                    <p style="font-family: 'avertaregular';" class="mbr-text">
                        African
                        <br>Local
                        <br>Continental
                        <br>Lunch
                        <br>Breakfast
                        <br>Italian
                        <br>Drinks
                        <br>Fast food
                    </p>

                </div>
                <div class="col-12 col-md-2 mbr-fonts-style display-7">
                    <h5 style="font-family: 'avertaregular';" class="pb-2">
                    Operating
                    Countries
                </h5>
                    <p class="mbr-text" style="font-family: 'avertaregular';color: white;">
                        Ghana
                        <ui style="margin-top: -8px;">
                            <li class="ul-dash" style="font-family: 'avertaregular';padding-bottom: 7px;padding-left: 10px;"> Accra </li>
                            <li class="ul-dash" style="font-family: 'avertaregular';padding-bottom: 7px;padding-left: 10px;"> Kumasi </li>
                            <li class="ul-dash" style="font-family: 'avertaregular';padding-bottom: 7px;padding-left: 10px;">Takoradi </li>
                            <li class="ul-dash" style="font-family: 'avertaregular';padding-bottom: 7px;padding-left: 10px;">Tomale </li>
                            <!-- <li class="ul-dash">Cape Coast</li> -->
                            <li class="ul-dash" style="font-family: 'avertaregular';padding-bottom: 7px;padding-left: 10px;">Koforidua</li>
                        </ui>

                        <p class="mbr-text" style="font-family: 'avertaregular';margin-top: 54px;color: white;">
                            Nigeria
                            <ui>
                                <li class="ul-dash" style="font-family: 'avertaregular';margin-top: -10px;margin-left: 10px;"> Lagos </li>
                            </ui>
                            <h4 class="soon-media-1" style="font-family: 'avertaregular';font-size: 7px;background-color: #c56ac5;color: black;height: 16px;padding: 4px;width: 24px;text-align: center;border-radius: 50px;margin-left: 52px;margin-top: -47px;"> soon </h4>
                        </p>
                    </p>
                    <div style="margin-top: -165px;margin-left: -11px;">
                        <h4 class="soon-media-2" style="font-family: 'avertaregular';font-size: 7px;margin-left: 90px;background-color: #c56ac5;color: black;height: 16px;padding: 4px;width: 24px;text-align: center;border-radius: 50px;"> soon </h4>
                    </div>

                    <div style="margin-top: 10px;margin-left: -20px;">

                        <h4 class="soon-media-3" style="font-family: 'avertaregular';font-size: 7px;margin-left: 90px;background-color: #c56ac5;color: black;height: 16px;padding: 4px;width: 24px;text-align: center;border-radius: 50px;"> soon </h4>
                    </div>
                    <div style="margin-top: 10px;margin-left: -6px;">
                        <h4 class="soon-media-4" style="font-family: 'avertaregular';font-size: 7px;margin-left: 90px;background-color: #c56ac5;color: black;height: 16px;padding: 4px;width: 24px;text-align: center;border-radius: 50px;"> soon </h4>
                    </div>
                    <!-- <div style="margin-top: 12px;">
                        <h4 style="font-size: 7px;margin-left: 90px;background-color: #c56ac5;color: black;height: 16px;padding: 4px;width: 24px;text-align: center;border-radius: 50px;"> soon </h4>
                    </div> -->

                </div>
                <div class="col-12 col-md-2 mbr-fonts-style display-7">
                    <h5 class="pb-2" style="padding-top: 45px;">
                    <!-- Services -->
                      </h5>
                    <p class="mbr-text kenya-media" style="font-family: 'avertaregular';color: white;">
                        Kenya
                        <ui>
                            <li class="ul-dash" style="font-family: 'avertaregular';margin-top: -10px;margin-left: 5px;"> Nairobi </li>
                            <!-- <li class="ul-dash"> Mombasa </li> -->
                        </ui>
                        <!-- <h4 style="font-size: 8px;background-color: #c56ac5;width: 30px;text-align: center;border-radius: 50px;margin-left: 80px;margin-top: -13px;"> soon </h4> -->
                    </p>

                    <p class="mbr-text" style="font-family: 'avertaregular';margin-top: 30px;color: white;">
                        Ethiopia
                        <ui>
                            <li class="ul-dash" style="font-family: 'avertaregular';margin-top: -10px;margin-left: 5px;"> Adis Ababa </li>
                            <!-- <li class="ul-dash"> Mombasa </li> -->
                        </ui>
                        <h4 class="soon-media-5" style="font-family: 'avertaregular';font-size: 7px;background-color: #c56ac5;width: 24px;text-align: center;border-radius: 50px;margin-left: 55px;margin-top: -46px;height: 16px;padding: 4px;color: black;"> soon </h4>
                    </p>

                    <!-- <div style="font-size: 7px;background-color: #c56ac5;width: 24px;text-align: center;border-radius: 50px;margin-left: 61px;margin-top: -46px;height: 16px;padding: 4px;color: black;"></div> -->

                    <p class="mbr-text" style="font-family: 'avertaregular';margin-top: 55px;color: white;">
                        Rwanda
                        <ui>
                            <li class="ul-dash" style="font-family: 'avertaregular';margin-top: -10px;margin-left: 10px;"> Kigali </li>
                        </ui>
                        <h4 class="soon-media-6" style="font-family: 'avertaregular';font-size: 7px;background-color: #c56ac5;width: 24px;text-align: center;border-radius: 50px;margin-left: 55px;margin-top: -47px;color: black;height: 16px;padding: 4px;"> soon </h4>
                    </p>
                    <p class="mbr-text" style="font-family: 'avertaregular';margin-top: 54px;color: white;">
                        Uganda
                        <ui>
                            <li class="ul-dash" style="font-family: 'avertaregular';margin-top: -10px;margin-left: 10px;"> Kampala </li>
                        </ui>
                        <h4 class="soon-media-7" style="font-family: 'avertaregular';font-size: 7px;background-color: #c56ac5;color: black;height: 16px;padding: 4px;width: 24px;text-align: center;border-radius: 50px;margin-left: 52px;margin-top: -47px;"> soon </h4>
                    </p>
                </div>
            </div>
            <div class="footer-lower">
                <!-- <div class="media-container-row">
                    <div class="col-sm-12">
                        <hr>
                    </div>
                </div> -->
                <div class="media-container-row mbr-white">
                    <!-- <div class="col-sm-6 copyright">
                        <p class="mbr-text mbr-fonts-style display-7"></p>
                    </div> -->
                    <div>
                        <div class="social-list align-center">
                            <div class="soc-item">
                                <!-- <a href="" target="_blank"> -->
                                <span class="fa fa-twitter socicon mbr-iconfont mbr-iconfont-social"></span>
                                <!-- </a> -->
                            </div>
                            <div class="soc-item">
                                <!--  <a href="" target="_blank"> -->
                                <span class="fa fa-instagram socicon mbr-iconfont mbr-iconfont-social"></span>

                                <!-- </a> -->
                            </div>
                            <div class="soc-item">
                                <!-- <a href="" target="_blank"> -->
                                <span class="fa fa-facebook-square socicon mbr-iconfont mbr-iconfont-social"></span>
                                <!-- </a> -->
                            </div>
                            <div class="soc-item">
                                <!-- <a href="" target="_blank"> -->
                                <span class="fa fa-linkedin-square socicon mbr-iconfont mbr-iconfont-social"></span>
                                <!-- </a> -->
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section once="footers" class="cid-rNDtRVzGUV" id="footer6-2k">
        <div class="media-container-row">
            <div class="col-sm-12">
                <hr style="border-top: 1px solid #8ec8fb38;" class="hr-bottom-media">
            </div>
        </div>

        <div class="container">

            <div class="media-container-row align-center mbr-white">
                <!-- <div class="col-12"> -->
                <p class="mbr-text mb-0 mbr-fonts-style display-7" style="font-family: 'avertaregular';color: white;">
                    (C) 2020 All Rights Reserved <a href="http://homechow.io/"> Homechow.io </a> App Ltd.
                </p>
                <!-- </div> -->
            </div>
        </div>
    </section>

    <!-- Modal -->
    <div class="modal fade" id="modalSignUp" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content" style="border-radius: 20px;">
                <div class="modal-header">
                    <button type="button" class="close" style="margin-left: 10px;" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title" style="margin-right: 190px;font-size: 20px;">Sign up</h4>
                </div>
                <form action="#">

                    <label style="margin-left: 55px;color: #707070;margin-top: 20px;">Name</label>

                    <div class="form-row" style="margin-left: 40px;margin-right: 40px;">
                        <div class="col">
                            <input type="text" class="form-control" style="border-radius: 10px;margin-right: 40px;" placeholder="Enter first name" name="email">
                        </div>

                    </div>

                    <label style="margin-left: 55px;color: #707070;margin-top: 20px;">Email</label>

                    <div class="form-row" style="margin-left: 40px;margin-right: 40px;">
                        <div class="col">
                            <input type="text" class="form-control" style="border-radius: 10px;margin-right: 40px;" placeholder="Email" name="email">
                        </div>

                    </div>
                    <label style="margin-left: 55px;color: #707070;margin-top: 20px;">Mobile number</label>

                    <div class="form-row" style="margin-left: 40px;margin-right: 40px;">
                        <div class="col">
                            <input type="text" class="form-control" style="border-radius: 10px;margin-right: 40px;" placeholder="Mobile number" name="email">
                        </div>

                    </div>
                    <div style="margin-left: 170px;padding-bottom: 10px;padding-top: 10px;">
                        <button type="button" class="btn btn-default button-continue">Continue</button>
                    </div>

                    <div>
                        <h3 class="have-an-account">
                            Already have an account? <a style="color: #58017D;" data-toggle="modal" data-dismiss="modal" data-target="#modalLogin"> <strong> Login </strong></a>
                        </h3>
                    </div>

                </form>

                <div>
                    <hr>
                    <h3 class="have-an-account">or sign up using Social media</h3>
                    <div style="margin-left: 190px;margin-bottom: 30px;">
                        <span class="iconify" data-icon="bx:bxl-facebook-circle" style="font-size: 30px;color: #5757ec;" data-inline="false"></span>
                        <span class="iconify" data-icon="ant-design:google-circle-filled" style="font-size: 30px; color: red;" data-inline="false"></span>
                        <span class="iconify" data-icon="ant-design:twitter-circle-filled" style="font-size: 30px;color: #3a3af7cf;" data-inline="false"></span>
                    </div>
                </div>

                <div style="padding-bottom: 10px;">
                    <h3 class="have-an-account" style="text-align: center;margin-left: -10px;padding-bottom: 10px;">
                        By continuing, you agree to our<br>
                        <a style="color: #58017D;" href="Terms & Conditions.html"> <strong> Terms of use,</strong></a>
                        <a style="color: #58017D;" href="Terms & Conditions.html"> <strong> Privacy policy </strong></a> and 
                        <a style="color: #58017D;"> <strong> Vendor </strong></a>
                    </h3>
                </div>

            </div>

        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="modalLogin" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content" style="border-radius: 20px;">
                <div class="modal-header">
                    <button type="button" class="close" style="margin-left: 10px;" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title" style="margin-right: 190px;font-size: 20px;">Log in</h4>
                </div>
                <form action="#">

                    <label style="margin-left: 55px;color: #707070;margin-top: 20px;">Phone number or email</label>

                    <div class="form-row" style="margin-left: 40px;margin-right: 40px;">
                        <div class="col">
                            <input type="text" class="form-control" style="border-radius: 10px;margin-right: 40px;" placeholder="Enter Phone numer or email here...." name="email">
                        </div>

                    </div>

                    <label style="margin-left: 55px;color: #707070;margin-top: 20px;">Password</label>

                    <div class="form-row" style="margin-left: 40px;margin-right: 40px;">
                        <div class="col">
                            <input type="password" class="form-control" style="border-radius: 10px;margin-right: 40px;" placeholder="" name="email">
                        </div>

                    </div>
                    <div style="margin-left: 170px;padding-bottom: 10px;padding-top: 10px;">
                        <button type="button" class="btn btn-default button-continue">Log in</button>
                    </div>

                    <div>
                        <h3 class="have-an-account">
                            <a style="color: #58017D;"> <strong> Forgot Password </strong></a>
                        </h3>
                    </div>
                    <div>
                        <h3 class="have-an-account">
                            Don't have an account <a style="color: #58017D;" data-toggle="modal" data-dismiss="modal" data-target="#modalSignUp" > <strong> Sign Up </strong></a>
                        </h3>
                    </div>

                </form>

            </div>

        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="modalMenuLogin" role="dialog">
        <div class="modal-dialog modal-sm">

            <!-- Modal content-->
            <div class="modal-content" style="border-radius: 19px;width: 350px;">

                <div class="row" style="margin-top: 35px;margin-left: 10px;">

                    <div class="col-sm-6">
                        <h4 style="font-size: 15px;">
                    Hope Adoli
                   </h4>
                        <h4 style="font-size: 10px;">
                    0242267686
                   </h4>

                        <h4 style="font-size: 10px;">
                    kafui@loudrsocio.co
                   </h4>
                    </div>

                    <div class="col-sm-6">
                        <button class="menu-button-edit" style="margin-left: 33px;margin-top: 10px;padding-bottom: 10px;padding-right: 10px;    padding-top: 7px;width: 65px;font-size: 13px;">
                            Edit
                        </button>
                    </div>

                </div>

                <div class="row current-balance" style="margin-top: 15px;margin-left: 25px;">

                    <div class="col-sm-6">
                        <h4 style="font-size: 15px;">
                    Current Balance
                   </h4>
                        <h4 style="font-size: 10px;color: black;">
                    <strong> GHC 200.46</strong>
                   </h4>

                    </div>

                    <div class="col-sm-6">
                        <button class="menu-button-edit" style="margin-left: 40px;padding-left: 10px;padding-bottom: 10px;padding-right: 10px;padding-top: 8px;font-size: 13px;">
                            Top up
                        </button>
                    </div>

                </div>

                <div class="row" style="
                margin-top: 15px;
                margin-left: 10px;">

                    <div class="col-sm-6">
                        <i class="fa fa-heart-o"></i>

                    </div>

                    <div class="col-sm-6">
                        <h4 style="font-size: 15px;margin-left: -130px;">
                          Refer a friend
                             </h4>
                    </div>

                    <hr style="width: 350px;margin-left: 10px;">

                </div>

                <div class="row" style="
                margin-top: 15px;
                margin-left: 10px;">

                    <div class="col-sm-6">
                        <span class="iconify" data-icon="uil:cart" data-inline="false"></span>

                    </div>

                    <div class="col-sm-3">
                        <h4 style="font-size: 15px;margin-left: -130px;">
                          All Orders
                             </h4>

                    </div>
                    <div class="col-sm-3">
                        <h4 class="button-all-orders"> 2 </h4>
                    </div>

                    <hr style="width: 350px;margin-left: 10px;">

                </div>

                <div class="row" style="
                margin-top: 15px;
                margin-left: 10px;">

                    <div class="col-sm-6">
                        <span class="iconify" data-icon="dashicons:star-empty" data-inline="false"></span>

                    </div>

                    <div class="col-sm-6">
                        <h4 style="font-size: 15px;margin-left: -130px;">
                          My Rating & reviews
                             </h4>
                    </div>

                    <hr style="width: 350px;margin-left: 10px;">

                </div>

                <div class="row" style="
                 margin-top: 15px;
                 margin-left: 10px;">

                    <div class="col-sm-6">
                        <span class="iconify" data-icon="fa-regular:comment" data-inline="false"></span>

                    </div>

                    <div class="col-sm-6">
                        <h4 style="font-size: 15px;margin-left: -130px;">
                          <a href="help-and-support.html">Customer support</a>
                             </h4>
                    </div>

                    <hr style="width: 350px;margin-left: 10px;">

                </div>

                <div class="row" style="
                 margin-top: 15px;
                 margin-left: 10px;padding-bottom: 20px;">

                    <div class="col-sm-6">
                        <span class="iconify" data-icon="bytesize:settings" data-inline="false"></span>

                    </div>

                    <div class="col-sm-6">
                        <h4 style="font-size: 15px;margin-left: -130px;">
                          Setting
                             </h4>
                    </div>

                    <!-- <hr style="width: 350px;margin-left: 10px;"> -->

                </div>

            </div>

            <div>
                <button type="button" class="close x-button-floating" style="background-color: white;" data-dismiss="modal">&times;</button>
            </div>

        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="modalSendRequest" role="dialog" style="overflow-y: scroll;">
        <div class="modal-dialog ">

            <!-- Modal content-->
            <div class="modal-content" style="margin-top: 190px;">

                <div class="send-div-media" style="width: 460px;text-align: center;padding-top: 50px;padding-left: 50px;padding-right: 20px;font-family: 'avertaregular';padding-bottom: 30px;">
                <h4 style="font-size: 20px;">
                    Your form was successfully submitted. We will reach out to you soon. Thank you.
                </div>

                    <div>
                <button data-dismiss="modal"  style="background-color: white;border-color: white;margin-left: 220px;padding-bottom: 24px;">
                    Ok
                </button>
                </div>
            </div>

        </div>

    </div>

    <!-- Modal -->
    <div class="modal fade" id="modalMenuNotLogin" role="dialog">
        <div class="modal-dialog modal-sm">

            <!-- Modal content-->
            <div class="modal-content" style="border-radius: 19px;width: 350px;">

                <div class="row" style="margin-top: 35px;margin-left: 10px;">

                    <button style="color: white;background-color: #58017D;width: 90px;height: 40px;border-radius: 50px;margin-left: 100px;font-size: 12px;" data-toggle="modal" data-dismiss="modal" data-target="#modalSignUp">
                        Sign up
                    </button>

                    <hr style="width: 350px;margin-left: 10px;">

                </div>

                <div class="row" style="
                margin-top: 15px;
                margin-left: 10px;">

                    <div class="col-sm-6">
                        <i class="fa fa-heart-o"></i>

                    </div>

                    <div class="col-sm-6">
                        <h4 style="font-size: 15px;margin-left: -130px;">
                          Refer a friend
                             </h4>
                    </div>

                    <hr style="width: 350px;margin-left: 10px;">

                </div>

                <div class="row" style="
                margin-top: 15px;
                margin-left: 10px;">

                    <div class="col-sm-6">
                        <span class="iconify" data-icon="uil:cart" data-inline="false"></span>

                    </div>

                    <div class="col-sm-3">
                        <h4 style="font-size: 15px;margin-left: -130px;">
                          All Orders
                             </h4>

                    </div>
                    <div class="col-sm-3">
                        <h4 class="button-all-orders"> 2 </h4>
                    </div>

                    <hr style="width: 350px;margin-left: 10px;">

                </div>

                <div class="row" style="
                margin-top: 15px;
                margin-left: 10px;">

                    <div class="col-sm-6">
                        <span class="iconify" data-icon="dashicons:star-empty" data-inline="false"></span>

                    </div>

                    <div class="col-sm-6">
                        <h4 style="font-size: 15px;margin-left: -130px;">
                          My Rating & reviews
                             </h4>
                    </div>

                    <hr style="width: 350px;margin-left: 10px;">

                </div>

                <div class="row" style="
                 margin-top: 15px;
                 margin-left: 10px;">

                    <div class="col-sm-6">
                        <span class="iconify" data-icon="fa-regular:comment" data-inline="false"></span>

                    </div>

                    <div class="col-sm-6">
                        <h4 style="font-size: 15px;margin-left: -130px;">
                          <a style="color: #000000" href="help-and-support.html">Customer support</a>
                             </h4>
                    </div>

                    <hr style="width: 350px;margin-left: 10px;">

                </div>

                <div class="row" style="
                 margin-top: 15px;
                 margin-left: 10px;padding-bottom: 20px;">

                    <div class="col-sm-6">
                        <span class="iconify" data-icon="bytesize:settings" data-inline="false"></span>

                    </div>

                    <div class="col-sm-6">
                        <h4 style="font-size: 15px;margin-left: -130px;">
                          Setting
                             </h4>
                    </div>

                    <!-- <hr style="width: 350px;margin-left: 10px;"> -->

                </div>

            </div>

            <div>
                <button type="button" class="close x-button-floating" style="background-color: white;" data-dismiss="modal">&times;</button>
            </div>

        </div>
    </div>