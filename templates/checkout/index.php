<section class="menu cid-rNDrya5QF1" once="menu" id="menu2-24">

        <nav class="navbar navbar-expand beta-menu navbar-dropdown align-items-center navbar-fixed-top navbar-toggleable-sm bg-color transparent"style="background-color: #58017D">
            <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <div class="hamburger">
                    <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                </div>
            </button>
            <div class="menu-logo">
                <div class="navbar-brand">
                    <span class="navbar-logo">
                    <a href="index.html">
                        <img src="assets/images/logo.png" alt="Homechow" class="image-media" title="" style="height: 30px;">
                    </a>
                </span>

                </div>
                <i class=""></i>
            </div>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">

                <div style="color: white;">
                    <h4 style="font-size: 18px;font-family: 'avertaregular';" id="kitchenId">
                       <a href="#features15-2u"> Street-food</a>
                    </h4>
                </div>

                <div style="color: white;">
                    <h4 style="font-size: 18px;font-family: 'avertaregular';" id="restaurantsId">
                       <a href="#features2-2f"> Restaurants</a>
                    </h4>
                </div>

                <div style="margin-left: 45px;color: white;">
                    <h4 style="font-size: 18px;font-family: 'avertaregular';" id="groupOrdersId">
                        Group orders
                    </h4>
                </div>

                <div style="margin-left: 40px;color: white;">
                    <h4 style="font-size: 18px;font-family: 'avertaregular';" id="drinksId">
                        <a href="#features2-2fs">Drinks</a>
                    </h4>
                </div>

                <div style="margin-left: 40px;color: white;">
                    <h4 style="font-size: 18px;font-family: 'avertaregular';" id="partyBookingId">
                        <a href="book-for-event.html">Events booking</a>
                    </h4>
                </div>

                <div id="newVendor" style="width: 117px;height: 35px;background-color: #1fbf58;border-radius: 25px;margin-left: 40px">
                <button style="border-style: none;color: white; background-color: #ffffff00;margin-top: 4px;margin-left: 6px;">
                    New Vendor
                </button>
                </div>

                <div class="nav-icons nav-media-ios" style="margin-left: 20px;">
                    <span style="color: white;font-size: 30px; padding-right: 4px;" class="iconify icon-app-store-ios-brands" data-icon="fa-brands:app-store" data-inline="false"></span>
                </div>

                <div class="nav-icons nav-media-google-play" style="margin-left: 15px;">
                    <span style="color: white;font-size: 30px;" class="iconify icon-app-store-ios-brands" data-icon="mdi:google-play" data-inline="false"></span>
                </div>

                <div class="nav-icons nav-media-avator" style="margin-left: 45px;">
                    <a data-toggle="modal" data-target="#modalSignUp">
                        <span style="color: white;font-size: 25px;padding-left: 2px;" class="iconify" data-icon="bx:bx-user" data-inline="false"></span>
                    </a>
                </div>

                <div class="nav-menu-media" style="margin-left: 15px;">

                    <a data-toggle="modal" data-target="#modalMenuLogin">

                        <img src="assets/images/webapp-menu-icon.svg">
                    </a>
                </div>

            </div>
        </nav>
    </section>

    <div class="container navbar-dropdowns  navbar-fixed-tops" style="margin-top: 120px;" id="busket-view">

        <!--  <div class="floating-busket-counter">

            <h4 style="color: white;font-size: 10px;padding-left: 3px;">
                    2
                </h4>

        </div> -->

        <div class="commucation-button">
            <img src="assets/images/message-circle.svg" class="communication-img" style=" margin-top: 0px;">
        </div>

    </div>

    <section class="section-order-div" style="margin-top: 150px;background-color: white;margin-left: 180px;margin-bottom: 60px;">
        <div style="background-color: #EBEBEB;width: 200px;height: 35px;border-radius: 50px;">

            <img src="assets/images/arrow-left.svg" style="margin-left: 18px;margin-top: 10px;">

            <h4 style="font-family: 'avertaregular';font-size: 15px;margin-left: 50px;margin-top: -17px;">Go back to basket</h4>

        </div>

        <div style="margin-top: 40px;">

            <h4 style="font-family: 'avertaregular';font-size: 60px">
            Checkout
        </h4>

            <h4 style="font-family: 'avertaregular';font-size: 30px;margin-top: 45px;">
            GHC 143.00
        </h4>

        </div>

        <hr style="width: 1100px;margin-left: 3px;margin-top: 30px;">

        <div>

            <h4 style="font-family: 'avertaregular';font-size: 20px">
            Delivery location
        </h4>


            <h4 style="font-family: 'avertaregular';color: #58017D;font-size: 15px;margin-left: 280px;margin-top: -25px;">
            <a data-toggle="modal" data-dismiss="modal" data-target="#modalChangeDirection" > Change</a>
        </h4>
        </div>

        <div style="border-color: #EEEAEA;width: 275px;height: 100px;border-style: solid;border-width: 1px;margin-top: 25px;border-radius: 15px;">

            <h4 style="font-family: 'avertaregular';font-size: 15px;margin-left: 10px;margin-top: 15px;">
            Mosque - Kumasi Ghana
        </h4>

            <h5 style="font-family: 'avertaregular';font-size: 15px;margin-left: 10px;margin-top: 10px;">
            Akornor Street, 32
        </h5>

            <h5 style="font-family: 'avertaregular';font-size: 15px;margin-left: 10px;margin-top: 10px;">
            opposite Akonor Government hospital
        </h5>

        </div>

        <div style="margin-top: 29px;">

            <h4 style="font-family: 'avertaregular';font-size: 17px">
            Select payment method
        </h4>

        </div>

        <div class="homechow-wallet-div-1" style="border-color: #EEEAEA;width: 130px;height: 95px;border-style: solid;border-width: 1px;border-radius: 20px;margin-top: 25px;">
            <h5 style="font-family: 'avertaregular';font-size: 20px;margin-left: 10px;margin-top: 10px;">
            Homechow wallet
        </h5>
            <h5 style="font-family: 'avertaregular';font-size: 20px;margin-top: 15px;margin-left: 10px;">
            GHC 143
        </h5>
        </div>

        <div class="homechow-wallet-div-2" style="border-color: #EEEAEA;width: 130px;height: 95px;border-style: solid;border-width: 1px;border-radius: 20px;margin-top: -95px;margin-left: 145px;">
            <h5 style="font-family: 'avertaregular';font-size: 20px;margin-left: 10px;margin-top: 10px;">
            Homechow wallet
        </h5>
            <h5 style="font-family: 'avertaregular';font-size: 20px;margin-top: 15px;margin-left: 10px;">
            GHC 143
        </h5>
        </div>

        <div class="homechow-wallet-div-3" style="border-color: #58017D;background-color: #EFDFF6;width: 130px;height: 95px;border-style: solid;border-width: 1px;border-radius: 20px;margin-top: -95px;margin-left: 290px;">
            <label class="container" style="font-family: 'avertaregular';margin-left: 10px;font-size: 15px;color: #58017D;margin-top: 10px;">Cash on Delivery
                <input type="checkbox" checked="checked">
                <span class="checkmark"></span>
            </label>
            <h5 style="font-family: 'avertaregular';font-size: 18px;margin-top: -4px;color: #58017D;margin-left: 43px;">
            GHC 154
        </h5>
        </div>

        <div style="margin-top: 35px;margin-left: 4px;">
            <h5 style="font-family: 'avertaregular';font-size: 17px">
            Ordering for someone
        </h5>

        </div>

        <div style="margin-left: 360px;margin-top: -44px;">
            <label class="switch">
                <input type="checkbox">
                <span class="slider round slide-round" style="width: 55px;"></span>
            </label>

            </div>

            <div style="margin-top: 30px">
                
                <input type="text" name="search" placeholder="Enter person’s number"style="border-color: #EEEAEA;border-radius: 25px;padding-left: 24px;width: 431px;background: #FFFFFF;font-size: 14px" required="required" class="form-control display-7 enter-personal-number-div">
            </div>

            

            <div>
                <label style="font-family: 'avertaregular';margin-left: 3px;color: #707070;margin-top: 27px;margin-bottom: 20px;">When should the delivery be made?</label>

                    <div class="form-row" style="margin-left: -2px;margin-right: 40px;">
                        <div class="col">
                            <select type="text" class="form-control enter-personal-number-div" style="border-radius: 10px;border-color: #EEEAEA;margin-right: 40px;width: 431px;padding-left: 23px;font-size: 14px" name="location">
                            <option style="font-family: 'avertaregular';color: #000000">As soon as possible</option>
                            </select>
                        </div>

                    </div>
            </div>

            <div>
                <label style="font-family: 'avertaregular';margin-left: 3px;color: #000000;margin-top: 27px;margin-bottom: 20px;"> Additional notes</label>

                    <div class="form-row" style="margin-left: -2px;margin-right: 40px;">
                        <div class="col">
                            <textarea class="enter-personal-number-div" textarea rows="3" cols="50" style="font-family: 'avertaregular';border-radius: 25px;color: #707070;border-color: #ECECEC;font-size: 16px;width: 435px;padding-left: 18px;padding-top: 15px;padding-right: 46px;font-size: 20px"> Please knock on the door or give it to the security he'll pay you. Thanks.</textarea> 
                        </div>

                    </div>
            </div>

            <!-- <div style="width: 100px;height: 40px;background-color: #58017D;border-radius: 25px;margin-top: 25px;">
                
                <button   style="font-family: 'avertaregular';background-color: #ffffff00;border-color: #ffffff00;color: white;margin-left: 5px;margin-top: 5px;font-size: 15px;">
                    <a href="order-received.html">Place order</a>
                </button>
            </div> -->
        

    </section>

    

   

    <!-- Modal -->
    <div class="modal fade" id="modalPasswordSetUp" role="dialog" style="overflow-y: scroll;">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content" style="border-radius: 20px;">
                <div class="modal-header">
                    <!-- <button type="button" class="close" style="margin-left: 10px;" data-dismiss="modal">&times;</button> -->
                    <h4 class="modal-title" style="font-size: 20px;text-align: center;margin-left: 170px;margin-top: 26px;margin-bottom: 24px;">Password</h4>

                </div>

                <div style="margin-left: 46px;width: 400px;padding-bottom: 0px;">
                    <h4 style="font-family: 'avertaregular';font-size: 15px"> 

                     A temporary password has been sent to your phone. Please enter it below.

                    </h4>
                </div>

                <form action="#">

                    <label style="margin-left: 55px;margin-top: 20px;">Temporary password</label>

                    <div class="form-row" style="margin-left: 40px;margin-right: 40px;">
                        <div class="col">
                            <input type="password" class="form-control" style="border-radius: 10px;margin-right: 40px;width: 330px;" name="tempPassword">
                        </div>

                    </div>

                    <label style="margin-left: 55px;margin-top: 20px;">New password</label>

                    <div class="form-row" style="margin-left: 40px;margin-right: 40px;">
                        <div class="col">
                            <input type="password" class="form-control" style="border-radius: 10px;margin-right: 40px;width: 330px;" name="newPassword">
                        </div>

                    </div>
                    <label style="margin-left: 55px;margin-top: 20px;">Confirm new password</label>

                    <div class="form-row" style="margin-left: 40px;margin-right: 40px;">
                        <div class="col">
                            <input type="password" class="form-control" style="border-radius: 10px;margin-right: 40px;width: 330px;" name="confirmNewPassowrd">
                        </div>

                    </div>
                    <div style="margin-left: 170px;padding-bottom: 10px;padding-top: 10px;">
                        <button type="button" class="btn btn-default button-continue" data-dismiss="modal">Continue</button>
                    </div>

                    <div>
                        <h3 class="have-an-account">
                           Didn’t receive temporary password? <a style="color: #58017D; font-family: 'avertabold';">Resend</a>
                        </h3>
                    </div>

                </form>

            </div>

        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="modalSignUp" role="dialog" style="overflow-y: scroll;">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content" style="border-radius: 20px;">
                <div class="modal-header">
                    <button type="button" class="close" style="margin-left: 10px;" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title" style="margin-right: 190px;font-size: 20px;">Sign up</h4>
                </div>
                <form action="#">

                    <label style="margin-left: 55px;color: #707070;margin-top: 20px;">Name</label>

                    <div class="form-row" style="margin-left: 40px;margin-right: 40px;">
                        <div class="col">
                            <input type="text" class="form-control" style="border-radius: 10px;margin-right: 40px;" placeholder="Enter first name" name="email">
                        </div>

                    </div>

                    <label style="margin-left: 55px;color: #707070;margin-top: 20px;">Email</label>

                    <div class="form-row" style="margin-left: 40px;margin-right: 40px;">
                        <div class="col">
                            <input type="text" class="form-control" style="border-radius: 10px;margin-right: 40px;" placeholder="Email" name="email">
                        </div>

                    </div>
                    <label style="margin-left: 55px;color: #707070;margin-top: 20px;">Mobile number</label>

                    <div class="form-row" style="margin-left: 40px;margin-right: 40px;">
                        <div class="col">
                            <input type="text" class="form-control" style="border-radius: 10px;margin-right: 40px;" placeholder="Mobile number" name="email">
                        </div>

                    </div>
                    <div style="margin-left: 170px;padding-bottom: 10px;padding-top: 10px;">
                        <button type="button" class="btn btn-default button-continue">Continue</button>
                    </div>

                    <div>
                        <h3 class="have-an-account">
                            Already have an account? <a style="color: #58017D;" data-toggle="modal" data-dismiss="modal" data-target="#modalLogin"> <strong> Login </strong></a>
                        </h3>
                    </div>

                </form>

                <div>
                    <hr>
                    <h3 class="have-an-account">or sign up using Social media</h3>
                    <div style="margin-left: 190px;margin-bottom: 30px;">
                        <span class="iconify" data-icon="bx:bxl-facebook-circle" style="font-size: 30px;color: #5757ec;" data-inline="false"></span>
                        <span class="iconify" data-icon="ant-design:google-circle-filled" style="font-size: 30px; color: red;" data-inline="false"></span>
                        <span class="iconify" data-icon="ant-design:twitter-circle-filled" style="font-size: 30px;color: #3a3af7cf;" data-inline="false"></span>
                    </div>
                </div>

                <div style="padding-bottom: 10px;">
                    <h3 class="have-an-account" style="text-align: center;margin-left: -10px;padding-bottom: 10px;">
                        By continuing, you agree to our<br>
                        <a style="color: #58017D;" href="Terms & Conditions.html"> <strong> Terms of use,</strong></a>
                        <a style="color: #58017D;" href="Terms & Conditions.html"> <strong> Privacy policy </strong></a> and 
                        <a style="color: #58017D;"> <strong> Vendor </strong></a>
                    </h3>
                </div>

            </div>

        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="modalAllOrders" role="dialog" style="overflow-y: scroll;">
        <div class="modal-dialog ">

            <!-- Modal content-->
            <div class="modal-content modal-all-order-form">
                <div class="modal-header" style="margin-top: 35px;">
                    <button type="button" class="close" style="margin-left: 10px;background-color: white;border-radius: 25px;width: 15px;height: 15px;margin-top: 3px;" data-dismiss="modal">
                        <h3 style="font-size: 20px;margin-top: -12px;margin-left: -5px;">x</h3>
                    </button>
                    <!-- <h4 class="modal-title" style="margin-right: 115px;font-size: 20px;">Edit profile</h4> -->
                    <div class="modal-div-all-orders-active">
                        <button class="modal-button-all-orders-active">
                            Active orders
                        </button>
                        <button class="modal-button-all-orders-active" style="background-color: white;border-color: white;color: #C8C8C8;">
                            Previous orders
                        </button>
                        <button class="modal-button-all-orders-active" style="background-color: white;border-color: white;color: #C8C8C8;">
                            Group orders
                        </button>
                    </div>

                </div>

                <div style="height: 100px;width: 410px;background-color: white;border-radius: 25px;margin-left: 42px;margin-top: 31px;">

                    <div>
                        <img src="assets/images/siMhabW0AA5BWx.jpg" style="width: 80px;border-radius: 25px;margin-top: 10px;margin-left: 10px;">
                    </div>

                    <div style="width: 275px;margin-left: 115px;margin-top: -77px;">

                        <h4 style="font-size: 15px;padding-bottom: 14px;">
                            Banku and Tilapia
                        </h4>
                        <h4 style="color: #5A5653;font-size: 12px;">
                            Lunch City
                        </h4>
                        <h4 style="color: #5A5653;font-size: 12px;">
                            GHC 35
                        </h4>
                        <h4 style="text-align: right;margin-top: -87px;margin-bottom: 42px;">
                            <i class="fa fa-angle-right"></i>
                        </h4>
                        <h4 style="font-size: 12px;text-align: right;">
                            Picked up by rider
                        </h4>
                    </div>

                </div>

            </div>

        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="modalEditProfile" role="dialog" style="overflow-y: scroll;">
        <div class="modal-dialog modal-sm">

            <!-- Modal content-->
            <div class="modal-content" style="border-radius: 20px;width: 360px;">
                <div class="modal-header">
                    <button type="button" class="close" style="margin-left: 10px;" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title" style="margin-right: 115px;font-size: 20px;">Edit profile</h4>
                </div>
                <form action="#">

                    <label style="margin-left: 55px;color: #707070;margin-top: 20px;">Name</label>

                    <div class="form-row" style="margin-left: 40px;margin-right: 40px;">
                        <div class="col">
                            <input type="text" class="form-control" style="border-radius: 10px;margin-right: 40px;" placeholder="Enter first name" name="email">
                        </div>

                    </div>

                    <label style="margin-left: 55px;color: #707070;margin-top: 20px;">Email</label>

                    <div class="form-row" style="margin-left: 40px;margin-right: 40px;">
                        <div class="col">
                            <input type="text" class="form-control" style="border-radius: 10px;margin-right: 40px;" placeholder="Email" name="email">
                        </div>

                    </div>
                    <label style="margin-left: 55px;color: #707070;margin-top: 20px;">Mobile number</label>

                    <div class="form-row" style="margin-left: 40px;margin-right: 40px;">
                        <div class="col">
                            <input type="text" class="form-control" style="border-radius: 10px;margin-right: 40px;" placeholder="Mobile number" name="email">
                        </div>

                    </div>
                    <div style="margin-left: 115px;padding-bottom: 10px;padding-top: 10px;">
                        <button type="button" class="btn btn-default button-continue">Save</button>
                    </div>

                    <div>
                        <h4 style="color: #01A517;font-size: 12px;margin-left: 96px;padding-bottom: 20px;padding-top: 20px;">
                            Profile successfully updated.
                        </h4>
                    </div>
                </form>

            </div>

        </div>
    </div>


    <!-- Modal -->
    <div class="modal fade" id="modalChangeDirection" role="dialog" style="overflow-y: scroll;">
        <div class="modal-dialog ">

            <!-- Modal content-->
            <div class="modal-content" style="background-color: #F6F6F6">

                <div class="mbr-overlay" style="opacity: 1;background-color: white;width: 30px;height: 30px;border-radius: 25px;margin-left: 40px;margin-top: 35px;">
                    <button type="button" class="close" style="margin-top: 1px;margin-right: 6px;" data-dismiss="modal" >&times;</button>

                </div>

                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3962.503693309499!2d-1.607639385653226!3d6.708212122850509!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xfdb967c54a8550b%3A0x635784fc88d2c21a!2sAntoa%20Rd%2C%20Kumasi%2C%20Ghana!5e0!3m2!1sen!2ske!4v1580288660357!5m2!1sen!2ske" width="500" height="400" frameborder="0" style="border:0;" allowfullscreen=""></iframe>


                <div style="margin-top: 20px;margin-left: 20px;">

                    <div>
                    <h5 style="font-size: 20px;margin-left: 25px;">
                        Antoa Rd
                    </h5>

                    </div>

                    <div style="margin-top: -26PX;margin-left: 275px;">

                    <h6 style="font-size: 17px; color: #58017D">
                        Enter custom location
                    </h6>

                    </div>
                </div>

                <div style="margin-left: 45px;margin-top: 25px;">

                    <h4 style="font-size: 15px">
                        Landmark or local name for this location
                    </h4>

                    <div class="form-group col" data-for="email">
                                    
                                    <div style="margin-bottom: 30px;">
                                    <input type="text" name="search"  style="border-radius: 9px;width: 310px;background: #FFFFFF;border-color: #EEEAEA;margin-left: -17px;" placeholder="Kumasi Zoological Garden" required="required" class="form-control display-7">
                                    <div style="width: 10px;height: 10px;border-radius: 25px;background-color: #EEEAEA;margin-top: -30px;margin-left: 260px;">
                                        <h4 style="font-size: 10px">
                                            x
                                        </h4>
                                    </div>
                                    </div>

                                </div>
                    
                    
                </div>


                <div>
                    <div style="margin-left: 45px;margin-top: 10px;" >
                        Please give this location a tag
                    </div>
                </div>

                <div style="width: 313px;margin-left: 45px;margin-bottom: 30px;margin-top: 20px;">

                    

                    <div style="font-size: 14px;color: #C8C8C8">
                        Work
                    </div>

                    <div style="margin-left: 63px;margin-top: -20px;font-size: 14px;color: #C8C8C8;">
                        Home
                    </div>

                    <div style="margin-left: 135px;margin-top: -27px;border-color: #58007D;color: #58007D;border-style: solid;width: 70px;border-radius: 25px;border-width: 1px;padding-left: 10px;height: 30px;font-size: 14px;padding-top: 3px;">
                        Mosque
                    </div>

                    <div style="margin-left: 240px;margin-top: -26px;font-size: 14px;color: #C8C8C8;">
                        Church
                    </div>
                    
                </div>


                <div>

                    <div class="form-group col" data-for="email">
                                    
                                    <div style="margin-bottom: 30px;">
                                    <input type="text" name="search" style="border-radius: 9px;width: 321px;background: #FFFFFF;border-color: #EEEAEA;margin-left: 25px;color: #CCCCCC;padding-left: 25px;" placeholder="Other" required="required" class="form-control display-7">
                                    
                                    </div>

                                </div>
                    
                </div>

                <div style="font-size: 20px;margin-left: 45px;">
                    Saved locations
                </div>

                <div>

                <div style="border-color: #EEEAEA;border-style: solid;border-width: 1px;width: 340px;height: 130px;margin-top: 25px;margin-bottom: 25px;margin-left: 45px;border-radius: 17px;">

                    <h4 style="font-size: 20px;margin-left: 20px;margin-top: 20px; margin-bottom: 20px;">
                        Home - Accra, Ghana
                    </h4>

                    <h4 style="font-size: 17px;margin-left: 20px;color: #3D3C3C;margin-bottom: 10px;">
                        27 Gborbilor St. Accra.
                    </h4>

                    <h4 style="font-size: 17px;margin-left: 20px;margin-top: 14px;color: #3D3C3C;">
                        10 minutes walk from Kpogas junction
                    </h4>
                    
                </div>

                <div style="border-color: #EEEAEA;border-style: solid;border-width: 1px;width: 340px;height: 130px;margin-top: 25px;margin-bottom: 25px;margin-left: 45px;border-radius: 17px;">

                    <h4 style="font-size: 20px;margin-left: 20px;margin-top: 20px; margin-bottom: 20px;">
                        Home - Accra, Ghana
                    </h4>

                    <h4 style="font-size: 17px;margin-left: 20px;color: #3D3C3C;margin-bottom: 10px;">
                        27 Gborbilor St. Accra.
                    </h4>

                    <h4 style="font-size: 17px;margin-left: 20px;margin-top: 14px;color: #3D3C3C;">
                        10 minutes walk from Kpogas junction
                    </h4>
                    
                </div>

                <div style="border-color: #EEEAEA;border-style: solid;border-width: 1px;width: 340px;height: 130px;margin-top: 25px;margin-bottom: 25px;margin-left: 45px;border-radius: 17px;">

                    <h4 style="font-size: 20px;margin-left: 20px;margin-top: 20px; margin-bottom: 20px;">
                        Home - Accra, Ghana
                    </h4>

                    <h4 style="font-size: 17px;margin-left: 20px;color: #3D3C3C;margin-bottom: 10px;">
                        27 Gborbilor St. Accra.
                    </h4>

                    <h4 style="font-size: 17px;margin-left: 20px;margin-top: 14px;color: #3D3C3C;">
                        10 minutes walk from Kpogas junction
                    </h4>
                    
                </div>

                </div>

                <div style="background-color: #58017D;width: 75px;height: 35px;margin-bottom: 20px;margin-left: 50px;border-radius: 25px;">
                    
                    <button style="font-size: 17px;background-color: #f0f8ff00;color: white;border-style: none;margin-left: 10px;padding-top: 6px;">
                        Save
                    </button>
                </div>

            </div>

        </div>

    </div>

    <!-- Modal -->
    <div class="modal fade" id="modalLogin" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content" style="border-radius: 20px;">
                <div class="modal-header">
                    <button type="button" class="close" style="margin-left: 10px;" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title" style="margin-right: 190px;font-size: 20px;">Log in</h4>
                </div>
                <form action="#">

                    <label style="margin-left: 55px;color: #707070;margin-top: 20px;">Phone number or email</label>

                    <div class="form-row" style="margin-left: 40px;margin-right: 40px;">
                        <div class="col">
                            <input type="text" class="form-control" style="border-radius: 10px;margin-right: 40px;" placeholder="Enter Phone numer or email here...." name="email">
                        </div>

                    </div>

                    <label style="margin-left: 55px;color: #707070;margin-top: 20px;">Password</label>

                    <div class="form-row" style="margin-left: 40px;margin-right: 40px;">
                        <div class="col">
                            <input type="password" class="form-control" style="border-radius: 10px;margin-right: 40px;" placeholder="" name="email">
                        </div>

                    </div>
                    <div style="margin-left: 170px;padding-bottom: 10px;padding-top: 10px;">
                        <button type="button" class="btn btn-default button-continue">Log in</button>
                    </div>

                    <div>
                        <h3 class="have-an-account">
                            <a style="color: #58017D;"> <strong> Forgot Password </strong></a>
                        </h3>
                    </div>
                    <div>
                        <h3 class="have-an-account">
                            Don't have an account <a style="color: #58017D;" data-toggle="modal" data-dismiss="modal" data-target="#modalSignUp" > <strong> Sign Up </strong></a>
                        </h3>
                    </div>

                </form>

            </div>

        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="modalMyRatingHistory" role="dialog" style="overflow-y: scroll;">
        <div class="modal-dialog ">

            <!-- Modal content-->
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" style="margin-left: 10px;" data-dismiss="modal">&times;</button>
                    <div>
                        <h4 style="font-size: 20px;text-align: center;margin-right: 150px;margin-top: 20px;">My ratings history</h4>
                    </div>

                </div>

                <div>
                    <h4 style="font-size: 20px;margin-left: 100px;margin-top: 10px;margin-bottom: 20px;">
                       You have given 117 ratings & reviews
                    </h4>
                </div>

                <div class="row" style="width: 330px;margin-left: 103px;padding-bottom: 20px;">

                    <div class="col-sm-12" style="background-color:#EFEFEF;margin-top: 5px;margin-bottom: 20px;border-radius: 11px;">
                        <h4 style="font-size: 17px;margin-top: 15px;">
                            Hoogah Osu
                        </h4>

                        <div style="margin-top: -34px;margin-left: 200px;">
                            <span class="iconify" data-icon="ant-design:star-filled" data-inline="false" style="color: green"></span>
                            <span class="iconify" data-icon="dashicons:star-filled" data-inline="false" style="color: green"></span>
                            <span class="iconify" data-icon="dashicons:star-filled" data-inline="false" style="color: green"></span>
                            <span class="iconify" data-icon="dashicons:star-filled" data-inline="false" style="color: green"></span>
                            <span class="iconify" data-icon="dashicons:star-filled" data-inline="false" style="color: green"></span>
                        </div>

                        <div style="font-size: 15px;margin-top: 25px;">
                            The quick, brown fox jumps over a lazy dog. DJs flock by when MTV ax quiz prog. Junk MTV quiz graced by fox whelps. Bawds jog, flick quartz
                        </div>

                        <div style="margin-top: 20px;padding-bottom: 10px;">
                            <img src="assets/images/like.svg" style="margin-top: -7px;"> 12
                            <img src="assets/images/dislike.svg" style="padding-left: 10px;margin-top: -6px;"> 2

                            <h4 style="color: #707070;font-size: 15px;margin-left: 210px;margin-top: -20px;">
                            24 Jan 2020
                        </h4>
                        </div>

                    </div>

                    <div class="col-sm-12" style="background-color:#EFEFEF;margin-top: 5px;margin-bottom: 20px;border-radius: 11px;">
                        <h4 style="font-size: 17px;margin-top: 15px;">
                           The Piano Bar
                        </h4>

                        <div style="margin-top: -34px;margin-left: 200px;">
                            <span class="iconify" data-icon="ant-design:star-filled" data-inline="false" style="color: green"></span>
                            <span class="iconify" data-icon="dashicons:star-filled" data-inline="false" style="color: green"></span>
                            <span class="iconify" data-icon="dashicons:star-filled" data-inline="false" style="color: green"></span>
                            <span class="iconify" data-icon="dashicons:star-filled" data-inline="false" style="color: green"></span>
                            <span class="iconify" data-icon="dashicons:star-filled" data-inline="false" style="color: green"></span>
                        </div>

                        <div style="font-size: 15px;margin-top: 25px;">
                            The quick, brown fox jumps over a lazy dog. DJs flock by when MTV ax quiz prog. Junk MTV quiz graced by fox whelps. Bawds jog, flick quartz
                        </div>

                        <div style="margin-top: 20px;padding-bottom: 10px;">
                            <img src="assets/images/like.svg" style="margin-top: -7px;"> 12
                            <img src="assets/images/dislike.svg" style="padding-left: 10px;margin-top: -6px;"> 2

                            <h4 style="color: #707070;font-size: 15px;margin-left: 210px;margin-top: -20px;">
                            24 Jan 2020
                        </h4>
                        </div>

                    </div>

                    <div class="col-sm-12" style="background-color:#EFEFEF;margin-top: 5px;margin-bottom: 20px;border-radius: 11px;">
                        <h4 style="font-size: 17px;margin-top: 15px;">
                            Bloom Bar
                        </h4>

                        <div style="margin-top: -34px;margin-left: 200px;">
                            <span class="iconify" data-icon="ant-design:star-filled" data-inline="false" style="color: green"></span>
                            <span class="iconify" data-icon="dashicons:star-filled" data-inline="false" style="color: green"></span>
                            <span class="iconify" data-icon="dashicons:star-filled" data-inline="false" style="color: green"></span>
                            <span class="iconify" data-icon="dashicons:star-filled" data-inline="false" style="color: green"></span>
                            <span class="iconify" data-icon="dashicons:star-filled" data-inline="false" style="color: green"></span>
                        </div>

                        <div style="font-size: 15px;margin-top: 25px;">
                            The quick, brown fox jumps over a lazy dog. DJs flock by when MTV ax quiz prog. Junk MTV quiz graced by fox whelps. Bawds jog, flick quartz
                        </div>

                        <div style="margin-top: 20px;padding-bottom: 10px;">
                            <img src="assets/images/like.svg" style="margin-top: -7px;"> 12
                            <img src="assets/images/dislike.svg" style="padding-left: 10px;margin-top: -6px;"> 2

                            <h4 style="color: #707070;font-size: 15px;margin-left: 210px;margin-top: -20px;">
                            24 Jan 2020
                        </h4>
                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>

    <!-- Modal -->
    <div class="modal fade" id="modalMyRateRestaurants" role="dialog" style="overflow-y: scroll;">
        <div class="modal-dialog ">

            <!-- Modal content-->
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" style="margin-left: 10px;" data-dismiss="modal">&times;</button>
                    <div>
                        <h4 style="font-size: 20px;text-align: center;margin-right: 200px;margin-top: 20px;">Hoogah</h4>
                    </div>

                </div>

                <div>
                    <h4 style="font-size: 60px;margin-left: 100px;margin-top: 30px;margin-bottom: 20px;">
                       4.6
                    </h4>
                </div>

                <div style="margin-left: 100px;margin-bottom: 20px;">

                    <span class="iconify" data-icon="ant-design:star-filled" data-inline="false" style="color: green"></span>
                    <span class="iconify" data-icon="dashicons:star-filled" data-inline="false" style="color: green"></span>
                    <span class="iconify" data-icon="dashicons:star-filled" data-inline="false" style="color: green"></span>
                    <span class="iconify" data-icon="dashicons:star-filled" data-inline="false" style="color: green"></span>
                    <span class="iconify" data-icon="dashicons:star-filled" data-inline="false" style="color: green"></span>

                </div>

                <div style="margin-left: 100px;margin-bottom: 40px;">

                    <h4 style="font-size: 15px;">
                        1,008 ratings
                    </h4>

                </div>

                <div style="margin-left: 318px;margin-top: -200px;margin-bottom: 80px;">

                    <div style="margin-bottom: -27px;margin-left: -30px;">
                        <h4 style="font-size: 10px">
                            67%
                        </h4>
                    </div>

                    <div style="padding-bottom: 10px;">

                        <span class="iconify" data-icon="ant-design:star-filled" data-inline="false" style="color: #C8C8C8"></span>
                        <span class="iconify" data-icon="dashicons:star-filled" data-inline="false" style="color: #C8C8C8"></span>
                        <span class="iconify" data-icon="dashicons:star-filled" data-inline="false" style="color: #C8C8C8"></span>
                        <span class="iconify" data-icon="dashicons:star-filled" data-inline="false" style="color: #C8C8C8"></span>
                        <span class="iconify" data-icon="dashicons:star-filled" data-inline="false" style="color: #C8C8C8"></span>

                    </div>

                    <div style="margin-bottom: -27px;margin-left: -30px;">
                        <h4 style="font-size: 10px">
                            14%
                        </h4>
                    </div>

                    <div style="padding-bottom: 10px;">

                        <span class="iconify" data-icon="ant-design:star-filled" data-inline="false" style="color: #C8C8C8"></span>
                        <span class="iconify" data-icon="dashicons:star-filled" data-inline="false" style="color: #C8C8C8"></span>
                        <span class="iconify" data-icon="dashicons:star-filled" data-inline="false" style="color: #C8C8C8"></span>
                        <span class="iconify" data-icon="dashicons:star-filled" data-inline="false" style="color: #C8C8C8"></span>

                    </div>

                    <div style="margin-bottom: -27px;margin-left: -30px;">
                        <h4 style="font-size: 10px">
                            10%
                        </h4>
                    </div>

                    <div style="padding-bottom: 10px;">

                        <span class="iconify" data-icon="ant-design:star-filled" data-inline="false" style="color: #C8C8C8"></span>
                        <span class="iconify" data-icon="dashicons:star-filled" data-inline="false" style="color: #C8C8C8"></span>
                        <span class="iconify" data-icon="dashicons:star-filled" data-inline="false" style="color: #C8C8C8"></span>

                    </div>

                    <div style="margin-bottom: -27px;margin-left: -30px;">
                        <h4 style="font-size: 10px">
                            5%
                        </h4>
                    </div>

                    <div style="padding-bottom: 10px;">

                        <span class="iconify" data-icon="ant-design:star-filled" data-inline="false" style="color: #C8C8C8"></span>
                        <span class="iconify" data-icon="dashicons:star-filled" data-inline="false" style="color: #C8C8C8"></span>

                    </div>

                    <div style="margin-bottom: -27px;margin-left: -30px;">
                        <h4 style="font-size: 10px">
                            4%
                        </h4>
                    </div>

                    <div style="padding-bottom: 10px;">

                        <span class="iconify" data-icon="ant-design:star-filled" data-inline="false" style="color: #C8C8C8"></span>

                    </div>

                </div>

                <div class="row" style="width: 330px;margin-left: 103px;padding-bottom: 20px;">

                    <div class="col-sm-12" style="background-color:#EFEFEF;margin-top: 5px;margin-bottom: 20px;border-radius: 11px;">
                        <h4 style="font-size: 17px;margin-top: 15px;">
                            Kafui
                        </h4>

                        <div style="margin-top: -34px;margin-left: 200px;">
                            <span class="iconify" data-icon="ant-design:star-filled" data-inline="false" style="color: green"></span>
                            <span class="iconify" data-icon="dashicons:star-filled" data-inline="false" style="color: green"></span>
                            <span class="iconify" data-icon="dashicons:star-filled" data-inline="false" style="color: green"></span>
                            <span class="iconify" data-icon="dashicons:star-filled" data-inline="false" style="color: green"></span>
                            <span class="iconify" data-icon="dashicons:star-filled" data-inline="false" style="color: green"></span>
                        </div>

                        <div style="font-size: 15px;margin-top: 25px;">
                            The quick, brown fox jumps over a lazy dog. DJs flock by when MTV ax quiz prog. Junk MTV quiz graced by fox whelps. Bawds jog, flick quartz
                        </div>

                        <div style="margin-top: 20px;padding-bottom: 10px;">
                            <img src="assets/images/like.svg" style="margin-top: -7px;"> 12
                            <img src="assets/images/dislike.svg" style="padding-left: 10px;margin-top: -6px;"> 2

                            <h4 style="color: #707070;font-size: 15px;margin-left: 210px;margin-top: -20px;">
                            24 Jan 2020
                        </h4>
                        </div>

                    </div>

                    <div class="col-sm-12" style="background-color:#EFEFEF;margin-top: 5px;margin-bottom: 20px;border-radius: 11px;">
                        <h4 style="font-size: 17px;margin-top: 15px;">
                           Kafui
                        </h4>

                        <div style="margin-top: -34px;margin-left: 200px;">
                            <span class="iconify" data-icon="ant-design:star-filled" data-inline="false" style="color: green"></span>
                            <span class="iconify" data-icon="dashicons:star-filled" data-inline="false" style="color: green"></span>
                            <span class="iconify" data-icon="dashicons:star-filled" data-inline="false" style="color: green"></span>
                            <span class="iconify" data-icon="dashicons:star-filled" data-inline="false" style="color: green"></span>
                            <span class="iconify" data-icon="dashicons:star-filled" data-inline="false" style="color: green"></span>
                        </div>

                        <div style="font-size: 15px;margin-top: 25px;">
                            The quick, brown fox jumps over a lazy dog. DJs flock by when MTV ax quiz prog. Junk MTV quiz graced by fox whelps. Bawds jog, flick quartz
                        </div>

                        <div style="margin-top: 20px;padding-bottom: 10px;">
                            <img src="assets/images/like.svg" style="margin-top: -7px;"> 12
                            <img src="assets/images/dislike.svg" style="padding-left: 10px;margin-top: -6px;"> 2

                            <h4 style="color: #707070;font-size: 15px;margin-left: 210px;margin-top: -20px;">
                            24 Jan 2020
                        </h4>
                        </div>

                    </div>

                    <div style="margin-left: 93px;margin-bottom: 20px;margin-top: 25px;">

                        <button style="background-color: #58007D;border-color: #58007D;border-radius: 25px;color: white;font-size: 15px;width: 140px;height: 40px;">
                            Rate Hoogah
                        </button>

                    </div>

                </div>

            </div>

        </div>

    </div>

    <!-- Modal -->
    <div class="modal fade" id="modalFood" role="dialog" style="overflow-y: scroll;">
        <div class="modal-dialog ">

            <!-- Modal content-->
            <div class="modal-content" style="background-color: #F6F6F6">

                <div class="mbr-overlay" style="opacity: 1;background-color: white;width: 30px;height: 30px;border-radius: 25px;margin-left: 40px;margin-top: 35px;">
                    <button type="button" class="close" style="margin-top: 1px;margin-right: 6px;" data-dismiss="modal">&times;</button>

                </div>

                <div style="width: 500px;height: 400px;">

                    <img src="assets/images/banku & tilapia@2x.png" style="width: 500px;height: 400px;">

                </div>

                <div style="background-color: ##F6F6F6;width: 430px;margin-left: 29px;margin-bottom: 40px;margin-top: 40px;">

                    <h4 style="font-size: 20px;text-align: left;">
                        Banku and Tilapia
                    </h4>

                    <h4 style="font-size: 20px;text-align: right;margin-top: -28px;">
                        GHC 35
                    </h4>

                </div>

                <div style="background-color: #F6F6F6;width: 400px;margin-left: 40px;">
                    <p style="color: #707070;font-size: 18px;">
                        Banku and pepper with local appamu tilapia grilled with onion, salt and garlic sauce. and pepper with local appamu tilapia grilled with onion, salt and garlic sauce. and pepper with local appamu tilapia grilled with onion, salt and garlic sauce.
                    </p>
                </div>

                <div style="background-color: #F6F6F6;width: 400px;margin-left: 40px;">

                    <div>
                        Extras
                    </div>

                    <form style="margin-left: -11px; margin-top: 15px;">
                        <label class="container" style="margin-left: 10px;font-size: 15px;color: #58017D">Coke (+ GHC 6)
                            <input type="checkbox" checked="checked">
                            <span class="checkmark"></span>
                        </label>

                        <label class="container" style="margin-left: 10px;font-size: 15px;color: #707070">1 bottled water (+ GHC 5)
                            <input type="checkbox">
                            <span class="checkmark"></span>
                        </label>

                        <label class="container" style="margin-left: 10px;font-size: 15px; color: #58017D">Orange juice (+ GHC 8)
                            <input type="checkbox" checked="checked">
                            <span class="checkmark"></span>
                        </label>
                    </form>
                </div>

                <div style="background-color: #fff;width: 185px;height: 50px;margin-bottom: 40px;border-radius: 11px;margin-left: 162px;margin-top: 15px;border-style: solid;border-width: 1px;border-color: #C8C8C8;">

                    <div style=" margin-top: 15px;margin-left: 20px;">
                        <i class="fa fa-minus" aria-hidden="true" style="color: #58017D"></i>
                    </div>

                    <div style=" margin-left: 87px;margin-top: -24px;">
                        <h4 style="color: #58017D; font-size: 20px">
                        2
                    </h4>
                    </div>

                    <div style="margin-left: 151px;margin-top: -30px;">

                        <i class="fa fa-plus" aria-hidden="true" style="color: #58017D"></i>
                    </div>
                </div>

                <div style="background-color: #58017D;width: 235px;height: 45px;margin-bottom: 51px;margin-left: 145px;border-radius: 40px;">

                    <div style="margin-top: 11px;margin-left: 25px;">
                        <button style="background-color: #f0f8ff00;color: white;font-size: 15px;border-style: none;">
                            GHC 119
                        </button>
                    </div>

                    <div style="margin-left: 116px;margin-top: -25px;">

                        <button style="background-color: #f0f8ff00;color: white;font-size: 15px;border-style: none;">
                            Add to basket
                        </button>

                    </div>

                    <div style="background-color: white;width: 2px;height: 32px;margin-left: 105px;margin-top: -28px;">

                    </div>

                </div>

            </div>

        </div>

    </div>

    <!-- Modal -->
    <div class="modal fade" id="modalMenuLogin" role="dialog" style="overflow-y: scroll;">
        <div class="modal-dialog modal-sm">

            <!-- Modal content-->
            <div class="modal-content" style="border-radius: 19px;width: 370px;">

                <div class="row" style="margin-top: 35px;margin-left: 10px;">

                    <div class="col-sm-6">
                        <h4 style="font-size: 15px;">
                    Hope Adoli
                   </h4>
                        <h4 style="font-size: 10px;">
                    0242267686
                   </h4>

                        <h4 style="font-size: 10px;">
                    kafui@loudrsocio.co
                   </h4>
                    </div>

                    <div class="col-sm-6">
                        <button class="menu-button-edit" data-toggle="modal" data-dismiss="modal" data-target="#modalEditProfile">
                            Edit
                        </button>
                    </div>

                </div>

                <!-- <div class="row current-balance" style="margin-top: 15px;margin-left: 25px;"> -->
                <div class="current-balance-parent-div">

                    <div class="current-balance-child1-div">
                        <h4 class="current-balance-h4-1">
                    Current Balance
                   </h4>
                        <h4 style="font-size: 15px;color: black;">
                    <strong> GHC 200.46</strong>
                   </h4>

                    </div>

                    <div class="curent-balance-child2-div">
                        <button class="button-top-up">
                            Top up
                        </button>
                    </div>

                </div>

                <div class="row" style="
                margin-top: 15px;
                margin-left: 10px;">

                    <div class="col-sm-6">
                        <i class="fa fa-heart-o"></i>

                    </div>

                    <div class="col-sm-6">
                        <h4 style="font-size: 15px;margin-left: -130px;">
                          Refer a friend
                             </h4>
                    </div>

                    <hr style="width: 318px;margin-left: 10px;">

                </div>

                <div class="row" style="
                margin-top: 15px;
                margin-left: 10px;">

                    <div class="col-sm-6" style="margin-top: -7px;">
                        <!-- <span class="iconify" data-icon="uil:cart" data-inline="false"></span> -->
                        <img src="assets/images/bag-09.svg" style="font-size: -16px;width: 15px;">

                    </div>

                    <div class="col-sm-3">
                        <h4 style="font-size: 15px;margin-left: -130px;">

                          <a style="color: black;" data-toggle="modal" data-dismiss="modal" data-target="#modalAllOrders">All Orders</a>
                             </h4>

                    </div>
                    <div class="col-sm-3" style="margin-top: -9px;">
                        <h4 class="button-all-orders"> 2 </h4>
                    </div>

                    <hr style="width: 318px;margin-left: 10px;">

                </div>

                <div class="row" style="
                margin-top: 15px;
                margin-left: 10px;">

                    <div class="col-sm-6">
                        <span class="iconify" data-icon="dashicons:star-empty" data-inline="false"></span>

                    </div>

                    <div class="col-sm-6">
                        <h4 style="font-size: 15px;margin-left: -130px;">
                            <a style="color: black;" data-toggle="modal" data-dismiss="modal" data-target="#modalMyRatingHistory">
                          My Rating & reviews
                      </a>
                             </h4>
                    </div>

                    <hr style="width: 318px;margin-left: 10px;">

                </div>

                <div class="row" style="margin-top: 15px;margin-left: 10px;margin-bottom: 15px">

                    <div class="col-sm-6">
                        <span class="iconify" data-icon="fa-regular:comment" data-inline="false"></span>

                    </div>

                    <div class="col-sm-6">
                        <h4 style="font-size: 15px;margin-left: -130px;">
                          <a style="color: black" href="help-and-support.html">Customer support</a>
                             </h4>
                    </div>

                </div>

            </div>

            <div>
                <button type="button" class="close x-button-floating" style="background-color: white;" data-dismiss="modal">&times;</button>
            </div>

        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="modalRestaurantsMenu" role="dialog" style="overflow-y: scroll;">
        <div class="modal-dialog modal-sm">

            <!-- Modal content-->
            <div class="modal-content" style="border-radius: 19px;width: 302px;margin-top: 85px">

                <div class="row" style="
                margin-top: 30px;
                margin-left: 10px;">

                    <div class="col-sm-6">
                        <i class="fa fa-heart-o"></i>

                    </div>

                    <div class="col-sm-6">
                        <h4 style="font-size: 15px;margin-left: -95px;">
                          Start a group order
                             </h4>
                    </div>

                    <!-- <hr style="width: 318px;margin-left: 10px;"> -->

                </div>

                <div class="row" style="
                margin-top: 15px;
                margin-left: 10px;">

                    <div class="col-sm-6">
                        <i class="fa fa-heart-o"></i>

                    </div>

                    <div class="col-sm-6">
                        <h4 style="font-size: 15px;margin-left: -95px;">
                            <a style="color: black;" data-toggle="modal" data-dismiss="modal" data-target="#modalMyRatingHistory">
                          Add to favourite restaurants
                      </a>
                             </h4>
                    </div>

                    <!-- <hr style="width: 318px;margin-left: 10px;"> -->

                </div>

                <div class="row" style="
                margin-top: 15px;
                margin-left: 10px;">

                    <div class="col-sm-6">
                        <span class="iconify" data-icon="dashicons:star-empty" data-inline="false"></span>

                    </div>

                    <div class="col-sm-6">
                        <h4 style="font-size: 15px;margin-left: -95px;">
                            <a style="color: black;" data-toggle="modal" data-dismiss="modal" data-target="#modalMyRateRestaurants">
                         Restaurant reviews
                      </a>
                             </h4>
                    </div>

                    <!-- <hr style="width: 318px;margin-left: 10px;"> -->

                </div>

                <div class="row" style="
                margin-top: 15px;
                margin-left: 10px;">

                    <div class="col-sm-6">
                        <span class="iconify" data-icon="dashicons:star-empty" data-inline="false"></span>

                    </div>

                    <div class="col-sm-6">
                        <h4 style="font-size: 15px;margin-left: -95px;">
                            <a style="color: black;" data-toggle="modal" data-dismiss="modal" data-target="#modalMyRatingHistory">
                         Report this restaurant
                      </a>
                             </h4>
                    </div>

                    <!-- <hr style="width: 318px;margin-left: 10px;"> -->

                </div>

                <div class="row" style="margin-top: 15px;margin-left: 10px;margin-bottom: 30px">

                    <div class="col-sm-6">
                        <img src="assets/images/export.svg">

                    </div>

                    <div class="col-sm-6">
                        <h4 style="font-size: 15px;margin-left: -95px;">
                          <a >Share this restaurant</a>
                             </h4>
                    </div>

                </div>

            </div>

            <div>
                <button type="button" class="close x-button-floating" style="background-color: white;margin-right: 123px;" data-dismiss="modal">&times;</button>
            </div>

        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="modalMenuNotLogin" role="dialog" style="overflow-y: scroll;">
        <div class="modal-dialog modal-sm">

            <!-- Modal content-->
            <div class="modal-content" style="border-radius: 19px;width: 350px;">

                <div class="row" style="margin-top: 35px;margin-left: 10px;">

                    <button style="color: white;background-color: #58017D;width: 90px;height: 40px;border-radius: 50px;margin-left: 100px;font-size: 12px;" data-toggle="modal" data-dismiss="modal" data-target="#modalSignUp">
                        Sign up
                    </button>

                    <hr style="width: 350px;margin-left: 10px;">

                </div>

                <div class="row" style="
                margin-top: 15px;
                margin-left: 10px;">

                    <div class="col-sm-6">
                        <i class="fa fa-heart-o"></i>

                    </div>

                    <div class="col-sm-6">
                        <h4 style="font-size: 15px;margin-left: -130px;">
                          Refer a friend
                             </h4>
                    </div>

                    <hr style="width: 350px;margin-left: 10px;">

                </div>

                <div class="row" style="
                margin-top: 15px;
                margin-left: 10px;">

                    <div class="col-sm-6">
                        <span class="iconify" data-icon="uil:cart" data-inline="false"></span>

                    </div>

                    <div class="col-sm-3">
                        <h4 style="font-size: 15px;margin-left: -130px;">
                          All Orders
                             </h4>

                    </div>
                    <div class="col-sm-3">
                        <h4 class="button-all-orders"> 2 </h4>
                    </div>

                    <hr style="width: 350px;margin-left: 10px;">

                </div>

                <div class="row" style="
                margin-top: 15px;
                margin-left: 10px;">

                    <div class="col-sm-6">
                        <span class="iconify" data-icon="dashicons:star-empty" data-inline="false"></span>

                    </div>

                    <div class="col-sm-6">
                        <h4 style="font-size: 15px;margin-left: -130px;">
                          My Rating & reviews
                             </h4>
                    </div>

                    <hr style="width: 350px;margin-left: 10px;">

                </div>

                <div class="row" style="
                 margin-top: 15px;
                 margin-left: 10px;">

                    <div class="col-sm-6">
                        <span class="iconify" data-icon="fa-regular:comment" data-inline="false"></span>

                    </div>

                    <div class="col-sm-6">
                        <h4 style="font-size: 15px;margin-left: -130px;">
                          <a style="color: #000000" href="help-and-support.html">Customer support</a>
                             </h4>
                    </div>

                    <hr style="width: 350px;margin-left: 10px;">

                </div>

                <div class="row" style="
                 margin-top: 15px;
                 margin-left: 10px;padding-bottom: 20px;">

                    <div class="col-sm-6">
                        <span class="iconify" data-icon="bytesize:settings" data-inline="false"></span>

                    </div>

                    <div class="col-sm-6">
                        <h4 style="font-size: 15px;margin-left: -130px;">
                          Setting
                             </h4>
                    </div>

                    <!-- <hr style="width: 350px;margin-left: 10px;"> -->

                </div>

            </div>

            <div>
                <button type="button" class="close x-button-floating" style="background-color: white;" data-dismiss="modal">&times;</button>
            </div>

        </div>
    </div>