
    <section class="menu cid-rNDrya5QF1" once="menu" id="menu2-24">

        <nav class="navbar navbar-expand beta-menu navbar-dropdown align-items-center navbar-fixed-top navbar-toggleable-sm bg-color transparent">
            <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <div class="hamburger">
                    <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                </div>
            </button>
            <div class="menu-logo">
                <div class="navbar-brand">
                    <span class="navbar-logo">
                    <a href="index.html">
                        <img src="assets/images/logo.png" alt="Homechow" class="image-media" title="" style="height: 30px;">
                    </a>
                </span>

                </div>
                <i class=""></i>
            </div>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">

                <div style="color: white;">
                    <h4 style="font-size: 18px;font-family: 'avertaregular';" id="kitchenId">
                       <a href="#features15-2u"> Street-food</a>
                    </h4>
                </div>

                <div style="color: white;">
                    <h4 style="font-size: 18px;font-family: 'avertaregular';" id="restaurantsId">
                       <a href="#features2-2f"> Restaurants</a>
                    </h4>
                </div>

                <div style="margin-left: 45px;color: white;">
                    <h4 style="font-size: 18px;font-family: 'avertaregular';" id="groupOrdersId">
                        Group orders
                    </h4>
                </div>

                <div style="margin-left: 40px;color: white;">
                    <h4 style="font-size: 18px;font-family: 'avertaregular';" id="drinksId">
                        <a href="#features2-2fs">Drinks</a>
                    </h4>
                </div>

                <div style="margin-left: 40px;color: white;">
                    <h4 style="font-size: 18px;font-family: 'avertaregular';" id="partyBookingId">
                        <a href="book-for-event.html">Events booking</a>
                    </h4>
                </div>

                <div id="newVendor" style="width: 117px;height: 35px;background-color: #1fbf58;border-radius: 25px;margin-left: 40px">
                <button style="border-style: none;color: white; background-color: #ffffff00;margin-top: 4px;margin-left: 6px;">
                    New Vendor
                </button>
                </div>

                <div class="nav-icons nav-media-ios" style="margin-left: 20px;">
                    <span style="color: white;font-size: 30px; padding-right: 4px;" class="iconify icon-app-store-ios-brands" data-icon="fa-brands:app-store" data-inline="false"></span>
                </div>

                <div class="nav-icons nav-media-google-play" style="margin-left: 15px;">
                    <span style="color: white;font-size: 30px;" class="iconify icon-app-store-ios-brands" data-icon="mdi:google-play" data-inline="false"></span>
                </div>

                <div class="nav-icons nav-media-avator" style="margin-left: 45px;">
                    <a data-toggle="modal" data-target="#modalSignUp">
                        <span style="color: white;font-size: 25px;padding-left: 2px;" class="iconify" data-icon="bx:bx-user" data-inline="false"></span>
                    </a>
                </div>

                <div class="nav-menu-media" style="margin-left: 15px;">

                    <a data-toggle="modal" data-target="#modalMenuLogin">

                        <img src="assets/images/webapp-menu-icon.svg">
                    </a>
                </div>

            </div>
        </nav>
    </section>

    <section class="header3 cid-rOhlph0oA1 mbr-parallax-background" id="header3-2t" style="height: 550px;">

        <div class="container align-center">
            <div class="row justify-content-md-center">
                <div class="mbr-white col-md-10">

                    <p class="mbr-text pb-3 mbr-fonts-style display-2" style="color: white;padding-top: 90px;">

                        <strong style="font-family: 'avertabold';">We curate over 300,000 meals
                        <br>served by more than 5,000
                        <br>restaurants and kitchens.</strong></p>

                </div>
            </div>
        </div>
        <div class="hidden-sm-down">
            <a class="drop-arrow-button" href="#features1-28" style="margin-left: -60px;">
                <h4 style="font-size: 15px;margin-left: 18px;margin-top: 12px;">Order now</h4>
            </a>
        </div>

    </section>

    <div class="container navbar-dropdowns  navbar-fixed-tops" style="margin-top: 120px;" id="busket-view">

        <div class="basket-label" style="margin-top: 100px;">
            <!-- <span class="iconify" data-icon="uil:cart" data-inline="false"></span> -->
            <h4 style="font-size: 15px;text-align: left;margin-left: 10px; font-family: 'avertaregular';margin-top: 5px;">

                <img src="assets/images/webapp-basket-icon.svg" style="height: 25px;">
                     <!-- <span class="iconify" data-icon="uil:cart" data-inline="false"></span> -->
                &nbsp &nbsp 2 items in your basket.

            </h4>
            <button class="btn-view">
                View
            </button>

        </div>

        <div class="floating-busket-counter">

            <h4 style="color: white;font-size: 10px;padding-left: 3px;font-family: 'avertaregular';">
                    2
                </h4>

        </div>

        <div class="commucation-button">
            <img src="assets/images/message-circle.svg" class="communication-img">
        </div>

    </div>

    <section class="features1 cid-rNDsef0Rkz mbr-fullscreen" id="features1-28">

        <div class="container">

            <h2 class="heading-chow" style="font-family: 'avertaregular';">
                How to order a meal on Homechow
            </h2>

            <div class="media-container-row">

                <div class="card p-3 col-12 col-md-6 col-lg-3">
                    <div class="card-img pb-3">
                        <!-- <span class="mbri-bootstrap mbr-iconfont"></span> -->
                    </div>
                    <div class="card-box">
                        <h4 class="card-title py-3 mbr-fonts-style display-5" style="font-family: 'avertaregular';">Enter Your<br>location</h4>
                        <p class="mbr-text mbr-fonts-style display-7" style="font-family: 'avertaregular';">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Nibh ipsum consequat nisl vel pretium. Mauris pellentesque pulvinar&nbsp;</p>
                    </div>
                </div>

                <div class="card p-3 col-12 col-md-6 col-lg-3">
                    <div class="card-img pb-3">
                        <!-- <span class="mbri-touch mbr-iconfont"></span> -->
                    </div>
                    <div class="card-box">
                        <h4 style="font-family: 'avertaregular';" class="card-title py-3 mbr-fonts-style display-5">
                        Select<br>restoraunt</h4>
                        <p style="font-family: 'avertaregular';" class="mbr-text mbr-fonts-style display-7">
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Nibh ipsum consequat nisl vel pretium. Mauris pellentesque pulvinar
                        </p>
                    </div>
                </div>

                <div class="card p-3 col-12 col-md-6 col-lg-3">
                    <div class="card-img pb-3">
                        <!-- <span class="mbri-responsive mbr-iconfont"></span> -->
                    </div>
                    <div class="card-box">
                        <h4 style="font-family: 'avertaregular';" class="card-title py-3 mbr-fonts-style display-5">
                        Choose<br>meal</h4>
                        <p style="font-family: 'avertaregular';" class="mbr-text mbr-fonts-style display-7">
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Nibh ipsum consequat nisl vel pretium. Mauris pellentesque pulvinar
                        </p>
                    </div>
                </div>

                <div class="card p-3 col-12 col-md-6 col-lg-3">
                    <div class="card-img pb-3">
                        <!-- <span class="mbri-desktop mbr-iconfont"></span> -->
                    </div>
                    <div class="card-box">
                        <h4 style="font-family: 'avertaregular';" class="card-title py-3 mbr-fonts-style display-5">
                        Place<br>order</h4>
                        <p style="font-family: 'avertaregular';" class="mbr-text mbr-fonts-style display-7">
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Nibh ipsum consequat nisl vel pretium. Mauris pellentesque pulvinar
                        </p>
                    </div>
                </div>

            </div>

        </div>

    </section>

    <div class="search-div" style=" margin-left: 420px;padding-top: 90px;">

        <form action="" method="POST" class="mbr-form form-with-styler" data-form-title="Mobirise Form">

            <div class="dragArea row">
                <div class="form-group col" data-for="email" data-toggle="modal" data-target="#modalsearch">
                    <img src="assets/images/search.svg" style="margin-bottom: -69px;margin-left: 12px;">
                    <input type="text" name="search" placeholder="Search" style="border-radius: 25px;padding-left: 50px;width: 590px;background-color: #EBEBEB;" required="required" class="form-control display-7 search-media-input">

                </div>

            </div>
        </form>

    </div>

    <section class="features1  form3 cid-rNDsWcIaDh" id="features1-2a" style="background-color: white;">

        <div class="container">

            <div class="row py-2 justify-content-center">
                <div class="col-12 col-lg-6  col-md-8 " data-form-type="formoid">

                    <div class="container">

                        <hr style="width: 950px;margin-left: -220px;">
                        <div class="media-container-row">

                            <div class="card p-3 col-12 col-md-6 col-lg-3 favorite-media-img" data-toggle="modal" data-target="#modalFavorites">
                                <div class="card-img pb-3">
                                    <div class="favorite-icon">
                                        <span class="iconify" data-icon="bx:bxs-heart" data-inline="false" style="font-size: 45px;color: white;margin-top: 8px;"></span>
                                    </div>
                                </div>
                                <div class="card-box">
                                    <h5 style="font-family: 'avertaregular';">
                        Favorites
                    </h5>

                                </div>
                            </div>

                            <div class="card p-3 col-12 col-md-6 col-lg-3 breakfast-media-img">
                                <div class="card-img pb-3">
                                    <div class="favorite-icon">
                                        <img src="assets/images/Breakfast.svg">
                                        <!-- <span class="iconify" data-icon="bx:bxs-heart" data-inline="false" style="font-size: 45px;color: white;margin-top: 8px;"></span> -->
                                    </div>
                                </div>
                                <div class="card-box" style="width: 100px;">
                                    <h5 style="font-family: 'avertaregular';">
                        Breakfast
                    </h5>

                                </div>
                            </div>

                            <div class="card p-3 col-12 col-md-6 col-lg-3 lunch-media-img" data-toggle="modal" data-target="#modalLunch">
                                <div class="card-img pb-3">
                                    <div class="favorite-icon">
                                        <img src="assets/images/Lunch.svg">
                                    </div>
                                </div>
                                <div class="card-box">
                                    <h5 style="font-family: 'avertaregular';">
                        Lunch
                    </h5>

                                </div>
                            </div>

                            <div class="card p-3 col-12 col-md-6 col-lg-3 kitchen-media-img">
                                <div class="card-img pb-3">
                                    <div class="favorite-icon">
                                        <img src="assets/images/Kitchens.svg">
                                    </div>
                                </div>
                                <div class="card-box">
                                    <h5 style="font-family: 'avertaregular';">
                        Kitchen
                    </h5>

                                </div>
                            </div>

                            <div class="card p-3 col-12 col-md-6 col-lg-3 snack-media-img">
                                <div class="card-img pb-3">
                                    <div class="favorite-icon">
                                        <img src="assets/images/Snack.svg">
                                    </div>
                                </div>
                                <div class="card-box">
                                    <h5 style="font-family: 'avertaregular';">
                        Snack
                    </h5>

                                </div>
                            </div>

                            <div class="card p-3 col-12 col-md-6 col-lg-3 drink-media-img">
                                <div class="card-img pb-3">
                                    <div class="favorite-icon">
                                        <img src="assets/images/Drinks.svg">
                                    </div>
                                </div>
                                <div class="card-box">
                                    <h5 style="font-family: 'avertaregular';">
                                        Drinks
                                    </h5>
                                    <!-- <h5>
                        Dinner
                    </h5> -->

                                </div>
                            </div>

                        </div>
                        <hr style="width: 950px;margin-left: -220px;">

                    </div>

    </section>

    <section class="features2 cid-rNDt802JK9" id="features2-2c" style="background-color: #f5f5f59c;">

        <div class="container">
            <h6 style="margin-left: 20px;font-size: 20px;color: #4e4b4b;font-family: 'avertaregular';">
                Top of the Morning
            </h6>
            <h6 style="margin-left: 20px;font-size: 15px; color: #929292;font-family: 'avertaregular';">
                Some carefully curated meals to get your day started.
            </h6>
            <div class="media-container-row">
                <div class="card p-3 col-12 col-md-6 col-lg-3">
                    <div class="card-wrapper">
                        <div class="card-img">
                            <img src="assets/images/background1.jpg" alt="Mobirise" class="image-custom">
                        </div>

                        <div class="card-box mbr-overlay media-card-box" style="/*opacity: 0.8;background-color: #000000;*/background: linear-gradient(to bottom, #ffffff00 0%, #000000 100%);width: 248px;margin-left: 15px;height: 215px;margin-top: 20px;border-bottom-left-radius: 30px;border-bottom-right-radius: 30px;">
                            <h2 style="font-size: 20px;color: whitesmoke;margin-left: 10px;margin-top: 136px;font-family: 'avertaregular';">
                                Hausa Kooko with k..
                             </h2>
                            <h3 style="font-size: 15px;color: whitesmoke;margin-left: 10px;font-family: 'avertaregular';">
                                Breakfast to Breakfast - Osu
                             </h3>
                        </div>
                    </div>
                </div>

                <div class="card p-3 col-12 col-md-6 col-lg-3">
                    <div class="card-wrapper">
                        <div class="card-img">
                            <img src="assets/images/background2.jpg" alt="Mobirise" class="image-custom">
                        </div>
                        <div class="card-box mbr-overlay media-card-box" style="/*opacity: 0.8;background-color: #000000;*/background: linear-gradient(to bottom, #ffffff00 0%, #000000 100%);width: 248px;margin-left: 15px;height: 215px;margin-top: 20px;border-bottom-left-radius: 30px;border-bottom-right-radius: 30px;">
                            <h2 style="font-size: 20px;color: whitesmoke;margin-left: 10px;margin-top: 136px;font-family: 'avertaregular';">
                                Apple Chia Seeds S...
                             </h2>
                            <h3 style="font-size: 15px;color: whitesmoke;margin-left: 10px;font-family: 'avertaregular';">
                                Breakfast to Breakfast - Osu
                             </h3>
                        </div>
                    </div>
                </div>

                <div class="card p-3 col-12 col-md-6 col-lg-3">
                    <div class="card-wrapper">
                        <div class="card-img">
                            <img src="assets/images/background3.jpg" alt="Mobirise" class="image-custom">
                        </div>
                        <div class="card-box mbr-overlay media-card-box" style="opacity: 0.7;background-color: #000000; width: 245px;margin-left: 16px;height: 60px;margin-top: 177px;border-bottom-left-radius: 30px;border-bottom-right-radius: 30px;">
                            <h2 style="font-size: 20px;color: whitesmoke;margin-left: 10px;margin-top: -23px;font-family: 'avertaregular';">
                                Waakye
                             </h2>
                            <h3 style="font-size: 15px;color: whitesmoke;margin-left: 10px;font-family: 'avertaregular';">
                                Auntie Munies Waakye
                             </h3>
                        </div>
                    </div>
                </div>

                <div class="card p-3 col-12 col-md-6 col-lg-3">
                    <div class="card-wrapper">
                        <div class="card-img">
                            <img src="assets/images/background4.jpg" alt="Mobirise" class="image-custom">
                        </div>
                        <div class="card-box mbr-overlay media-card-box" style="opacity: 0.6;background-color: #000000; width: 245px;margin-left: 16px;height: 60px;margin-top: 177px;border-bottom-left-radius: 30px;border-bottom-right-radius: 30px;">
                            <h2 style="font-size: 20px;color: whitesmoke;margin-left: 10px;margin-top: -23px;font-family: 'avertaregular';">
                                Hausa Kooko with k..
                             </h2>
                            <h3 style="font-size: 15px;color: whitesmoke;margin-left: 10px;font-family: 'avertaregular';">
                                Breakfast to Breakfast - Osu
                             </h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container" style="padding-top: 45px;">
            <h6 style="margin-left: 20px;font-size: 20px;margin-top: 20px;color: #4e4b4b;font-family: 'avertaregular';">
                Restaurants near you
            </h6>
            <div class="media-container-row">
                <div class="card p-3 col-12 col-md-6 col-lg-3">
                    <div class="card-wrapper">
                        <div class="card-img">
                            <img src="assets/images/Lord-of-the-wings logo.jpg" alt="Mobirise" class="image-custom">
                        </div>
                        <div class="card-box mbr-overlay media-card-box " style="opacity: 0.6;background-color: #000000; width: 245px;margin-left: 16px;height: 60px;margin-top: 177px;border-bottom-left-radius: 30px;border-bottom-right-radius: 30px;">
                            <h2 style="font-size: 20px;color: whitesmoke;margin-left: 10px;margin-top: -23px;font-family: 'avertaregular';">
                                Second Cup Osu
                             </h2>
                            <h3 style="font-size: 15px;color: whitesmoke;margin-left: 10px;font-family: 'avertaregular';">
                                30 - 40 min
                             </h3>
                        </div>
                    </div>
                </div>

                <div class="card p-3 col-12 col-md-6 col-lg-3">
                    <div class="card-wrapper">
                        <div class="card-img">
                            <img src="assets/images/Chop-Italy.png" alt="Mobirise" class="image-custom">
                        </div>
                        <div class="card-box mbr-overlay media-card-box" style="opacity: 0.6;background-color: #000000; width: 245px;margin-left: 16px;height: 60px;margin-top: 177px;border-bottom-left-radius: 30px;border-bottom-right-radius: 30px;">
                            <h2 style="font-size: 20px;color: whitesmoke;margin-left: 10px;margin-top: -23px;font-family: 'avertaregular';">
                                Second Cup Osu
                             </h2>
                            <h3 style="font-size: 15px;color: whitesmoke;margin-left: 10px;font-family: 'avertaregular';">
                                30 - 40 min
                             </h3>
                        </div>
                    </div>
                </div>

                <div class="card p-3 col-12 col-md-6 col-lg-3">
                    <div class="card-wrapper">
                        <div class="card-img">
                            <img src="assets/images/Celsbridge-Bar _ Grill.png" alt="Mobirise" class="image-custom">
                        </div>
                        <div class="card-box mbr-overlay media-card-box" style="opacity: 0.6;background-color: #000000; width: 245px;margin-left: 16px;height: 60px;margin-top: 177px;border-bottom-left-radius: 30px;border-bottom-right-radius: 30px;">
                            <h2 style="font-size: 20px;color: whitesmoke;margin-left: 10px;margin-top: -23px;font-family: 'avertaregular';">
                                Second Cup Osu
                             </h2>
                            <h3 style="font-size: 15px;color: whitesmoke;margin-left: 10px;font-family: 'avertaregular';">
                                30 - 40 min
                             </h3>
                        </div>
                    </div>
                </div>

                <div class="card p-3 col-12 col-md-6 col-lg-3">
                    <div class="card-wrapper">
                        <div class="card-img">
                            <img src="assets/images/Hoo-gah.jpg" alt="Mobirise" class="image-custom">
                        </div>
                        <div class="card-box mbr-overlay media-card-box" style="opacity: 0.6;background-color: #000000; width: 245px;margin-left: 16px;height: 60px;margin-top: 177px;border-bottom-left-radius: 30px;border-bottom-right-radius: 30px;">
                            <h2 style="font-size: 20px;color: whitesmoke;margin-left: 10px;margin-top: -23px;font-family: 'avertaregular';">
                                Second Cup Osu
                             </h2>
                            <h3 style="font-size: 15px;color: whitesmoke;margin-left: 10px;font-family: 'avertaregular';">
                                30 - 40 min
                             </h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container" style="padding-top: 60px;">
            <h6 style="margin-left: 20px;font-size: 20px;margin-top: 20px;font-family: 'avertaregular';">
                Vegiterian friendly restaurants
            </h6>
            <div class="media-container-row">
                <div class="card p-3 col-12 col-md-6 col-lg-3">
                    <div class="card-wrapper">
                        <div class="card-img">
                            <img src="assets/images/kalabash-restaurant.jpg" alt="Mobirise" class="image-custom">
                        </div>
                        <div class="card-box mbr-overlay media-card-box" style="opacity: 0.6;background-color: #000000; width: 245px;margin-left: 16px;height: 60px;margin-top: 177px;border-bottom-left-radius: 30px;border-bottom-right-radius: 30px;">
                            <h2 style="font-size: 20px;color: whitesmoke;margin-left: 10px;margin-top: -23px;font-family: 'avertaregular';">
                                Second Cup Osu
                             </h2>
                            <h3 style="font-size: 15px;color: whitesmoke;margin-left: 10px;font-family: 'avertaregular';">
                                30 - 40 min
                             </h3>
                        </div>
                    </div>
                </div>

                <div class="card p-3 col-12 col-md-6 col-lg-3">
                    <div class="card-wrapper">
                        <div class="card-img">
                            <img src="assets/images/Urban-Grill.png" alt="Mobirise" class="image-custom">
                        </div>
                        <div class="card-box mbr-overlay media-card-box" style="opacity: 0.6;background-color: #000000; width: 245px;margin-left: 16px;height: 60px;margin-top: 177px;border-bottom-left-radius: 30px;border-bottom-right-radius: 30px;">
                            <h2 style="font-size: 20px;color: whitesmoke;margin-left: 10px;margin-top: -23px;font-family: 'avertaregular';">
                                Second Cup Osu
                             </h2>
                            <h3 style="font-size: 15px;color: whitesmoke;margin-left: 10px;font-family: 'avertaregular';">
                                30 - 40 min
                             </h3>
                        </div>
                    </div>
                </div>

                <div class="card p-3 col-12 col-md-6 col-lg-3">
                    <div class="card-wrapper">
                        <div class="card-img">
                            <img src="assets/images/barcelos-logo.jpg" alt="Mobirise" class="image-custom">
                        </div>
                        <div class="card-box mbr-overlay media-card-box" style="opacity: 0.6;background-color: #000000; width: 245px;margin-left: 16px;height: 60px;margin-top: 177px;border-bottom-left-radius: 30px;border-bottom-right-radius: 30px;">
                            <h2 style="font-size: 20px;color: whitesmoke;margin-left: 10px;margin-top: -23px;font-family: 'avertaregular';">
                                Second Cup Osu
                             </h2>
                            <h3 style="font-size: 15px;color: whitesmoke;margin-left: 10px;font-family: 'avertaregular';">
                                30 - 40 min
                             </h3>
                        </div>
                    </div>
                </div>

                <div class="card p-3 col-12 col-md-6 col-lg-3">
                    <div class="card-wrapper">
                        <div class="card-img">
                            <img src="assets/images/Champs-Sports-Bar-and-Grill.jpg" alt="Mobirise" class="image-custom">
                        </div>
                        <div class="card-box mbr-overlay media-card-box" style="opacity: 0.6;background-color: #000000; width: 245px;margin-left: 16px;height: 60px;margin-top: 177px;border-bottom-left-radius: 30px;border-bottom-right-radius: 30px;">
                            <h2 style="font-size: 20px;color: whitesmoke;margin-left: 10px;margin-top: -23px;font-family: 'avertaregular';">
                                Second Cup Osu
                             </h2>
                            <h3 style="font-size: 15px;color: whitesmoke;margin-left: 10px;font-family: 'avertaregular';">
                                30 - 40 min
                             </h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="features2 cid-rNDtm1hBwb" id="features2-2fs" style="background-color: #f5f5f59c;">

        <div class="container" data-toggle="modal" data-target="#modalDrink" >
            <img src="assets/images/drinks-flier.png" class="drinks-flier-media" style="width: 1100px;">

        </div>
    </section>

    <section class="features2 cid-rOr7Bnd4Pl" id="features15-2u" style="background-color: #f5f5f59c;">

        <div class="container">
            <h6 style="margin-left: 20px;font-size: 20px;margin-top: 20px;color: #5f5d5d;font-family: 'avertaregular';">
                Kibanda Dinning
            </h6>
            <h6 style="margin-left: 20px;font-size: 15px;color: #7f7e7e;font-family: 'avertaregular';">
                Order meals from local food joints and get it delivered to you now.
            </h6>
            <!-- <h6 style="margin-left: 1010px;margin-top: -30px;color: #58017D;font-size: 13px;">
                Sort by

                <i class="fa fa-angle-down"></i>
            </h6> -->
            <div class="media-container-row">
                <div class="card p-3 col-12 col-md-6 col-lg-4">
                    <div class="card-wrapper">
                        <div class="card-img">
                            <a href="restaurants.html">
                                <img src="assets/images/background1.jpg" alt="Mobirise" class="image-custom">
                            </a>
                        </div>

                        <div class="card-box row" style="padding-top: 40px;width: 450px;">

                            <div class="col-sm-6" style="margin-top: 10px;">
                                <img src="assets/images/Burger-King-logo.jpg" alt="Mobirise" class="round-small-image">
                                <h5 style="font-size: 15px;margin-left: 60px;margin-top: -30px;font-family: 'avertaregular';">
                                   Jans Kitchen
                                </h5>
                                <h5 style="font-size: 13px;color: #797878;margin-left: 60px;">

                                </h5>
                            </div>

                            <div class="col-sm-6" style="margin-left: -80px;">
                                <br>
                                <!-- <span class="iconify" data-icon="ant-design:star-filled" style="color: green;" data-inline="false"></span> -->
                                <!-- <h4 style="font-size: 13px;margin-left: 25px;margin-top: -15px;">4.6</h4>
                                <h4 style="font-size: 13px;margin-left: 50px;margin-top: -13px;">(242)</h4> -->
                                <h4 style="font-size: 13px;margin-left: 130px;margin-top: -13px;color: #888585;font-family: 'avertaregular';"> 45 mins</h4>
                                <!-- <h4 style="font-size: 13px;color: red;margin-left: 130px;margin-top: -35px;"> Closed</h4> -->
                            </div>

                        </div>
                    </div>
                </div>

                <div class="card p-3 col-12 col-md-6 col-lg-4">
                    <div class="card-wrapper">
                        <div class="card-img">
                            <a href="restaurants.html">
                                <img src="assets/images/background2.jpg" alt="Mobirise" class="image-custom">
                            </a>
                        </div>
                        <div class="card-box row" style="padding-top: 40px;width: 450px;">

                            <div class="col-sm-6" style="margin-top: 10px;">
                                <img src="assets/images/Firefly-Lounge-Bar-ghana.jpg" alt="Mobirise" class="round-small-image">
                                <h5 style="font-size: 15px;margin-left: 60px;margin-top: -30px;font-family: 'avertaregular';">
                                    FireFly Lounge Bar
                                </h5>
                                <h5 style="font-size: 13px;color: #797878;margin-left: 60px;">

                                </h5>
                            </div>

                            <div class="col-sm-6" style="margin-left: -80px;">
                                <br>
                                <!-- <span class="iconify" data-icon="ant-design:star-filled" style="color: green;" data-inline="false"></span> -->
                                <!-- <h4 style="font-size: 13px;margin-left: 25px;margin-top: -15px;">4.6</h4>
                                <h4 style="font-size: 13px;margin-left: 50px;margin-top: -13px;">(242)</h4> -->
                                <h4 style="font-size: 13px;margin-left: 130px;margin-top: -13px;color: #888585;font-family: 'avertaregular';"> 45 mins</h4>
                                <h4 class="closed-status-media" style="font-size: 13px;color: red;margin-left: 130px;margin-top: -35px;font-family: 'avertaregular';"> Closed</h4>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="card p-3 col-12 col-md-6 col-lg-4">
                    <div class="card-wrapper">
                        <!-- <div class="card-img">
                            <img src="assets/images/background3.jpg" alt="Mobirise" class="image-custom">
                        </div> -->

                        <div class="card_cont" style="height: 302px;width: 339px;margin-top: 16px;margin-left: 16px;border-radius: 25px;">
                            <h4 class="card-title display-5 py-2 mbr-fonts-style" style="font-size: 40px;color: white;margin-top: 20px;line-height: 1.3;font-family: 'avertaregular';">
                            Browse more <br>
                            Kibanda joints
                        </h4>

                            <button style="width: 90px;height: 32px;border-radius: 25px;margin-top: 33px;color: #58017D; border-color: #58017D;font-family: 'avertaregular';">
                                Browse
                            </button>
                        </div>

                    </div>
                </div>

            </div>
        </div>

    </section>

    <section class="features2 cid-rNDtm1hBwb" id="features2-2f" style="margin-top: -20px;background-color: #f5f5f59c;">

        <div class="container">
            <h6 style="margin-left: 20px;font-size: 20px;margin-top: 20px;color: #5f5d5d;font-family: 'avertaregular';">
                All restaurants
            </h6>
            <h6 style="margin-left: 20px;font-size: 15px;color: #7f7e7e;font-family: 'avertaregular';">
                Restaurants listed are the  nearest to your delivery location
            </h6>
            <h6 style="margin-left: 1010px;margin-top: -30px;color: #58017D;font-size: 13px;font-family: 'avertaregular';">
                Sort by

                <i class="fa fa-angle-down"></i>
            </h6>
            <div class="media-container-row">
                <div class="card p-3 col-12 col-md-6 col-lg-4">

                    <div class="card-wrapper">
                        <div class="card-img">
                            <a href="restaurants.html">
                                <img src="assets/images/background1.jpg" alt="Mobirise" class="image-custom">
                            </a>
                        </div>

                        <div class="card-box row" style="padding-top: 10px;width: 450px;">

                            <div class="col-sm-6 all-restaurant-media-div" style="margin-top: 10px;">
                                <img src="assets/images/Burger-King-logo.jpg" alt="Mobirise" class="round-small-image">
                                <h5 style="font-size: 15px;margin-left: 60px;margin-top: -40px;font-family: 'avertaregular';">
                                   Barcelos
                                </h5>
                                <h5 style="font-size: 13px;color: #797878;margin-left: 60px;font-family: 'avertaregular';">
                                    Category
                                </h5>
                            </div>

                            <div class="col-sm-6 all-restorant-rating-media" style="margin-left: -80px;">
                                <br>
                                <span class="iconify" data-icon="ant-design:star-filled" style="color: green;" data-inline="false"></span>
                                <h4 style="font-size: 13px;margin-left: 25px;margin-top: -15px;font-family: 'avertaregular';">4.6</h4>
                                <h4 style="font-size: 13px;margin-left: 50px;margin-top: -13px;font-family: 'avertaregular';">(242)</h4>
                                <h4 style="font-size: 13px;margin-left: 130px;margin-top: -13px;color: #888585;font-family: 'avertaregular';"> 45 mins</h4>
                            </div>

                        </div>
                    </div>

                </div>

                <div class="card p-3 col-12 col-md-6 col-lg-4">
                    <div class="card-wrapper">
                        <div class="card-img">
                            <a href="restaurants.html">
                                <img src="assets/images/background2.jpg" alt="Mobirise" class="image-custom">
                            </a>
                        </div>
                        <div class="card-box row" style="padding-top: 10px;width: 450px;">

                            <div class="col-sm-6 all-restaurant-media-div" style="margin-top: 10px;">
                                <img src="assets/images/Firefly-Lounge-Bar-ghana.jpg" alt="Mobirise" class="round-small-image">
                                <h5 style="font-size: 15px;margin-left: 60px;margin-top: -40px;font-family: 'avertaregular';">
                                    FireFly Lounge Bar
                                </h5>
                                <h5 style="font-size: 13px;color: #797878;margin-left: 60px;font-family: 'avertaregular';">
                                    Category
                                </h5>
                            </div>

                            <div class="col-sm-6 all-restorant-rating-media" style="margin-left: -80px;">
                                <br>
                                <span class="iconify" data-icon="ant-design:star-filled" style="color: green;" data-inline="false"></span>
                                <h4 style="font-size: 13px;margin-left: 25px;margin-top: -15px;font-family: 'avertaregular';">4.6</h4>
                                <h4 style="font-size: 13px;margin-left: 50px;margin-top: -13px;font-family: 'avertaregular';">(242)</h4>
                                <h4 style="font-size: 13px;margin-left: 130px;margin-top: -13px;color: #888585;font-family: 'avertaregular';"> 45 mins</h4>
                                <h4 class="status-media-closed" style="font-size: 13px;color: red;margin-left: 130px;margin-top: -35px;font-family: 'avertaregular';"> Closed</h4>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="card p-3 col-12 col-md-6 col-lg-4">
                    <div class="card-wrapper">
                        <div class="card-img">
                            <a href="restaurants.html">
                                <img src="assets/images/background3.jpg" alt="Mobirise" class="image-custom">
                            </a>
                        </div>
                        <div class="card-box row" style="padding-top: 10px;width: 450px;">

                            <div class="col-sm-6 all-restaurant-media-div" style="margin-top: 10px;">
                                <img src="assets/images/Firefly-Lounge-Bar-ghana.jpg" alt="Mobirise" class="round-small-image">
                                <h5 style="font-size: 15px;margin-left: 60px;margin-top: -40px;font-family: 'avertaregular';">
                                   Popaye
                                </h5>
                                <h5 style="font-size: 13px;color: #797878;margin-left: 60px;font-family: 'avertaregular';">
                                    Category
                                </h5>
                            </div>

                            <div class="col-sm-6 all-restorant-rating-media" style="margin-left: -80px;">
                                <br>
                                <span class="iconify" data-icon="ant-design:star-filled" style="color: green;" data-inline="false"></span>
                                <h4 style="font-size: 13px;margin-left: 25px;margin-top: -15px;font-family: 'avertaregular';">4.6</h4>
                                <h4 style="font-size: 13px;margin-left: 50px;margin-top: -13px;font-family: 'avertaregular';">(242)</h4>
                                <h4 style="font-size: 13px;margin-left: 130px;margin-top: -13px;color: #888585;font-family: 'avertaregular';"> 45 mins</h4>
                            </div>

                        </div>
                    </div>
                </div>

            </div>
        </div>
        <div class="container">
            <div class="media-container-row">
                <div class="card p-3 col-12 col-md-6 col-lg-4">
                    <div class="card-wrapper">
                        <div class="card-img">
                            <a href="restaurants.html">
                                <img src="assets/images/background1.jpg" alt="Mobirise" class="image-custom">
                            </a>
                        </div>
                        <div class="card-box row" style="padding-top: 10px;width: 450px;">

                            <div class="col-sm-6 all-restaurant-media-div" style="margin-top: 10px;">
                                <img src="assets/images/Burger-King-logo.jpg" alt="Mobirise" class="round-small-image">
                                <h5 style="font-family: 'avertaregular';font-size: 15px;margin-left: 60px;margin-top: -40px;">
                                   Barcelos
                                </h5>
                                <h5 style="font-family: 'avertaregular';font-size: 13px;color: #797878;margin-left: 60px;">
                                    Category
                                </h5>
                            </div>

                            <div class="col-sm-6 all-restorant-rating-media" style="margin-left: -80px;">
                                <br>
                                <span class="iconify" data-icon="ant-design:star-filled" style="color: green;" data-inline="false"></span>
                                <h4 style="font-family: 'avertaregular';font-size: 13px;margin-left: 25px;margin-top: -15px;">4.6</h4>
                                <h4 style="font-family: 'avertaregular';font-size: 13px;margin-left: 50px;margin-top: -13px;">(242)</h4>
                                <h4 style="font-family: 'avertaregular';font-size: 13px;margin-left: 130px;margin-top: -13px;color: #888585"> 45 mins</h4>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="card p-3 col-12 col-md-6 col-lg-4">
                    <div class="card-wrapper">
                        <div class="card-img">
                            <a href="restaurants.html">
                                <img src="assets/images/background2.jpg" alt="Mobirise" class="image-custom">
                            </a>
                        </div>
                        <div class="card-box row" style="padding-top: 10px;width: 450px;">

                            <div class="col-sm-6 all-restaurant-media-div" style="margin-top: 10px;">
                                <img src="assets/images/Firefly-Lounge-Bar-ghana.jpg" alt="Mobirise" class="round-small-image">
                                <h5 style="font-family: 'avertaregular';font-size: 15px;margin-left: 60px;margin-top: -40px;">
                                    FireFly Lounge Bar
                                </h5>
                                <h5 style="font-family: 'avertaregular';font-size: 13px;color: #797878;margin-left: 60px;">
                                    Category
                                </h5>
                            </div>

                            <div class="col-sm-6 all-restorant-rating-media" style="margin-left: -80px;">
                                <br>
                                <span class="iconify" data-icon="ant-design:star-filled" style="color: green;" data-inline="false"></span>
                                <h4 style="font-family: 'avertaregular';font-size: 13px;margin-left: 25px;margin-top: -15px;">4.6</h4>
                                <h4 style="font-family: 'avertaregular';font-size: 13px;margin-left: 50px;margin-top: -13px;">(242)</h4>
                                <h4 style="font-family: 'avertaregular';font-size: 13px;margin-left: 130px;margin-top: -13px;color: #888585"> 45 mins</h4>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="card p-3 col-12 col-md-6 col-lg-4">
                    <div class="card-wrapper">
                        <div class="card-img">
                            <a href="restaurants.html">
                                <img src="assets/images/background3.jpg" alt="Mobirise" class="image-custom">
                            </a>
                        </div>
                        <div class="card-box row" style="padding-top: 10px;width: 450px;">

                            <div class="col-sm-6 all-restaurant-media-div" style="margin-top: 10px;">
                                <img src="assets/images/Firefly-Lounge-Bar-ghana.jpg" alt="Mobirise" class="round-small-image">
                                <h5 style="font-family: 'avertaregular';font-size: 15px;margin-left: 60px;margin-top: -40px;">
                                   Frankies
                                </h5>
                                <h5 style="font-family: 'avertaregular';font-size: 13px;color: #797878;margin-left: 60px;">
                                    Category
                                </h5>
                            </div>

                            <div class="col-sm-6 all-restorant-rating-media" style="margin-left: -80px;">
                                <br>
                                <span class="iconify" data-icon="ant-design:star-filled" style="color: green;" data-inline="false"></span>
                                <h4 style="font-size: 13px;margin-left: 25px;margin-top: -15px;">4.6</h4>
                                <h4 style="font-size: 13px;margin-left: 50px;margin-top: -13px;">(242)</h4>
                                <h4 style="font-size: 13px;margin-left: 130px;margin-top: -13px;color: #888585"> 45 mins</h4>
                            </div>

                        </div>
                    </div>
                </div>

            </div>
            <button class="button-load-more" style="font-family: 'avertaregular';">
                Load More
            </button>

        </div>
    </section>

    <section class="features11 cid-rNDtCeDr5X" id="features11-2h">

        <div class="container">
            <div class="col-md-12">
                <div class="media-container-row">
                    <div class="mbr-figure m-auto" style="width: 50%;">
                        <img src="assets/images/Group order.jpg" alt="Mobirise" class="group-order-img-media" title="" style="margin-left: 140px;">
                    </div>
                    <div class=" align-left aside-content media-aside-content" style="margin-left: 130px;margin-top: 50px;">
                        <h2 class="mbr-title pt-2 mbr-fonts-style display-2" style="color: #58017D;">
                       <strong style="font-family: 'avertaregular';">
                        The Meal Tastes <br>
                        Better When We <br>
                        Eat Together. 
                    </strong>
                    </h2>
                        <div class="mbr-section-text">

                            <p style="font-family: 'avertaregular';" class="mbr-text mb-5 pt-3 mbr-light mbr-fonts-style display-4">
                                We understand that the best part of our
                                <br> lives are lived with the people we love.
                                <br>
                                <br> That is why we have created the Group
                                <br> Order feature so that you can stay
                                <br> connected together. Forever.
                                <br>
                                <br>

                                <h4 style="font-family: 'avertaregular';font-size: 20px;color: #58017D;margin-left: 48px;">How to use goup order</h4>
                            </p>
                            <div class="play-video-media">

                                <img src="assets/images/play-button.svg" style="margin-top: -78px;">
                            </div>

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="features11 cid-rNDtFNiRhT mbr-fullscreen" id="features11-2i" style="background-color: #F6F6F6;">

        <div class="container">
            <div class="col-md-12">
                <div class="media-container-row">
                    <div class="mbr-figure m-auto phone-mockup-media" style="width: 45%;">
                        <img src="assets/images/Phone mockups.png" alt="Mobirise" title="" style="margin-top: -50px;">
                    </div>
                    <div class=" align-left aside-content the-best-food-media" style="margin-left: 160px;">
                        <h2 class="mbr-title  mbr-fonts-style display-2 heading-best-food-media" style="color: #58017D;" style="font-size: 60px;">
                         <strong style="font-family: 'avertaregular';">
                        The Best Food <br>
                        Delivery App
                        </strong>
                    </h2>

                        <div class="block-content">
                            <div class="card">

                                <div class="card-box good-food-div-media" style="width: 460px;">
                                    <div >
                                    <p style="font-family: 'avertaregular';" class="block-text mbr-fonts-style display-7" style="font-size: 25px;margin-top: -80px;">
                                        <br>
                                        <br>
                                        <br> 
                                        Good food is meant to be shared and you can order from your favorite restaurant and enjoy tasty meals on the go using Homechow.
                                    </p>
                                    </div>
                                </div>

                                <img class="google-play-media" src="assets/images/google-play.png" style="width: 140px;">
                                <img class="apple-store-media" src="assets/images/apple-store.png" style="width: 140px;margin-left: 260px;margin-top: -51px;">
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="cid-rNDtMJWRWX" id="footer1-2j">

        <div class="container">
            <div class="media-container-row content text-white">
                <div class="col-12 col-md-2 mbr-fonts-style display-7">
                    <h5 style="font-family: 'avertaregular';" class="pb-2">
                        <br>
                    Support
                      </h5>
                    <p style="font-family: 'avertaregular';" class="mbr-text">
                        FAQ
                        <br>Help & Support
                        <br>Ride with us
                        <br> Careers
                        <br>
                        <br>
                    </p>
                    <h5 style="font-family: 'avertaregular';padding-top: 50px;">
                        Legal
                        <br>
                    </h5>
                    <p style="font-family: 'avertaregular';" class="mbr-text">
                        <br>Terms & Condition
                        <br>Privacy Policy
                        <br>RVendor Agreement
                    </p>
                </div>
                <div class="col-12 col-md-2 mbr-fonts-style display-7">
                    <h5 style="font-family: 'avertaregular';" class="pb-2">
                        <br>
                    Services
                </h5>
                    <p style="font-family: 'avertaregular';" class="mbr-text">
                        Booking
                        <br>Kitchen
                        <br>Marketplace
                        <br>Drinks
                    </p>

                    <h4 style="font-family: 'avertaregular';" class="comming-soon soon-media-market-place"> soon </h4>
                </div>
                <div class="col-12 col-md-2 mbr-fonts-style display-7">
                    <h5 style="font-family: 'avertaregular';" class="pb-2">
                    Popular<br>
                    categories
                </h5>
                    <p style="font-family: 'avertaregular';" class="mbr-text">
                        African
                        <br>Local
                        <br>Continental
                        <br>Lunch
                        <br>Breakfast
                        <br>Italian
                        <br>Drinks
                        <br>Fast food
                    </p>

                </div>
                <div class="col-12 col-md-2 mbr-fonts-style display-7">
                    <h5 style="font-family: 'avertaregular';" class="pb-2">
                    Operating
                    Countries
                </h5>
                    <p class="mbr-text" style="font-family: 'avertaregular';color: white;">
                        Ghana
                        <ui style="margin-top: -8px;">
                            <li class="ul-dash" style="font-family: 'avertaregular';padding-bottom: 7px;padding-left: 10px;"> Accra </li>
                            <li class="ul-dash" style="font-family: 'avertaregular';padding-bottom: 7px;padding-left: 10px;"> Kumasi </li>
                            <li class="ul-dash" style="font-family: 'avertaregular';padding-bottom: 7px;padding-left: 10px;">Takoradi </li>
                            <li class="ul-dash" style="font-family: 'avertaregular';padding-bottom: 7px;padding-left: 10px;">Tomale </li>
                            <!-- <li class="ul-dash">Cape Coast</li> -->
                            <li class="ul-dash" style="font-family: 'avertaregular';padding-bottom: 7px;padding-left: 10px;">Koforidua</li>
                        </ui>

                        <p class="mbr-text" style="font-family: 'avertaregular';margin-top: 54px;color: white;">
                            Nigeria
                            <ui>
                                <li class="ul-dash" style="font-family: 'avertaregular';margin-top: -10px;margin-left: 10px;"> Lagos </li>
                            </ui>
                            <h4 class="soon-media-1" style="font-family: 'avertaregular';font-size: 7px;background-color: #c56ac5;color: black;height: 16px;padding: 4px;width: 24px;text-align: center;border-radius: 50px;margin-left: 52px;margin-top: -47px;"> soon </h4>
                        </p>
                    </p>
                    <div style="margin-top: -165px;margin-left: -11px;">
                        <h4 class="soon-media-2" style="font-family: 'avertaregular';font-size: 7px;margin-left: 90px;background-color: #c56ac5;color: black;height: 16px;padding: 4px;width: 24px;text-align: center;border-radius: 50px;"> soon </h4>
                    </div>

                    <div style="margin-top: 10px;margin-left: -20px;">

                        <h4 class="soon-media-3" style="font-family: 'avertaregular';font-size: 7px;margin-left: 90px;background-color: #c56ac5;color: black;height: 16px;padding: 4px;width: 24px;text-align: center;border-radius: 50px;"> soon </h4>
                    </div>
                    <div style="margin-top: 10px;margin-left: -6px;">
                        <h4 class="soon-media-4" style="font-family: 'avertaregular';font-size: 7px;margin-left: 90px;background-color: #c56ac5;color: black;height: 16px;padding: 4px;width: 24px;text-align: center;border-radius: 50px;"> soon </h4>
                    </div>
                    <!-- <div style="margin-top: 12px;">
                        <h4 style="font-size: 7px;margin-left: 90px;background-color: #c56ac5;color: black;height: 16px;padding: 4px;width: 24px;text-align: center;border-radius: 50px;"> soon </h4>
                    </div> -->

                </div>
                <div class="col-12 col-md-2 mbr-fonts-style display-7">
                    <h5 class="pb-2" style="padding-top: 45px;">
                    <!-- Services -->
                      </h5>
                    <p class="mbr-text kenya-media" style="font-family: 'avertaregular';color: white;">
                        Kenya
                        <ui>
                            <li class="ul-dash" style="font-family: 'avertaregular';margin-top: -10px;margin-left: 5px;"> Nairobi </li>
                            <!-- <li class="ul-dash"> Mombasa </li> -->
                        </ui>
                        <!-- <h4 style="font-size: 8px;background-color: #c56ac5;width: 30px;text-align: center;border-radius: 50px;margin-left: 80px;margin-top: -13px;"> soon </h4> -->
                    </p>

                    <p class="mbr-text" style="font-family: 'avertaregular';margin-top: 30px;color: white;">
                        Ethiopia
                        <ui>
                            <li class="ul-dash" style="font-family: 'avertaregular';margin-top: -10px;margin-left: 5px;"> Adis Ababa </li>
                            <!-- <li class="ul-dash"> Mombasa </li> -->
                        </ui>
                        <h4 class="soon-media-5" style="font-family: 'avertaregular';font-size: 7px;background-color: #c56ac5;width: 24px;text-align: center;border-radius: 50px;margin-left: 55px;margin-top: -46px;height: 16px;padding: 4px;color: black;"> soon </h4>
                    </p>

                    <!-- <div style="font-size: 7px;background-color: #c56ac5;width: 24px;text-align: center;border-radius: 50px;margin-left: 61px;margin-top: -46px;height: 16px;padding: 4px;color: black;"></div> -->

                    <p class="mbr-text" style="font-family: 'avertaregular';margin-top: 55px;color: white;">
                        Rwanda
                        <ui>
                            <li class="ul-dash" style="font-family: 'avertaregular';margin-top: -10px;margin-left: 10px;"> Kigali </li>
                        </ui>
                        <h4 class="soon-media-6" style="font-family: 'avertaregular';font-size: 7px;background-color: #c56ac5;width: 24px;text-align: center;border-radius: 50px;margin-left: 55px;margin-top: -47px;color: black;height: 16px;padding: 4px;"> soon </h4>
                    </p>
                    <p class="mbr-text" style="font-family: 'avertaregular';margin-top: 54px;color: white;">
                        Uganda
                        <ui>
                            <li class="ul-dash" style="font-family: 'avertaregular';margin-top: -10px;margin-left: 10px;"> Kampala </li>
                        </ui>
                        <h4 class="soon-media-7" style="font-family: 'avertaregular';font-size: 7px;background-color: #c56ac5;color: black;height: 16px;padding: 4px;width: 24px;text-align: center;border-radius: 50px;margin-left: 52px;margin-top: -47px;"> soon </h4>
                    </p>
                </div>
            </div>
            <div class="footer-lower">
                <!-- <div class="media-container-row">
                    <div class="col-sm-12">
                        <hr>
                    </div>
                </div> -->
                <div class="media-container-row mbr-white">
                    <!-- <div class="col-sm-6 copyright">
                        <p class="mbr-text mbr-fonts-style display-7"></p>
                    </div> -->
                    <div>
                        <div class="social-list align-center">
                            <div class="soc-item">
                                <!-- <a href="" target="_blank"> -->
                                <span class="fa fa-twitter socicon mbr-iconfont mbr-iconfont-social"></span>
                                <!-- </a> -->
                            </div>
                            <div class="soc-item">
                                <!--  <a href="" target="_blank"> -->
                                <span class="fa fa-instagram socicon mbr-iconfont mbr-iconfont-social"></span>

                                <!-- </a> -->
                            </div>
                            <div class="soc-item">
                                <!-- <a href="" target="_blank"> -->
                                <span class="fa fa-facebook-square socicon mbr-iconfont mbr-iconfont-social"></span>
                                <!-- </a> -->
                            </div>
                            <div class="soc-item">
                                <!-- <a href="" target="_blank"> -->
                                <span class="fa fa-linkedin-square socicon mbr-iconfont mbr-iconfont-social"></span>
                                <!-- </a> -->
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section once="footers" class="cid-rNDtRVzGUV" id="footer6-2k">
        <div class="media-container-row">
            <div class="col-sm-12">
                <hr style="border-top: 1px solid #8ec8fb38;" class="hr-bottom-media">
            </div>
        </div>

        <div class="container">

            <div class="media-container-row align-center mbr-white">
                <!-- <div class="col-12"> -->
                <p class="mbr-text mb-0 mbr-fonts-style display-7" style="font-family: 'avertaregular';color: white;">
                    (C) 2020 All Rights Reserved <a href="http://homechow.io/"> Homechow.io </a> App Ltd.
                </p>
                <!-- </div> -->
            </div>
        </div>
    </section>

    <!-- Modal -->
    <div class="modal fade" id="modalFavorites" role="dialog" style="overflow-y: scroll;">
        <div class="modal-dialog ">

            <!-- Modal content-->
            <div class="modal-content" style="margin-top: 150px;">

                <div class="modal-header">
                    <button type="button" class="close" style="margin-left: 10px;" data-dismiss="modal">&times;</button>
                    <div>
                        <h4 style="font-family: 'avertaregular';font-size: 20px;text-align: center;margin-right: 185px;margin-top: 2px;">Favourites</h4>
                    </div>

                </div>

                <div style="background-color: #F6F6F6">

                    <div style="margin-left: 65px;margin-top: 29px;">
                        <img src="assets/images/banku-&-tilapia23.png" class="favorite-image-tilapia" style="width: 370px;height: 280px;border-radius: 25px;">
                    </div>

                </div>

                <div style="background-color: #F6F6F6;height: 125px;">
                    <div style="margin-left: 72px;margin-top: 25px;">
                        <img src="assets/images/Hotel_Honey_Restaurant _ Bar.png" style="width: 70px; height: 70px; border-radius: 25px">
                    </div>

                    <div data-toggle="modal" data-target="#modalRemoveFavorites">
                        <h4 style="font-family: 'avertaregular';font-size: 20px;margin-top: -60px;margin-left: 168px;">
                            Hotel Honey Restaurant...
                        </h4>

                        <span class="iconify" data-icon="ant-design:star-filled" style="color: green;margin-left: 167px;" data-inline="false"></span>
                        <h4 style="font-family: 'avertaregular';font-size: 17px;color: green;margin-left: 196px;margin-top: -16px;">4.6</h4>
                        <h4 style="font-family: 'avertaregular';color: #989898;font-size: 17px;margin-top: -26px;margin-left: 228px;">(242)</h4>
                        <h4 class="mins-40-media" style="font-family: 'avertaregular';font-size: 17px;color: #989898;margin-left: 361px;margin-top: -25px;">40 min</h4>
                    </div>
                </div>

            </div>

        </div>

    </div>

    <!-- Modal -->
    <div class="modal fade" id="modalLunch" role="dialog" style="overflow-y: scroll;">
        <div class="modal-dialog ">

            <!-- Modal content-->
            <div class="modal-content" style="margin-top: 150px;">

                <div class="modal-header">
                    <button type="button" class="close" style="margin-left: 10px;" data-dismiss="modal">&times;</button>
                    <div>
                        <h4 style="font-family: 'avertaregular';font-size: 20px;text-align: center;margin-right: 185px;margin-top: 2px;">Lunch</h4>
                    </div>

                </div>

                <div style="background-color: #F6F6F6">

                    <div style="margin-left: 65px;margin-top: 29px;">
                        <img src="assets/images/banku-&-tilapia23.png" style="width: 370px;height: 280px;border-radius: 25px;">
                    </div>

                </div>

                <div style="background-color: #F6F6F6;height: 125px;">
                    <div style="margin-left: 72px;margin-top: 25px;">
                        <img src="assets/images/Hotel_Honey_Restaurant _ Bar.png" style="width: 70px; height: 70px; border-radius: 25px">
                    </div>

                    <div>
                        <h4 style="font-family: 'avertaregular';font-size: 20px;margin-top: -60px;margin-left: 168px;">
                            Hotel Honey Restaurant...
                        </h4>

                        <span class="iconify" data-icon="ant-design:star-filled" style="color: green;margin-left: 167px;" data-inline="false"></span>
                        <h4 style="font-family: 'avertaregular';font-size: 17px;color: green;margin-left: 196px;margin-top: -16px;">4.6</h4>
                        <h4 style="font-family: 'avertaregular';color: #989898;font-size: 17px;margin-top: -26px;margin-left: 228px;">(242)</h4>
                        <h4 style="font-family: 'avertaregular';font-size: 17px;color: #989898;margin-left: 361px;margin-top: -25px;">40 min</h4>
                    </div>
                </div>

            </div>

        </div>

    </div>

    <!-- Modal -->
    <div class="modal fade" id="modalsearch" role="dialog" style="overflow-y: scroll;">
        <div class="modal-dialog modal-lg">

            <!-- Modal content-->
            <div class="modal-content" style="margin-top: 85px;width: 1152px;margin-left: -150px;">

                <div style="margin-top: 19px;">
                    <button type="button"  style="margin-left: 55px;margin-top: 4px;border-style: none;font-size: 25px;" data-dismiss="modal">&times;</button>

                    <div style="margin-top: 185px;margin-left: 80px;">
                        <h4 style="font-family: 'avertaregular';font-size: 15px">
                            Popular filters
                        </h4>
                    </div>

                    <div style="margin-left: 79px;margin-top: 30px;">
                        <h4 style="font-family: 'avertaregular';font-size: 13px;width: 140px;border-style: solid;height: 35px;padding-top: 10px;padding-left: 16px;border-radius: 25px;border-width: 1px;color: #58017D;">
                            GHC 10 - GHC 39
                        </h4>

                        <h4 style="font-family: 'avertaregular';font-size: 13px;margin-top: -32px;margin-left: 160px;">
                            GHC 40 - GHC 70
                        </h4>
                    </div>

                    <div style="margin-left: 79px;margin-top: 30px;">
                        <h4 style="font-family: 'avertaregular';font-size: 13px;width: 96px;border-style: solid;height: 35px;padding-top: 10px;padding-left: 16px;border-radius: 25px;border-width: 1px;color: #58017D;">
                            Vegetarian
                        </h4>

                        <h4 style="font-family: 'avertaregular';font-size: 13px;margin-top: -32px;margin-left: 116px;">
                           Discounts
                        </h4>

                        <h4 style="font-family: 'avertaregular';font-size: 13px;margin-top: -21px;margin-left: 194px;">
                            Nearest
                        </h4>

                        <h4 style="font-family: 'avertaregular';font-size: 13px;margin-top: -21px;margin-left: 268px;">
                            Top-rated
                        </h4>
                    </div>


                    <div style="margin-top: 55px;">
                        <h4 style="font-family: 'avertaregular';font-size: 15px;margin-left: 80px;">
                            Categories
                        </h4>
                    </div>

                    <div style="margin-left: 80px">
                        <h4 style="font-family: 'avertaregular';font-size: 15px;border-radius: 25px;border-style: solid;color: #58017D;border-width: 1px;width: 77px;height: 25px;padding-left: 13px;padding-top: 4px;margin-top: 13px;">
                            Alcohol
                        </h4>
                        <h4 style="font-family: 'avertaregular';font-size: 15px;margin-top: -27px;margin-left: 93px;">
                            BBQ
                        </h4>
                        <h4 style="font-family: 'avertaregular';font-size: 15px;margin-top: -32px;margin-left: 142px;width: 135px;border-style: solid;height: 30px;padding-top: 6px;padding-left: 11px;border-width: 1px;border-radius: 25px;color: #58017D;">
                            Club sandwiches
                        </h4>

                        <h4 style="font-family: 'avertaregular';font-size: 15px;margin-top: -31px;margin-left: 300px;">
                            Beef menu
                        </h4>
                    </div>

                     <div style="margin-left: 80px;margin-top: 35px;">
                        <h4 style="font-family: 'avertaregular';font-size: 15px;border-width: 1px;margin-top: 13px;">
                            Discounts
                        </h4>
                        <h4 style="font-family: 'avertaregular';font-size: 15px;margin-top: -23px;margin-left: 93px;">
                            Nearest
                        </h4>
                        <h4 style="font-family: 'avertaregular';font-size: 15px;margin-top: -32px;margin-left: 169px;width: 100px;border-style: solid;height: 30px;padding-top: 6px;padding-left: 11px;border-width: 1px;border-radius: 25px;color: #58017D;">
                            Vegetarian
                        </h4>

                        <h4 style="font-family: 'avertaregular';font-size: 15px;margin-top: -31px;margin-left: 281px;">
                            Top rated
                        </h4>
                    </div>

                    <div style="margin-left: 80px;margin-top: 35px;">
                        <h4 style="font-family: 'avertaregular';font-size: 15px;border-width: 1px;margin-top: 13px;">
                            Grills
                        </h4>
                        <h4 style="font-family: 'avertaregular';font-size: 15px;margin-top: -23px;margin-left: 54px;">
                            Desserts
                        </h4>
                        <h4 style="font-family: 'avertaregular';font-size: 15px;margin-top: -32px;margin-left: 124px;width: 68px;border-style: solid;height: 30px;padding-top: 6px;padding-left: 15px;border-width: 1px;border-radius: 25px;color: #58017D;">
                            Local
                        </h4>

                        <h4 style="font-family: 'avertaregular';font-size: 15px;margin-top: -31px;margin-left: 206px;">
                            Snacks
                        </h4>

                        <h4 style="font-family: 'avertaregular';font-size: 15px;margin-top: -23px;margin-left: 272px;">
                            Lebanese
                        </h4>

                        <h4 style="font-family: 'avertaregular';font-size: 15px;margin-top: -23px;margin-left: 351px;">
                            Stew
                        </h4>
                    </div>

                </div>

                <div style="background-color: #F6F6F6;width: 640px;margin-left: 510px;margin-top: -577px;">

                    <div style="margin-left: 105px;margin-top: 60px;">

                        <h4 class="modal-title" style="margin-right: 190px;font-size: 20px;font-family: 'avertaregular';">Search</h4>

                    </div>

                    <div style="margin-left: 105px; margin-top: 35px;">

                        <form action="" method="POST" class="mbr-form form-with-styler" data-form-title="Mobirise Form">

                            <div class="dragArea row">
                                <div class="form-group col" data-for="email">
                                    <input type="text" name="search" placeholder="Search" style="border-radius: 25px;border-color: #EEEAEA;padding-left: 50px;width: 410px;" required="required" class="form-control display-7">

                                </div>

                            </div>
                        </form>

                    </div>

                    <div>
                        <div style="margin-top: 25px;">
                            <img src="assets/images/banku-&-tilapia23.png" style="width: 412px;margin-left: 106px;height: 250px;border-radius: 25px;">
                        </div>

                        <div >
                            <div >
                            <img src="assets/images/Hotel_Honey_Restaurant _ Bar.png" style="margin-left: 108px;margin-top: 30px;">
                            </div>

                            <div style="margin-left: 80px;margin-top: -65px;margin-bottom: 110px;">
                        <h4 style="font-family: 'avertaregular';font-size: 20px;margin-top: -60px;margin-left: 168px;">
                            Hotel Honey Restaurant...
                        </h4>

                        <span class="iconify" data-icon="ant-design:star-filled" style="color: green;margin-left: 167px;" data-inline="false"></span>
                        <h4 style="font-family: 'avertaregular';font-size: 17px;color: green;margin-left: 196px;margin-top: -16px;">4.6</h4>
                        <h4 style="font-family: 'avertaregular';color: #989898;font-size: 17px;margin-top: -26px;margin-left: 228px;">(242)</h4>
                        <h4 style="font-family: 'avertaregular';font-size: 17px;color: #989898;margin-left: 361px;margin-top: -25px;">40 min</h4>
                    </div>
                        </div>
                    </div>

                </div>

            </div>

        </div>

    </div>

     <!-- Modal -->
    <div class="modal fade" id="modalDrink" role="dialog" style="overflow-y: scroll;">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content modal-media-drink" style="margin-top: 85px;width: 700px;background-color: #F6F6F6">

                <div class="modal-header">
                    <button type="button" class="close" style="margin-left: 0px;" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title" style="margin-right: 320px;font-size: 20px;">Drinks</h4>
                </div>

                <div style="margin-top: 40px;" class="title-drink-media">
                    <h4 style="font-family: 'avertaregular';font-size: 17px;margin-left: 30px;">
                        Wine
                    </h4>

                    <h4 class="view-all-media" style="font-family: 'avertaregular';font-size: 17px;color: #58017D;margin-top: -26px;margin-left: 585px;"data-dismiss="modal" data-for="email" data-toggle="modal" data-target="#modalViewAll">
                        View all
                    </h4>
                </div>

                <div style="margin-bottom: 80px;">
                    

                    <div class="drink-img-media-1" style="margin-left: 33px;margin-top: 20px;border-radius: 10px;width: 200px;background-color: white;height: 190px;padding-top: 15px;">
                        <img src="assets/images/pearly-bay-sparkling-wine-non-alcoholic_x700.png" style="margin-left: 28px;">
                        <div style="font-family: 'avertaregular';font-size: 17px;text-align: center;margin-top: 20px;">
                            Pearly Bay Sparkling Grape Juice
                        </div>
                    </div>

                    <div class="drink-img-media-2" style="padding-top: 15px;margin-top: -190px;margin-left: 258px; width: 200px;height: 189px;background-color: white;border-radius: 10px;">
                        <img src="assets/images/Layer1 1.png" style=" margin-left: 91px;">

                        <div style="font-family: 'avertaregular';font-size: 17px;text-align: center;margin-top: 28px;">
                            Challand Sans Alcool
                        </div>

                    </div>

                    <div class="drink-img-media-3" style="margin-top: -192px;margin-left: 484px;width: 200px;background-color: white;height: 189px;border-radius: 10px;padding-top: 15px;">
                        <img src="assets/images/Image 3.png" style="padding-left: 80px;padding-top: 14px;">

                    <div style="font-family: 'avertaregular';font-size: 17px;text-align: center;margin-top: 35px;" data-dismiss="modal" data-for="email" data-toggle="modal" data-target="#modalViewAll">
                        Red wine
                    </div>
                    </div>

                </div>

                <div style="margin-top: 40px;" class="title-drink-media">
                    <h4 style="font-family: 'avertaregular';font-size: 17px;margin-left: 30px;" >
                        Cider
                    </h4>

                    <h4 class="view-all-media" style="font-family: 'avertaregular';font-size: 17px;color: #58017D;margin-top: -26px;margin-left: 585px;">
                        View all
                    </h4>
                </div>

                <div style="margin-bottom: 80px;">
                    

                    <div class="drink-img-media-1"  style="margin-left: 33px;margin-top: 20px;border-radius: 10px;width: 200px;background-color: white;height: 190px;padding-top: 15px;">
                        <img src="assets/images/Image 4.png" style="margin-left: 28px;">
                        <div style="font-family: 'avertaregular';font-size: 17px;text-align: center;margin-top: 20px;">
                            Pearly Bay Sparkling Grape Juice
                        </div>
                    </div>

                    <div class="drink-img-media-2" style="padding-top: 15px;margin-top: -190px;margin-left: 258px; width: 200px;height: 189px;background-color: white;border-radius: 10px;">
                        <img src="assets/images/Image 5.png" style="width: 196px;height: 181px;margin-top: -10px;border-radius: 20px;">

                        <div style="font-family: 'avertaregular';font-size: 17px;text-align: center;margin-top: 28px;">
                            Challand Sans Alcool
                        </div>

                    </div>

                    <div class="drink-img-media-3" style="margin-top: -192px;margin-left: 484px;width: 200px;background-color: white;height: 189px;border-radius: 10px;padding-top: 15px;">
                        <img src="assets/images/Image 6@2x.png" style="width: 196px;height: 181px;border-radius: 25px;">

                    <div style="font-family: 'avertaregular';font-size: 17px;text-align: center;margin-top: 35px;">
                        Red wine
                    </div>
                    </div>

                </div>

            </div>

        </div>

    </div>


    <!-- Modal -->
    <div class="modal fade" id="modalViewAll" role="dialog" style="overflow-y: scroll;">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content" style="margin-top: 85px;width: 700px;background-color: #F6F6F6">

                <div class="modal-header">
                    <button type="button" class="close" style="margin-left: 0px;" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title" style="margin-right: 320px;font-size: 20px;">Wine</h4>
                </div>

                <div style="margin-top: 40px;">
                    <h4 style="font-family: 'avertaregular';font-size: 17px;margin-left: 65px;">
                        Wine
                    </h4>

                    <div>
                        

                        <div class="row" style="margin-top: 25px;">
                            
                                <div class="col-sm-6 image-drill-down-media" data-dismiss="modal" data-for="email" data-toggle="modal" data-target="#modalDrillDownImages">

                                    <div style="background-color: white;width: 196px;height: 189px;margin-left: 64px;padding-left: 20px;padding-top: 12px;border-radius: 10px;">
                                     <img src="assets/images/pearly-bay-sparkling-wine-non-alcoholic_x700.png" >
                                     </div>
                                </div>

                                <div class="col-sm-6 media-content-drill-down" style="margin-left: -40px;" data-dismiss="modal" data-for="email" data-toggle="modal" data-target="#modalDrillDownImages">

                                    <div style="width: 200px;text-align: center;padding-bottom: 15px;">

                                    <h4 style="font-family: 'avertaregular';font-size: 20px">
                                        Pearly Bay Sparkling Grape Juice
                                    </h4>

                                    </div>

                                    <div style="width: 270px;padding-bottom: 15px;">

                                    <p style="font-family: 'avertaregular';font-size: 17px">
                                        Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut
                                    </p>
                                    </div>


                                    <div>

                                    <h4 style="font-family: 'avertaregular';font-size: 20px">
                                        GHC 35
                                    </h4>
                                    </div>
                                    
                                </div>

                        </div>


                        <div class="row" style="margin-top: 50px;margin-bottom: 40px;">
                            
                                <div class="col-sm-6 image-drill-down-media" data-dismiss="modal" data-for="email" data-toggle="modal" data-target="#modalDrillDownImages">
                                    <div style="background-color: white;width: 196px;height: 189px;margin-left: 64px;padding-left: 20px;padding-top: 12px;border-radius: 10px; padding-left: 80px;padding-top: 25px;">
                                     <img src="assets/images/Layer1 1.png" >
                                     </div>
                                </div>

                                <div class="col-sm-6 media-content-drill-down" style="margin-left: -40px;" data-dismiss="modal" data-for="email" data-toggle="modal" data-target="#modalDrillDownImages">

                                    <div style="width: 200px;text-align: center;padding-bottom: 15px;">


                                    <h4 style="font-family: 'avertaregular';font-size: 20px">
                                        Challand Sans Alcool
                                    </h4>

                                </div>

                                <div style="width: 270px;padding-bottom: 15px;">

                                    <p style="font-family: 'avertaregular';font-size: 17px">
                                        Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut
                                    </p>

                                </div>

                                    <h4 style="font-family: 'avertaregular';font-size: 20px">
                                        GHC 35
                                    </h4>
                                    
                                </div>

                        </div>


                    </div>

                    
                </div>

            </div>

        </div>

    </div>


    <!-- Modal -->
    <div class="modal fade" id="modalDrillDownImages" role="dialog" style="overflow-y: scroll;">
        <div class="modal-dialog">

            

            <!-- Modal content-->
            <div class="modal-content single-image-media" style="margin-top: 85px;width: 700px;">

                <div class="modal-header">
                    <!-- <button type="button" class="close" style="margin-left: 0px;" data-dismiss="modal">&times;</button> -->
                    <img src="assets/images/arrow-left.svg" data-dismiss="modal">
                </div>

                <div style="height: 240px;" class="image-drill-media">
                    <img src="assets/images/pearly-bay-sparkling-wine-non-alcoholic_x700.png" style="margin-left: 275px;margin-top: 50px;">
                </div>

                
                    <div style="border-color: #EEEAEA;background-color: #F6F6F6">
                        <div style=" margin-top: 49px;margin-left: 83px;">
                            <h4 style="font-family: 'avertaregular';font-size: 20px">
                                Pearly Bay Sparkling Grape Juice
                            </h4>

                            <h4 style="font-family: 'avertaregular';font-size: 20px;margin-top: -28px;margin-left: 445px;">
                                GHC 35
                            </h4>
                        </div>

                        <div class="content-media-drilldown" style="margin-left: 85px; margin-top: 40px;padding-right: 120px;">
                            <p style="font-family: 'avertaregular';font-size: 17px">
                                Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam
                                
                            </p>
                        </div>

                        <div class="content-counte-media" style="font-family: 'avertaregular';background-color: #fff;width: 185px;height: 50px;margin-bottom: 40px;border-radius: 11px;margin-left: 265px;margin-top: 65pxpx;border-style: solid;border-width: 1px;border-color: #C8C8C8;">

                    <div style=" margin-top: 15px;margin-left: 20px;">
                    <i class="fa fa-minus" aria-hidden="true" style="color: #58017D"></i>
                    </div>

                    <div style=" margin-left: 87px;margin-top: -24px;">
                    <h4 style="color: #58017D; font-size: 20px">
                        3
                    </h4>
                    </div>

                    <div style="margin-left: 151px;margin-top: -30px;">

                    <i class="fa fa-plus" aria-hidden="true" style="color: #58017D"></i>
                    </div>
                </div>


                <div class="submit-button-media" style="background-color: #58017D;width: 235px;height: 45px;margin-bottom: 40px;margin-left: 250px;border-radius: 40px;">

                    <div style="padding-top: 10px;margin-left: 25px;">
                    <button style="font-family: 'avertaregular';background-color: #f0f8ff00;color: white;font-size: 15px;border-style: none;">
                        GHC 119
                    </button>
                    </div>

                    <div style="margin-left: 116px;margin-top: -25px;">

                    <button style="font-family: 'avertaregular';background-color: #f0f8ff00;color: white;font-size: 15px;border-style: none;">
                        Add to basket
                    </button>

                    </div>
                    
                    <div style="background-color: white;width: 2px;height: 32px;margin-left: 105px;margin-top: -28px;">
                        
                    </div>

                    
                </div>
                    </div>
                </div>
           

        </div>


    </div>

    <!-- Modal -->
    <div class="modal fade" id="modalRemoveFavorites" role="dialog" style="overflow-y: scroll;">
        <div class="modal-dialog ">

            <!-- Modal content-->
            <div class="modal-content" style="margin-top: 285px;width: 340px;border-radius: 25px;margin-left: 81px;">

                <div style="margin-top: 50px;margin-left: 13px;">
                    <p style="font-family: 'avertaregular';font-size: 17px">
                        Are you sure you want to remove Kalabash Restaurant from your favourite restaurants?
                    </p>
                </div>

                <div>
                    <button style="font-family: 'avertaregular';color: #ED0000;font-size: 17px;margin-left: 125px;margin-top: 20px;border-style: none;margin-bottom: 12px;">
                        Yes, remove
                    </button>
                </div>

                <div>
                    <button style="font-family: 'avertaregular';font-size: 17px;margin-left: 108px;border-style: none;margin-bottom: 20px;">
                        No, don’t remove
                    </button>
                </div>

            </div>

        </div>

    </div>

    <!-- Modal -->
    <div class="modal fade" id="modalPasswordSetUp" role="dialog" style="overflow-y: scroll;">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content" style="border-radius: 20px;">
                <div class="modal-header">
                    <!-- <button type="button" class="close" style="margin-left: 10px;" data-dismiss="modal">&times;</button> -->
                    <h4 class="modal-title" style="font-size: 20px;text-align: center;margin-left: 170px;margin-top: 26px;margin-bottom: 24px;">Password</h4>

                </div>

                <div style="margin-left: 46px;width: 400px;padding-bottom: 0px;">
                    <h4 style="font-family: 'avertaregular';font-size: 15px"> 

                     A temporary password has been sent to your phone. Please enter it below.

                    </h4>
                </div>

                <form action="#">

                    <label style="font-family: 'avertaregular';margin-left: 55px;margin-top: 20px;">Temporary password</label>

                    <div class="form-row" style="margin-left: 40px;margin-right: 40px;">
                        <div class="col">
                            <input type="password" class="form-control" style="border-radius: 10px;margin-right: 40px;width: 330px;" name="tempPassword">
                        </div>

                    </div>

                    <label style="font-family: 'avertaregular';margin-left: 55px;margin-top: 20px;">New password</label>

                    <div class="form-row" style="margin-left: 40px;margin-right: 40px;">
                        <div class="col">
                            <input type="password" class="form-control" style="border-radius: 10px;margin-right: 40px;width: 330px;" name="newPassword">
                        </div>

                    </div>
                    <label style="font-family: 'avertaregular';margin-left: 55px;margin-top: 20px;">Confirm new password</label>

                    <div class="form-row" style="margin-left: 40px;margin-right: 40px;">
                        <div class="col">
                            <input type="password" class="form-control" style="border-radius: 10px;margin-right: 40px;width: 330px;" name="confirmNewPassowrd">
                        </div>

                    </div>
                    <div style="margin-left: 170px;padding-bottom: 10px;padding-top: 10px;">
                        <button style="font-family: 'avertaregular';" type="button" class="btn btn-default button-continue" data-dismiss="modal">Continue</button>
                    </div>

                    <div>
                        <h3 style="font-family: 'avertaregular';" class="have-an-account">
                           Didn’t receive temporary password? <a style="color: #58017D; font-family: 'avertabold';">Resend</a>
                        </h3>
                    </div>

                </form>

            </div>

        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="modalSignUp" role="dialog" style="overflow-y: scroll;">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content" style="border-radius: 20px;">
                <div class="modal-header">
                    <button type="button" class="close" style="margin-left: 10px;" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title" style="font-family: 'avertaregular';margin-right: 190px;font-size: 20px;">Sign up</h4>
                </div>
                <form action="#">

                    <label style="font-family: 'avertaregular';margin-left: 55px;color: #707070;margin-top: 20px;">Name</label>

                    <div class="form-row" style="font-family: 'avertaregular';margin-left: 40px;margin-right: 40px;">
                        <div class="col">
                            <input type="text" class="form-control" style="border-radius: 10px;margin-right: 40px;" placeholder="Enter first name" name="email">
                        </div>

                    </div>

                    <label style="margin-left: 55px;font-family: 'avertaregular';color: #707070;margin-top: 20px;">Email</label>

                    <div class="form-row" style="margin-left: 40px;margin-right: 40px;">
                        <div class="col">
                            <input type="text" class="form-control" style="border-radius: 10px;margin-right: 40px;" placeholder="Email" name="email">
                        </div>

                    </div>
                    <label style="margin-left: 55px;color: #707070;font-family: 'avertaregular';margin-top: 20px;">Mobile number</label>

                    <div class="form-row" style="margin-left: 40px;margin-right: 40px;">
                        <div class="col">
                            <input type="text" class="form-control" style="border-radius: 10px;margin-right: 40px;" placeholder="Mobile number" name="email">
                        </div>

                    </div>
                    <div style="margin-left: 170px;padding-bottom: 10px;font-family: 'avertaregular';padding-top: 10px;">
                        <button type="button" class="btn btn-default button-continue" >Continue</button>
                    </div>

                    <div>
                        <h3 style="font-family: 'avertaregular';" class="have-an-account">
                            Already have an account? <a style="font-family: 'avertaregular';color: #58017D;" data-toggle="modal" data-dismiss="modal" data-target="#modalLogin"> <strong> Login </strong></a>
                        </h3>
                    </div>

                </form>

                <div>
                    <hr>
                    <h3 class="have-an-account" style="font-family: 'avertaregular';">or sign up using Social media</h3>
                    <div style="margin-left: 190px;margin-bottom: 30px;">
                        <span class="iconify" data-icon="bx:bxl-facebook-circle" style="font-family: 'avertaregular';font-size: 30px;color: #5757ec;" data-inline="false"></span>
                        <span class="iconify" data-icon="ant-design:google-circle-filled" style="font-family: 'avertaregular';font-size: 30px; color: red;" data-inline="false"></span>
                        <span class="iconify" data-icon="ant-design:twitter-circle-filled" style="font-family: 'avertaregular';font-size: 30px;color: #3a3af7cf;" data-inline="false"></span>
                    </div>
                </div>

                <div style="padding-bottom: 10px;">
                    <h3 class="have-an-account" style="font-family: 'avertaregular';text-align: center;margin-left: -10px;padding-bottom: 10px;">
                        By continuing, you agree to our<br>
                        <a style="font-family: 'avertaregular';color: #58017D;" href="Terms & Conditions.html"> <strong> Terms of use,</strong></a>
                        <a style="font-family: 'avertaregular';color: #58017D;" href="Terms & Conditions.html"> <strong> Privacy policy </strong></a> and 
                        <a style="cfont-family: 'avertaregular';olor: #58017D;"> <strong> Vendor </strong></a>
                    </h3>
                </div>

            </div>

        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="modalAllOrders" role="dialog" style="overflow-y: scroll;">
        <div class="modal-dialog ">

            <!-- Modal content-->
            <div class="modal-content modal-all-order-form">
                <div class="modal-header" style="margin-top: 35px;">
                    <button type="button" class="close" style="margin-left: 10px;background-color: white;border-radius: 25px;width: 15px;height: 15px;margin-top: 3px;" data-dismiss="modal">
                        <h3 style="font-size: 20px;margin-top: -12px;margin-left: -5px;">x</h3>
                    </button>
                    <!-- <h4 class="modal-title" style="margin-right: 115px;font-size: 20px;">Edit profile</h4> -->
                    <div class="modal-div-all-orders-active">
                        <button style="font-family: 'avertaregular';" class="modal-button-all-orders-active">
                            Active orders
                        </button>
                        <button style="font-family: 'avertaregular';" class="modal-button-all-orders-active" style="background-color: white;border-color: white;color: #C8C8C8;">
                            Previous orders
                        </button>
                        <button style="font-family: 'avertaregular';" class="modal-button-all-orders-active" style="background-color: white;border-color: white;color: #C8C8C8;">
                            Group orders
                        </button>
                    </div>

                </div>

                <div style="height: 100px;width: 410px;background-color: white;border-radius: 25px;margin-left: 42px;margin-top: 31px;">

                    <div>
                        <img src="assets/images/siMhabW0AA5BWx.jpg" style="width: 80px;border-radius: 25px;margin-top: 10px;margin-left: 10px;">
                    </div>

                    <div style="width: 275px;margin-left: 115px;margin-top: -77px;">

                        <h4 style="font-family: 'avertaregular';font-size: 15px;padding-bottom: 14px;">
                            Banku and Tilapia
                        </h4>
                        <h4 style="font-family: 'avertaregular';color: #5A5653;font-size: 12px;">
                            Lunch City
                        </h4>
                        <h4 style="font-family: 'avertaregular';color: #5A5653;font-size: 12px;">
                            GHC 35
                        </h4>
                        <h4 style="text-align: right;margin-top: -87px;margin-bottom: 42px;">
                            <i class="fa fa-angle-right"></i>
                        </h4>
                        <h4 style="font-family: 'avertaregular';font-size: 12px;text-align: right;">
                            Picked up by rider
                        </h4>
                    </div>

                </div>

            </div>

        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="modalEditProfile" role="dialog" style="overflow-y: scroll;">
        <div class="modal-dialog modal-sm">

            <!-- Modal content-->
            <div class="modal-content" style="border-radius: 20px;width: 360px;">
                <div class="modal-header">
                    <button type="button" class="close" style="margin-left: 10px;" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title" style="font-family: 'avertaregular';margin-right: 115px;font-size: 20px;">Edit profile</h4>
                </div>
                <form action="#">

                    <label style="font-family: 'avertaregular';margin-left: 55px;color: #707070;margin-top: 20px;">Name</label>

                    <div class="form-row" style="margin-left: 40px;margin-right: 40px;">
                        <div class="col">
                            <input type="text" class="form-control" style="border-radius: 10px;margin-right: 40px;" placeholder="Enter first name" name="email">
                        </div>

                    </div>

                    <label style="font-family: 'avertaregular';margin-left: 55px;color: #707070;margin-top: 20px;">Email</label>

                    <div class="form-row" style="margin-left: 40px;margin-right: 40px;">
                        <div class="col">
                            <input type="text" class="form-control" style="border-radius: 10px;margin-right: 40px;" placeholder="Email" name="email">
                        </div>

                    </div>
                    <label style="font-family: 'avertaregular';margin-left: 55px;color: #707070;margin-top: 20px;">Mobile number</label>

                    <div class="form-row" style="margin-left: 40px;margin-right: 40px;">
                        <div class="col">
                            <input type="text" class="form-control" style="border-radius: 10px;margin-right: 40px;" placeholder="Mobile number" name="email">
                        </div>

                    </div>
                    <div style="margin-left: 115px;padding-bottom: 10px;padding-top: 10px;">
                        <button style="font-family: 'avertaregular';" type="button" class="btn btn-default button-continue">Save</button>
                    </div>

                    <div>
                        <h4 style="color: #01A517;font-family: 'avertaregular';font-size: 12px;margin-left: 96px;padding-bottom: 20px;padding-top: 20px;">
                            Profile successfully updated.
                        </h4>
                    </div>
                </form>

            </div>

        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="modalLogin" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content" style="border-radius: 20px;">
                <div class="modal-header">
                    <button type="button" class="close" style="margin-left: 10px;" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title" style="font-family: 'avertaregular';margin-right: 190px;font-size: 20px;">Log in</h4>
                </div>
                <form action="#">

                    <label style="margin-left: 55px;font-family: 'avertaregular';color: #707070;margin-top: 20px;">Phone number or email</label>

                    <div class="form-row" style="margin-left: 40px;margin-right: 40px;">
                        <div class="col">
                            <input type="text" class="form-control" style="border-radius: 10px;margin-right: 40px;" placeholder="Enter Phone numer or email here...." name="email">
                        </div>

                    </div>

                    <label style="margin-left: 55px;color: #707070;font-family: 'avertaregular';margin-top: 20px;">Password</label>

                    <div class="form-row" style="margin-left: 40px;margin-right: 40px;">
                        <div class="col">
                            <input type="password" class="form-control" style="border-radius: 10px;margin-right: 40px;" placeholder="" name="email">
                        </div>

                    </div>
                    <div style="margin-left: 170px;padding-bottom: 10px;padding-top: 10px;">
                        <button style="font-family: 'avertaregular';" type="button" class="btn btn-default button-continue">Log in</button>
                    </div>

                    <div>
                        <h3 class="have-an-account">
                            <a style="font-family: 'avertaregular';color: #58017D;"> <strong> Forgot Password </strong></a>
                        </h3>
                    </div>
                    <div>
                        <h3 style="font-family: 'avertaregular';" class="have-an-account">
                            Don't have an account <a style="font-family: 'avertaregular';color: #58017D;" data-toggle="modal" data-dismiss="modal" data-target="#modalSignUp" > <strong> Sign Up </strong></a>
                        </h3>
                    </div>

                </form>

            </div>

        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="modalMyRatingHistory" role="dialog" style="overflow-y: scroll;">
        <div class="modal-dialog ">

            <!-- Modal content-->
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" style="margin-left: 10px;" data-dismiss="modal">&times;</button>
                    <div>
                        <h4 style="font-family: 'avertaregular';font-size: 20px;text-align: center;margin-right: 150px;margin-top: 20px;">My ratings history</h4>
                    </div>

                </div>

                <div>
                    <h4 style="font-family: 'avertaregular';font-size: 20px;margin-left: 113px;margin-top: 10px;margin-bottom: 20px;">
                       You have given 117 ratings & reviews
                    </h4>
                </div>

                <div class="row" style="width: 330px;margin-left: 115px;padding-bottom: 20px;">

                    <div class="col-sm-12" style="background-color:#EFEFEF;margin-top: 5px;margin-bottom: 20px;border-radius: 11px;">
                        <h4 style="font-family: 'avertaregular';font-size: 17px;margin-top: 15px;">
                            Hoogah Osu
                        </h4>

                        <div style="margin-top: -34px;margin-left: 200px;">
                            <span class="iconify" data-icon="ant-design:star-filled" data-inline="false" style="color: green"></span>
                            <span class="iconify" data-icon="dashicons:star-filled" data-inline="false" style="color: green"></span>
                            <span class="iconify" data-icon="dashicons:star-filled" data-inline="false" style="color: green"></span>
                            <span class="iconify" data-icon="dashicons:star-filled" data-inline="false" style="color: green"></span>
                            <span class="iconify" data-icon="dashicons:star-filled" data-inline="false" style="color: green"></span>
                        </div>

                        <div style="font-family: 'avertaregular';font-size: 15px;margin-top: 25px;">
                            The quick, brown fox jumps over a lazy dog. DJs flock by when MTV ax quiz prog. Junk MTV quiz graced by fox whelps. Bawds jog, flick quartz
                        </div>

                        <div style="margin-top: 20px;padding-bottom: 10px;">
                            <img src="assets/images/like.svg" style="margin-top: -7px;"> 12
                            <img src="assets/images/dislike.svg" style="padding-left: 10px;margin-top: -6px;"> 2

                            <h4 style="font-family: 'avertaregular';color: #707070;font-size: 15px;margin-left: 210px;margin-top: -20px;">
                            24 Jan 2020
                        </h4>
                        </div>

                    </div>

                    <div class="col-sm-12" style="background-color:#EFEFEF;margin-top: 5px;margin-bottom: 20px;border-radius: 11px;">
                        <h4 style="font-family: 'avertaregular';font-size: 17px;margin-top: 15px;">
                           The Piano Bar
                        </h4>

                        <div style="margin-top: -34px;margin-left: 200px;">
                            <span class="iconify" data-icon="ant-design:star-filled" data-inline="false" style="color: green"></span>
                            <span class="iconify" data-icon="dashicons:star-filled" data-inline="false" style="color: green"></span>
                            <span class="iconify" data-icon="dashicons:star-filled" data-inline="false" style="color: green"></span>
                            <span class="iconify" data-icon="dashicons:star-filled" data-inline="false" style="color: green"></span>
                            <span class="iconify" data-icon="dashicons:star-filled" data-inline="false" style="color: green"></span>
                        </div>

                        <div style="font-family: 'avertaregular';font-size: 15px;margin-top: 25px;">
                            The quick, brown fox jumps over a lazy dog. DJs flock by when MTV ax quiz prog. Junk MTV quiz graced by fox whelps. Bawds jog, flick quartz
                        </div>

                        <div style="margin-top: 20px;padding-bottom: 10px;">
                            <img src="assets/images/like.svg" style="font-family: 'avertaregular';margin-top: -7px;"> 12
                            <img src="assets/images/dislike.svg" style="font-family: 'avertaregular';padding-left: 10px;margin-top: -6px;"> 2

                            <h4 style="font-family: 'avertaregular';color: #707070;font-size: 15px;margin-left: 210px;margin-top: -20px;">
                            24 Jan 2020
                        </h4>
                        </div>

                    </div>

                    <div class="col-sm-12" style="background-color:#EFEFEF;margin-top: 5px;margin-bottom: 20px;border-radius: 11px;">
                        <h4 style="font-family: 'avertaregular';font-size: 17px;margin-top: 15px;">
                            Bloom Bar
                        </h4>

                        <div style="margin-top: -34px;margin-left: 200px;">
                            <span class="iconify" data-icon="ant-design:star-filled" data-inline="false" style="color: green"></span>
                            <span class="iconify" data-icon="dashicons:star-filled" data-inline="false" style="color: green"></span>
                            <span class="iconify" data-icon="dashicons:star-filled" data-inline="false" style="color: green"></span>
                            <span class="iconify" data-icon="dashicons:star-filled" data-inline="false" style="color: green"></span>
                            <span class="iconify" data-icon="dashicons:star-filled" data-inline="false" style="color: green"></span>
                        </div>

                        <div style="font-family: 'avertaregular';font-size: 15px;margin-top: 25px;">
                            The quick, brown fox jumps over a lazy dog. DJs flock by when MTV ax quiz prog. Junk MTV quiz graced by fox whelps. Bawds jog, flick quartz
                        </div>

                        <div style="margin-top: 20px;padding-bottom: 10px;">
                            <img src="assets/images/like.svg" style="font-family: 'avertaregular';margin-top: -7px;"> 12
                            <img src="assets/images/dislike.svg" style="font-family: 'avertaregular';padding-left: 10px;margin-top: -6px;"> 2

                            <h4 style="font-family: 'avertaregular';color: #707070;font-size: 15px;margin-left: 210px;margin-top: -20px;">
                            24 Jan 2020
                        </h4>
                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>

    <!-- Modal -->
    <div class="modal fade" id="modalMenuLogin" role="dialog" style="overflow-y: scroll;">
        <div class="modal-dialog modal-sm">

            <!-- Modal content-->
            <div class="modal-content media-menu-login" style="border-radius: 19px;width: 370px;">

                <div class="row" style="margin-top: 35px;margin-left: 10px;">

                    <div class="col-sm-6">
                        <h4 style="font-family: 'avertaregular';font-size: 15px;">
                    Hope Adoli
                   </h4>
                        <h4 style="font-family: 'avertaregular';font-size: 10px;">
                    0242267686
                   </h4>

                        <h4 style="font-family: 'avertaregular';font-size: 10px;">
                    kafui@loudrsocio.co
                   </h4>
                    </div>

                    <div class="col-sm-6 media-edit-button">
                        <button style="font-family: 'avertaregular';" class="menu-button-edit" data-toggle="modal" data-dismiss="modal" data-target="#modalEditProfile">
                            Edit
                        </button>
                    </div>

                </div>

                <!-- <div class="row current-balance" style="margin-top: 15px;margin-left: 25px;"> -->
                <div class="current-balance-parent-div">

                    <div class="current-balance-child1-div">
                        <h4 style="font-family: 'avertaregular';" class="current-balance-h4-1">
                    Current Balance
                   </h4>
                        <h4 style="font-family: 'avertaregular';font-size: 15px;color: black;">
                    <strong> GHC 200.46</strong>
                   </h4>

                    </div>

                    <div class="curent-balance-child2-div">
                        <button style="font-family: 'avertaregular';" class="button-top-up">
                            Top up
                        </button>
                    </div>

                </div>

                <div class="row" style="
                margin-top: 15px;
                margin-left: 10px;">

                    <div class="col-sm-6">
                        <i class="fa fa-heart-o"></i>

                    </div>

                    <div class="col-sm-6">
                        <h4 class="referFriendId" style="font-family: 'avertaregular';font-size: 15px;margin-left: -130px;">
                          Refer a friend
                             </h4>
                    </div>

                    <hr style="width: 318px;margin-left: 10px;">

                </div>

                <div class="row" style="
                margin-top: 15px;
                margin-left: 10px;">

                    <div class="col-sm-6" style="margin-top: -7px;">
                        <!-- <span class="iconify" data-icon="uil:cart" data-inline="false"></span> -->
                        <img src="assets/images/bag-09.svg" style="font-size: -16px;width: 15px;">

                    </div>

                    <div class="col-sm-3">
                        <h4 style="font-size: 15px;margin-left: -130px;" class="all-order-media">

                          <a style="color: black;font-family: 'avertaregular';" data-toggle="modal" data-dismiss="modal" data-target="#modalAllOrders">All Orders</a>
                             </h4>

                    </div>
                    <div class="col-sm-3 media-number-order" style="margin-top: -9px;">
                        <h4 style="font-family: 'avertaregular';" class="button-all-orders"> 2 </h4>
                    </div>

                    <hr style="width: 318px;margin-left: 10px;">

                </div>

                <div class="row" style="
                margin-top: 15px;
                margin-left: 10px;">

                    <div class="col-sm-6">
                        <span class="iconify" data-icon="dashicons:star-empty" data-inline="false"></span>

                    </div>

                    <div class="col-sm-6">
                        <h4 class="my-rating-media" style="font-size: 15px;margin-left: -130px;">
                            <a style="font-family: 'avertaregular';color: black;" data-toggle="modal" data-dismiss="modal" data-target="#modalMyRatingHistory">
                          My Rating & reviews
                      </a>
                             </h4>
                    </div>

                    <hr style="width: 318px;margin-left: 10px;">

                </div>

                <div class="row" style="margin-top: 15px;margin-left: 10px;margin-bottom: 15px">

                    <div class="col-sm-6">
                        <span class="iconify" data-icon="fa-regular:comment" data-inline="false"></span>

                    </div>

                    <div class="col-sm-6">
                        <h4 class="customer-support" style="font-size: 15px;margin-left: -130px;">
                          <a style="font-family: 'avertaregular';color: black" href="help-and-support.html">Customer support</a>
                             </h4>
                    </div>

                </div>

            </div>

            <div>
                <button type="button" class="close x-button-floating" style="background-color: white;" data-dismiss="modal">&times;</button>
            </div>

        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="modalMenuNotLogin" role="dialog" style="overflow-y: scroll;">
        <div class="modal-dialog modal-sm">

            <!-- Modal content-->
            <div class="modal-content" style="border-radius: 19px;width: 350px;">

                <div class="row" style="margin-top: 35px;margin-left: 10px;">

                    <button style="font-family: 'avertaregular';color: white;background-color: #58017D;width: 90px;height: 40px;border-radius: 50px;margin-left: 100px;font-size: 12px;" data-toggle="modal" data-dismiss="modal" data-target="#modalSignUp">
                        Sign up
                    </button>

                    <hr style="width: 350px;margin-left: 10px;">

                </div>

                <div class="row" style="
                margin-top: 15px;
                margin-left: 10px;">

                    <div class="col-sm-6">
                        <i class="fa fa-heart-o"></i>

                    </div>

                    <div class="col-sm-6">
                        <h4 style="font-family: 'avertaregular';font-size: 15px;margin-left: -130px;">
                          Refer a friend
                             </h4>
                    </div>

                    <hr style="width: 350px;margin-left: 10px;">

                </div>

                <div class="row" style="
                margin-top: 15px;
                margin-left: 10px;">

                    <div class="col-sm-6">
                        <span class="iconify" data-icon="uil:cart" data-inline="false"></span>

                    </div>

                    <div class="col-sm-3">
                        <h4 style="font-family: 'avertaregular';font-size: 15px;margin-left: -130px;">
                          All Orders
                             </h4>

                    </div>
                    <div class="col-sm-3">
                        <h4 class="button-all-orders"> 2 </h4>
                    </div>

                    <hr style="width: 350px;margin-left: 10px;">

                </div>

                <div class="row" style="
                margin-top: 15px;
                margin-left: 10px;">

                    <div class="col-sm-6">
                        <span class="iconify" data-icon="dashicons:star-empty" data-inline="false"></span>

                    </div>

                    <div class="col-sm-6">
                        <h4 style="font-family: 'avertaregular';font-size: 15px;margin-left: -130px;">
                          My Rating & reviews
                             </h4>
                    </div>

                    <hr style="width: 350px;margin-left: 10px;">

                </div>

                <div class="row" style="
                 margin-top: 15px;
                 margin-left: 10px;">

                    <div class="col-sm-6">
                        <span class="iconify" data-icon="fa-regular:comment" data-inline="false"></span>

                    </div>

                    <div class="col-sm-6">
                        <h4 style="font-size: 15px;margin-left: -130px;">
                          <a style="font-family: 'avertaregular';color: #000000" href="help-and-support.html">Customer support</a>
                             </h4>
                    </div>

                    <hr style="width: 350px;margin-left: 10px;">

                </div>

                <div class="row" style="
                 margin-top: 15px;
                 margin-left: 10px;padding-bottom: 20px;">

                    <div class="col-sm-6">
                        <span class="iconify" data-icon="bytesize:settings" data-inline="false"></span>

                    </div>

                    <div class="col-sm-6">
                        <h4 style="font-family: 'avertaregular';font-size: 15px;margin-left: -130px;">
                          Setting
                             </h4>
                    </div>

                </div>

            </div>

            <div>
                <button type="button" class="close x-button-floating" style="background-color: white;" data-dismiss="modal">&times;</button>
            </div>

        </div>
    </div>
