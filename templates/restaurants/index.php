<section class="menu cid-rNDrya5QF1" once="menu" id="menu2-24">

        <nav class="navbar navbar-expand beta-menu navbar-dropdown align-items-center navbar-fixed-top navbar-toggleable-sm bg-color transparent"style="background-color: #58017D">
            <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <div class="hamburger">
                    <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                </div>
            </button>
            <div class="menu-logo">
                <div class="navbar-brand">
                    <span class="navbar-logo">
                    <a href="index.html">
                        <img src="assets/images/logo.png" alt="Homechow" class="image-media" title="" style="height: 30px;">
                    </a>
                </span>

                </div>
                <i class=""></i>
            </div>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">

                <div style="color: white;">
                    <h4 style="font-size: 18px;font-family: 'avertaregular';" id="kitchenId">
                       <a href="#features15-2u"> Street-food</a>
                    </h4>
                </div>

                <div style="color: white;">
                    <h4 style="font-size: 18px;font-family: 'avertaregular';" id="restaurantsId">
                       <a href="#features2-2f"> Restaurants</a>
                    </h4>
                </div>

                <div style="margin-left: 45px;color: white;">
                    <h4 style="font-size: 18px;font-family: 'avertaregular';" id="groupOrdersId">
                        Group orders
                    </h4>
                </div>

                <div style="margin-left: 40px;color: white;">
                    <h4 style="font-size: 18px;font-family: 'avertaregular';" id="drinksId">
                        <a href="#features2-2fs">Drinks</a>
                    </h4>
                </div>

                <div style="margin-left: 40px;color: white;">
                    <h4 style="font-size: 18px;font-family: 'avertaregular';" id="partyBookingId">
                        <a href="book-for-event.html">Events booking</a>
                    </h4>
                </div>

                <div id="newVendor" style="width: 117px;height: 35px;background-color: #1fbf58;border-radius: 25px;margin-left: 40px">
                <button style="border-style: none;color: white; background-color: #ffffff00;margin-top: 4px;margin-left: 6px;">
                    New Vendor
                </button>
                </div>

                <div class="nav-icons nav-media-ios" style="margin-left: 20px;">
                    <span style="color: white;font-size: 30px; padding-right: 4px;" class="iconify icon-app-store-ios-brands" data-icon="fa-brands:app-store" data-inline="false"></span>
                </div>

                <div class="nav-icons nav-media-google-play" style="margin-left: 15px;">
                    <span style="color: white;font-size: 30px;" class="iconify icon-app-store-ios-brands" data-icon="mdi:google-play" data-inline="false"></span>
                </div>

                <div class="nav-icons nav-media-avator" style="margin-left: 45px;">
                    <a data-toggle="modal" data-target="#modalSignUp">
                        <span style="color: white;font-size: 25px;padding-left: 2px;" class="iconify" data-icon="bx:bx-user" data-inline="false"></span>
                    </a>
                </div>

                <div class="nav-menu-media" style="margin-left: 15px;">

                    <a data-toggle="modal" data-target="#modalMenuLogin">

                        <img src="assets/images/webapp-menu-icon.svg">
                    </a>
                </div>

            </div>
        </nav>
    </section>

    <section class="header3 cid-rOhlph0oA11 mbr-parallax-background" id="header3-2t" style="height: 356px;">

        <div class="container align-center">
            <div class="row justify-content-md-center">
                <div class="mbr-white col-md-10">

                </div>
            </div>
        </div>

    </section>

    <div class="container navbar-dropdowns  navbar-fixed-tops" style="margin-top: 120px;" id="busket-view">

        <div class="basket-label basket-label-media" style="margin-top: 100px;">
            <!-- <span class="iconify" data-icon="uil:cart" data-inline="false"></span> -->
            <h4 style="font-family: 'avertaregular';font-size: 15px;text-align: left;margin-left: 10px;">

                <img src="assets/images/webapp-basket-icon.svg" style="height: 25px;">
                     <!-- <span class="iconify" data-icon="uil:cart" data-inline="false"></span> -->
                &nbsp &nbsp 2 items in your basket.

            </h4>
            
            <button style="font-family: 'avertaregular';font-size: 15px;" class="btn-view button-view-media" data-toggle="modal" data-dismiss="modal" data-target="#modalBasket">

                <!-- <button class="btn-view" data-toggle="modal" data-dismiss="modal" data-target="#modalHoganGroupOrder"> -->
            <!-- <button class="btn-view" data-toggle="modal" data-dismiss="modal" data-target="#modalDiscount"> -->
                <!-- <button class="btn-view" data-toggle="modal" data-dismiss="modal" data-target="#modalJoinGroupOrder"> -->
                View
            </button>

        </div>

        <div class="floating-busket-counter floating-busket-counter-media">

            <h4 style="font-family: 'avertaregular';color: white;font-size: 10px;padding-left: 3px;">
                    2
                </h4>

        </div>

        <div class="commucation-button commucation-button-media">
            <img src="assets/images/message-circle.svg" class="communication-img" style="margin-top: -65px;">
        </div>

    </div>

    <section style="background-color: white;padding-bottom: 100px;">

        <div class="container">

            <div style="padding-top: 70px;padding-left: 225px;padding-bottom: 60px;">

                <div class="restaurants-media-logo">
                    <img src="assets/images/Hoo-gah.png" alt="Mobirise">
                    <h5 style="font-family: 'avertaregular';font-size: 20px;margin-left: 160px;margin-top: -102px;">
                                   Hoogah - Osu
                                </h5>
                    <h5 style="font-family: 'avertaregular';font-size: 15px;color: #797878;margin-left: 160px;margin-top: 25px;">
                                    Category
                                </h5>
                </div>

                <div class="star-media-restaurant" style="margin-left: 255px;margin-top: -65px;">
                    <br>
                    <span class="iconify" data-icon="ant-design:star-filled" style="color: green;font-size: 20px;" data-inline="false"></span>
                    <h4 style="font-family: 'avertaregular';font-size: 15px;margin-left: 30px;margin-top: -22px;color: green;">4.6</h4>
                    <h4 style="font-family: 'avertaregular';font-size: 13px;font-size: 15px;margin-left: 58px;margin-top: -25px; color: #989898;">(242)</h4>

                </div>

                <div class="ellipsis-media" style="background-color: #EEEAEA;width: 30px;height: 30px;border-radius: 25px;margin-left: 765px;margin-top: -70px;">
                    <a data-toggle="modal" data-target="#modalRestaurantsMenu">
                        <i class="fa fa-ellipsis-h" style="margin-left: 8px;margin-top: 7px;"></i>
                    </a>
                </div>

            </div>

            <div class="description-media-restaurants" style="width: 800px;margin-left: 237px;margin-top: 60px;">
                <p style="font-family: 'avertaregular';color: #4E4E4E;font-size: 20px;">

                                But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one

                            </p>
            </div>

        </div>
    </section>

    <section class="features2 cid-rNDtm1hBwb" id="features2-2f" style="padding-top: 0px;padding-bottom: 40px;">

        <div class="container">
            <h6  class="featured-media" style="font-family: 'avertaregular';margin-left: 230px;font-size: 20px;margin-top: -50px;color: #5f5d5d;">
                Featured meals
            </h6>

            <div class="media-container-row">

                <div class="card p-3 col-12 col-md-6 col-lg-3">
                    <div class="card-wrapper">
                        <div class="card-img featured-food-image-1" style="margin-right: -575px;">
                            <img src="assets/images/beef-steak.png" alt="Mobirise" class="image-custom" style="width: 130px;height: 140px;">
                        </div>

                        <div class="card-box mbr-overlay overlay-media" style="opacity: 0.6;background-color: #000000;width: 129px;margin-left: 363px;height: 60px;margin-top: 96px;border-bottom-left-radius: 30px;border-bottom-right-radius: 30px;">
                            <h2 style="font-family: 'avertaregular';font-size: 10px;color: whitesmoke;margin-top: -10px;">
                               Beef steak with sauce
                             </h2>

                        </div>

                    </div>
                </div>

                <div class="card p-3 col-12 col-md-6 col-lg-3">
                    <div class="card-wrapper">
                        <div class="card-img featured-food-image-2" style="margin-right: -355px;">
                            <img src="assets/images/burrito-chicken-delicious-dinner-461198.png" alt="Mobirise" class="image-custom" style="width: 130px;height: 140px;">
                        </div>

                        <div class="card-box mbr-overlay overlay-media-1" style="opacity: 0.6;background-color: #000000;width: 129px;margin-left: 251px;height: 60px;margin-top: 96px;border-bottom-left-radius: 30px;border-bottom-right-radius: 30px;">
                            <h2 style="font-family: 'avertaregular';font-size: 10px;color: whitesmoke;margin-top: -10px;">
                               Burrito chicken del…
                             </h2>

                        </div>

                    </div>
                </div>

                <div class="card p-3 col-12 col-md-6 col-lg-3">
                    <div class="card-wrapper">
                        <div class="card-img featured-food-image-3" style="margin-left: 124px;">
                            <img src="assets/images/meat-burger-with-coleslaw-on-side-and-brown-handled-fork-156114.png" alt="Mobirise" class="image-custom" style="width: 130px;height: 140px;">
                        </div>

                        <div class="card-box mbr-overlay overlay-media-2" style="opacity: 0.6;background-color: #000000;width: 129px;margin-left: 141px;height: 60px;margin-top: 96px;border-bottom-left-radius: 30px;border-bottom-right-radius: 30px;">
                            <h2 style="font-family: 'avertaregular';font-size: 10px;color: whitesmoke;margin-top: -10px;">
                                Meat burger with co…
                             </h2>

                        </div>

                    </div>
                </div>

                <div class="card p-3 col-12 col-md-6 col-lg-3">
                    <div class="card-wrapper">
                        <div class="card-img featured-food-image-4" style="margin-left: -107px;">
                            <img src="assets/images/meat-burger-with-coleslaw-on-side-and-brown-handled-fork-156114.png" alt="Mobirise" class="image-custom" style="width: 130px;height: 140px;">
                        </div>

                        <div class="card-box mbr-overlay overlay-media-3" style="opacity: 0.6;background-color: #000000;width: 129px;margin-left: 21px;height: 60px;margin-top: 96px;border-bottom-left-radius: 30px;border-bottom-right-radius: 30px;">
                            <h2 style="font-family: 'avertaregular';font-size: 10px;color: whitesmoke;margin-top: -10px;">
                                Meat burger with co…
                             </h2>

                        </div>

                    </div>
                </div>

                <div class="card p-3 col-12 col-md-6 col-lg-3">
                    <div class="card-wrapper">
                        <div class="card-img featured-food-image-5" style="margin-left: -340px;">
                            <img src="assets/images/meat-burger-with-coleslaw-on-side-and-brown-handled-fork-156114.png" alt="Mobirise" class="image-custom" style="width: 130px;height: 140px;">
                        </div>

                        <div class="card-box mbr-overlay overlay-media-4" style="opacity: 0.6;background-color: #000000;width: 129px;margin-left: -95px;height: 60px;margin-top: 96px;border-bottom-left-radius: 30px;border-bottom-right-radius: 30px;">
                            <h2 style="font-family: 'avertaregular';font-size: 10px;color: whitesmoke;margin-top: -10px;">

                                Meat burger with co…
                             </h2>

                        </div>
                    </div>
                </div>

            </div>
        </div>

    </section>

    <div id="buttonDiv" style="background-color: #F6F6F6;width: 217px;height: 50px;margin-bottom: 30px;margin-left: 580px;border-radius: 25px;">

        <button style="font-family: 'avertaregular';" class="pick-up-button active">
            Delivery
        </button>

        <button style="font-family: 'avertaregular';" class="pick-up-button">
            Pickup
        </button>

    </div>

    <div class="lunch-pasta-media" style="margin-left: 415px;width: 716px;height: 50px;margin-bottom: 20px">
        <button style="font-family: 'avertaregular';color: #58007D;background-color: #EFDFF6;border-color: #58007D;border-radius: 25px;width: 90px;height: 39px;">
            Lunch
        </button>

        <button style="font-family: 'avertaregular';" class="restaurants-button">
            Pasta
        </button>

        <button style="font-family: 'avertaregular';" class="restaurants-button">
            Hot dogs
        </button>

        <button style="font-family: 'avertaregular';" class="restaurants-button">
            Soups
        </button>

        <button style="font-family: 'avertaregular';" class="restaurants-button">
            Sauces
        </button>
    </div>

    <hr class="hr-media-rest" style="width: 600px;margin-left: 420px;margin-top: -5px;">

    <div class="search-div" style=" margin-left: 420px;padding-top: 90px;">

        <form action="" method="POST" class="mbr-form form-with-styler" data-form-title="Mobirise Form">

            <div class="dragArea row">
                <div class="form-group col" data-for="email" data-toggle="modal" data-target="#modalsearch">
                    <img src="assets/images/search.svg" style="margin-bottom: -69px;margin-left: 12px;">
                    <input type="text" name="search" placeholder="Search" style="border-radius: 25px;padding-left: 50px;width: 590px;background-color: #EBEBEB;" required="required" class="form-control display-7 search-media-input">

                </div>

            </div>
        </form>

    </div>

    <!-- <div class="search-media-rest" style="margin-left: 280px;">
        <form action="" method="POST" class="mbr-form form-with-styler" data-form-title="Mobirise Form">
            <input type="hidden" name="email" data-form-email="true" value="">

            <div class="dragArea row" style="margin-left: 120px;">
                <div class="form-group col" data-for="email">
                    <img class="search-svg-media" src="assets/images/search.svg" style="margin-bottom: -69px;margin-left: 12px;">
                    <input class="input-media-search" type="text" name="search" placeholder="Search Hoogah - Osu" style="border-radius: 25px;padding-left: 50px;width: 600px;background: #EBEBEB;" required="required" class="form-control display-7">

                </div>
                
            </div>
        </form>
    </div> -->

    <div style="margin-top: 30px; margin-bottom: 120px">

        <div class="banku-media-img" style="margin-left: 420px;">
            <a data-toggle="modal" data-dismiss="modal" data-target="#modalFood">
            <img src="assets/images/banku & tilapia.png" style="border-radius: 25px;height: 96px;width: 96px;">

            <h4 style="font-family: 'avertaregular';font-size: 16px;margin-left: 120px;margin-top: -90px;">
            Banku & Tilapia
        </h4>

            <div class="desc-media" style="width: 410px;margin-left: 120px;">
                <p style="font-family: 'avertaregular';font-size: 16px; color: #909090;">
            But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will
        </p>
            </div>

            <div class="amount-media" style="margin-left: 540px;margin-top: -65px;">

                <p style="font-family: 'avertaregular';font-size: 16px">
            GHC 35
        </p>

            </div>
        </a>

            <hr class="hr-food-media" style="width:600px;margin-top: 100px;margin-left: 3px;padding-bottom: 30px;">

        </div>

        <div class="banku-media-img" style="margin-left: 420px;">
            <a data-toggle="modal" data-dismiss="modal" data-target="#modalFood">
            <img src="assets/images/banku & tilapia.png" style="border-radius: 25px;height: 96px;width: 96px;">

            <h4 style="font-family: 'avertaregular';font-size: 16px;margin-left: 120px;margin-top: -90px;">
            Banku & Tilapia
        </h4>

            <div class="desc-media" style="width: 410px;margin-left: 120px;">
                <p style="font-family: 'avertaregular';font-size: 16px; color: #909090;">
            But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will
        </p>
            </div>

            <div class="amount-media" style="margin-left: 540px;margin-top: -65px;">

                <p style="font-family: 'avertaregular';font-size: 16px">
            GHC 35
        </p>

            </div>
        </a>

            <hr class="hr-food-media" style="width:600px;margin-top: 100px;margin-left: 3px;padding-bottom: 30px;">

        </div>

        <div class="banku-media-img" style="margin-left: 420px;">
            <a data-toggle="modal" data-dismiss="modal" data-target="#modalFood">
                <img src="assets/images/banku & tilapia.png" style="border-radius: 25px;height: 96px;width: 96px;">

                <h4 style="font-family: 'avertaregular';font-size: 16px;margin-left: 120px;margin-top: -90px;">
            Banku & Tilapia
        </h4>

                <div class="desc-media" style="width: 410px;margin-left: 120px;">
                    <p style="font-family: 'avertaregular';font-size: 16px; color: #909090;">
            But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will
        </p>
                </div>

                <div class="amount-media" style="margin-left: 540px;margin-top: -65px;">

                    <p style="font-family: 'avertaregular';font-size: 16px">
            GHC 35
        </p>

                </div>
            </a>

        </div>

       

    </div>

    <section class="features11 cid-rNDtFNiRhT mbr-fullscreen" id="features11-2i" style="background-color: #F6F6F6;">

        <div class="container">
            <div class="col-md-12">
                <div class="media-container-row">
                    <div class="mbr-figure m-auto phone-mockup-media" style="width: 45%;">
                        <img src="assets/images/Phone mockups.png" alt="Mobirise" title="" style="margin-top: -50px;">
                    </div>
                    <div class=" align-left aside-content the-best-food-media" style="margin-left: 160px;">
                        <h2 class="mbr-title  mbr-fonts-style display-2 heading-best-food-media" style="color: #58017D;" style="font-size: 60px;">
                         <strong style="font-family: 'avertaregular';">
                        The Best Food <br>
                        Delivery App
                        </strong>
                    </h2>

                        <div class="block-content">
                            <div class="card">

                                <div class="card-box good-food-div-media" style="width: 460px;">
                                    <div >
                                    <p style="font-family: 'avertaregular';" class="block-text mbr-fonts-style display-7" style="font-size: 25px;margin-top: -80px;">
                                        <br>
                                        <br>
                                        <br> 
                                        Good food is meant to be shared and you can order from your favorite restaurant and enjoy tasty meals on the go using Homechow.
                                    </p>
                                    </div>
                                </div>

                                <img class="google-play-media" src="assets/images/google-play.png" style="width: 140px;">
                                <img class="apple-store-media" src="assets/images/apple-store.png" style="width: 140px;margin-left: 260px;margin-top: -51px;">
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="cid-rNDtMJWRWX" id="footer1-2j">

        <div class="container">
            <div class="media-container-row content text-white">
                <div class="col-12 col-md-2 mbr-fonts-style display-7">
                    <h5 style="font-family: 'avertaregular';" class="pb-2">
                        <br>
                    Support
                      </h5>
                    <p style="font-family: 'avertaregular';" class="mbr-text">
                        FAQ
                        <br>Help & Support
                        <br>Ride with us
                        <br> Careers
                        <br>
                        <br>
                    </p>
                    <h5 style="font-family: 'avertaregular';padding-top: 50px;">
                        Legal
                        <br>
                    </h5>
                    <p style="font-family: 'avertaregular';" class="mbr-text">
                        <br>Terms & Condition
                        <br>Privacy Policy
                        <br>RVendor Agreement
                    </p>
                </div>
                <div class="col-12 col-md-2 mbr-fonts-style display-7">
                    <h5 style="font-family: 'avertaregular';" class="pb-2">
                        <br>
                    Services
                </h5>
                    <p style="font-family: 'avertaregular';" class="mbr-text">
                        Booking
                        <br>Kitchen
                        <br>Marketplace
                        <br>Drinks
                    </p>

                    <h4 style="font-family: 'avertaregular';" class="comming-soon soon-media-market-place"> soon </h4>
                </div>
                <div class="col-12 col-md-2 mbr-fonts-style display-7">
                    <h5 style="font-family: 'avertaregular';" class="pb-2">
                    Popular<br>
                    categories
                </h5>
                    <p style="font-family: 'avertaregular';" class="mbr-text">
                        African
                        <br>Local
                        <br>Continental
                        <br>Lunch
                        <br>Breakfast
                        <br>Italian
                        <br>Drinks
                        <br>Fast food
                    </p>

                </div>
                <div class="col-12 col-md-2 mbr-fonts-style display-7">
                    <h5 style="font-family: 'avertaregular';" class="pb-2">
                    Operating
                    Countries
                </h5>
                    <p class="mbr-text" style="font-family: 'avertaregular';color: white;">
                        Ghana
                        <ui style="margin-top: -8px;">
                            <li class="ul-dash" style="font-family: 'avertaregular';padding-bottom: 7px;padding-left: 10px;"> Accra </li>
                            <li class="ul-dash" style="font-family: 'avertaregular';padding-bottom: 7px;padding-left: 10px;"> Kumasi </li>
                            <li class="ul-dash" style="font-family: 'avertaregular';padding-bottom: 7px;padding-left: 10px;">Takoradi </li>
                            <li class="ul-dash" style="font-family: 'avertaregular';padding-bottom: 7px;padding-left: 10px;">Tomale </li>
                            <!-- <li class="ul-dash">Cape Coast</li> -->
                            <li class="ul-dash" style="font-family: 'avertaregular';padding-bottom: 7px;padding-left: 10px;">Koforidua</li>
                        </ui>

                        <p class="mbr-text" style="font-family: 'avertaregular';margin-top: 54px;color: white;">
                            Nigeria
                            <ui>
                                <li class="ul-dash" style="font-family: 'avertaregular';margin-top: -10px;margin-left: 10px;"> Lagos </li>
                            </ui>
                            <h4 class="soon-media-1" style="font-family: 'avertaregular';font-size: 7px;background-color: #c56ac5;color: black;height: 16px;padding: 4px;width: 24px;text-align: center;border-radius: 50px;margin-left: 52px;margin-top: -47px;"> soon </h4>
                        </p>
                    </p>
                    <div style="margin-top: -165px;margin-left: -11px;">
                        <h4 class="soon-media-2" style="font-family: 'avertaregular';font-size: 7px;margin-left: 90px;background-color: #c56ac5;color: black;height: 16px;padding: 4px;width: 24px;text-align: center;border-radius: 50px;"> soon </h4>
                    </div>

                    <div style="margin-top: 10px;margin-left: -20px;">

                        <h4 class="soon-media-3" style="font-family: 'avertaregular';font-size: 7px;margin-left: 90px;background-color: #c56ac5;color: black;height: 16px;padding: 4px;width: 24px;text-align: center;border-radius: 50px;"> soon </h4>
                    </div>
                    <div style="margin-top: 10px;margin-left: -6px;">
                        <h4 class="soon-media-4" style="font-family: 'avertaregular';font-size: 7px;margin-left: 90px;background-color: #c56ac5;color: black;height: 16px;padding: 4px;width: 24px;text-align: center;border-radius: 50px;"> soon </h4>
                    </div>
                    <!-- <div style="margin-top: 12px;">
                        <h4 style="font-size: 7px;margin-left: 90px;background-color: #c56ac5;color: black;height: 16px;padding: 4px;width: 24px;text-align: center;border-radius: 50px;"> soon </h4>
                    </div> -->

                </div>
                <div class="col-12 col-md-2 mbr-fonts-style display-7">
                    <h5 class="pb-2" style="padding-top: 45px;">
                    <!-- Services -->
                      </h5>
                    <p class="mbr-text kenya-media" style="font-family: 'avertaregular';color: white;">
                        Kenya
                        <ui>
                            <li class="ul-dash" style="font-family: 'avertaregular';margin-top: -10px;margin-left: 5px;"> Nairobi </li>
                            <!-- <li class="ul-dash"> Mombasa </li> -->
                        </ui>
                        <!-- <h4 style="font-size: 8px;background-color: #c56ac5;width: 30px;text-align: center;border-radius: 50px;margin-left: 80px;margin-top: -13px;"> soon </h4> -->
                    </p>

                    <p class="mbr-text" style="font-family: 'avertaregular';margin-top: 30px;color: white;">
                        Ethiopia
                        <ui>
                            <li class="ul-dash" style="font-family: 'avertaregular';margin-top: -10px;margin-left: 5px;"> Adis Ababa </li>
                            <!-- <li class="ul-dash"> Mombasa </li> -->
                        </ui>
                        <h4 class="soon-media-5" style="font-family: 'avertaregular';font-size: 7px;background-color: #c56ac5;width: 24px;text-align: center;border-radius: 50px;margin-left: 55px;margin-top: -46px;height: 16px;padding: 4px;color: black;"> soon </h4>
                    </p>

                    <!-- <div style="font-size: 7px;background-color: #c56ac5;width: 24px;text-align: center;border-radius: 50px;margin-left: 61px;margin-top: -46px;height: 16px;padding: 4px;color: black;"></div> -->

                    <p class="mbr-text" style="font-family: 'avertaregular';margin-top: 55px;color: white;">
                        Rwanda
                        <ui>
                            <li class="ul-dash" style="font-family: 'avertaregular';margin-top: -10px;margin-left: 10px;"> Kigali </li>
                        </ui>
                        <h4 class="soon-media-6" style="font-family: 'avertaregular';font-size: 7px;background-color: #c56ac5;width: 24px;text-align: center;border-radius: 50px;margin-left: 55px;margin-top: -47px;color: black;height: 16px;padding: 4px;"> soon </h4>
                    </p>
                    <p class="mbr-text" style="font-family: 'avertaregular';margin-top: 54px;color: white;">
                        Uganda
                        <ui>
                            <li class="ul-dash" style="font-family: 'avertaregular';margin-top: -10px;margin-left: 10px;"> Kampala </li>
                        </ui>
                        <h4 class="soon-media-7" style="font-family: 'avertaregular';font-size: 7px;background-color: #c56ac5;color: black;height: 16px;padding: 4px;width: 24px;text-align: center;border-radius: 50px;margin-left: 52px;margin-top: -47px;"> soon </h4>
                    </p>
                </div>
            </div>
            <div class="footer-lower">
                <!-- <div class="media-container-row">
                    <div class="col-sm-12">
                        <hr>
                    </div>
                </div> -->
                <div class="media-container-row mbr-white">
                    <!-- <div class="col-sm-6 copyright">
                        <p class="mbr-text mbr-fonts-style display-7"></p>
                    </div> -->
                    <div>
                        <div class="social-list align-center">
                            <div class="soc-item">
                                <!-- <a href="" target="_blank"> -->
                                <span class="fa fa-twitter socicon mbr-iconfont mbr-iconfont-social"></span>
                                <!-- </a> -->
                            </div>
                            <div class="soc-item">
                                <!--  <a href="" target="_blank"> -->
                                <span class="fa fa-instagram socicon mbr-iconfont mbr-iconfont-social"></span>

                                <!-- </a> -->
                            </div>
                            <div class="soc-item">
                                <!-- <a href="" target="_blank"> -->
                                <span class="fa fa-facebook-square socicon mbr-iconfont mbr-iconfont-social"></span>
                                <!-- </a> -->
                            </div>
                            <div class="soc-item">
                                <!-- <a href="" target="_blank"> -->
                                <span class="fa fa-linkedin-square socicon mbr-iconfont mbr-iconfont-social"></span>
                                <!-- </a> -->
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section once="footers" class="cid-rNDtRVzGUV" id="footer6-2k">
        <div class="media-container-row">
            <div class="col-sm-12">
                <hr style="border-top: 1px solid #8ec8fb38;" class="hr-bottom-media">
            </div>
        </div>

        <div class="container">

            <div class="media-container-row align-center mbr-white">
                <!-- <div class="col-12"> -->
                <p class="mbr-text mb-0 mbr-fonts-style display-7" style="font-family: 'avertaregular';color: white;">
                    (C) 2020 All Rights Reserved <a href="http://homechow.io/"> Homechow.io </a> App Ltd.
                </p>
                <!-- </div> -->
            </div>
        </div>
    </section>




    <!-- Modal -->
    <div class="modal fade" id="modalRateRestaurants" role="dialog" style="overflow-y: scroll;">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content" style="border-radius: 20px;">
                
                <div style=" margin-left: 125px;margin-top: 60px;">

                    <div>

                    <span class="iconify" style="color: green;font-size: 30px;" data-icon="dashicons:star-filled" data-inline="false"></span>

                    </div>

                    <div style="margin-left: 55px;margin-top: -25px;">
                    <span class="iconify" style="color: #EFEFEF;font-size: 30px;" data-icon="dashicons:star-filled" data-inline="false"></span>
                    </div>

                    <div style="margin-left: 110px;margin-top: -30px;">
                    <span class="iconify" style="color: #EFEFEF;font-size: 30px;" data-icon="dashicons:star-filled" data-inline="false"></span>
                    </div>

                    <div style="margin-left: 165px;margin-top: -30px;">
                    <span class="iconify" style="color: #EFEFEF;font-size: 30px;" data-icon="dashicons:star-filled" data-inline="false"></span>
                    </div>

                    <div style="margin-left: 220px;margin-top: -30px;">
                    <span class="iconify" style="color: #EFEFEF;font-size: 30px;" data-icon="dashicons:star-filled" data-inline="false"></span>
                    </div>
                    
                </div>

                <div style="font-family: 'avertaregular';color: #C8C8C8;font-size: 17px;margin-left: 200px;margin-top: 20px;margin-bottom: 20px;">
                    Very good
                </div>

                <div style="background-color: #EFEFEF;height: 250px;">
                    <p style="font-family: 'avertaregular';font-size: 17px;padding-left: 20px;padding-right: 20px;padding-top: 20px;">
                        The quick, brown fox jumps over a lazy dog. DJs flock by when MTV ax quiz prog. Junk MTV quiz graced by fox whelps. Bawds jog, flick quartz
                    </p>
                </div>

                <div style="margin-top: 30px;margin-left: 208px;padding-bottom: 30px;">

                    <div>
                    <button style="font-family: 'avertaregular';background-color: #ffffff00;border-style: none; font-size: 17px;">
                        Send
                    </button>
                    </div>

                    <div style="margin-top: 20px;">
                    <button data-dismiss="modal" style="font-family: 'avertaregular';color: #A8A8A8;background-color: #ffffff00;border-style: none;font-size: 17px; ">
                        Cancel
                    </button>
                    </div>
                </div>

            </div>

        </div>

    </div>



    <!-- Modal -->
    <div class="modal fade" id="modalPasswordSetUp" role="dialog" style="overflow-y: scroll;">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content" style="border-radius: 20px;">
                <div class="modal-header">
                    <!-- <button type="button" class="close" style="margin-left: 10px;" data-dismiss="modal">&times;</button> -->
                    <h4 class="modal-title" style="font-family: 'avertaregular';font-size: 20px;text-align: center;margin-left: 170px;margin-top: 26px;margin-bottom: 24px;">Password</h4>

                </div>

                <div style="margin-left: 46px;width: 400px;padding-bottom: 0px;">
                    <h4 style="font-family: 'avertaregular';font-size: 15px"> 

                     A temporary password has been sent to your phone. Please enter it below.

                    </h4>
                </div>

                <form action="#">

                    <label style="font-family: 'avertaregular';margin-left: 55px;margin-top: 20px;">Temporary password</label>

                    <div class="form-row" style="margin-left: 40px;margin-right: 40px;">
                        <div class="col">
                            <input type="password" class="form-control" style="border-radius: 10px;margin-right: 40px;width: 330px;" name="tempPassword">
                        </div>

                    </div>

                    <label style="font-family: 'avertaregular';margin-left: 55px;margin-top: 20px;">New password</label>

                    <div class="form-row" style="margin-left: 40px;margin-right: 40px;">
                        <div class="col">
                            <input type="password" class="form-control" style="border-radius: 10px;margin-right: 40px;width: 330px;" name="newPassword">
                        </div>

                    </div>
                    <label style="font-family: 'avertaregular';margin-left: 55px;margin-top: 20px;">Confirm new password</label>

                    <div class="form-row" style="margin-left: 40px;margin-right: 40px;">
                        <div class="col">
                            <input type="password" class="form-control" style="border-radius: 10px;margin-right: 40px;width: 330px;" name="confirmNewPassowrd">
                        </div>

                    </div>
                    <div style="margin-left: 170px;padding-bottom: 10px;padding-top: 10px;">
                        <button style="font-family: 'avertaregular';" type="button" class="btn btn-default button-continue" data-dismiss="modal">Continue</button>
                    </div>

                    <div>
                        <h3 class="have-an-account">
                           Didn’t receive temporary password? <a style="font-family: 'avertaregular';color: #58017D; font-family: 'avertabold';">Resend</a>
                        </h3>
                    </div>

                </form>

            </div>

        </div>
    </div>


    <!-- Modal -->
    <div class="modal fade" id="modalGroupOrder" role="dialog" style="overflow-y: scroll;">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content" >
                <div class="modal-header modal-media-header" style="margin-top: 40px;">
                    <!-- <button type="button" class="close" style="margin-left: 10px;" data-dismiss="modal">&times;</button> -->

                    
                    <div style="margin-left: 109px;">
                    <h4 class="modal-title" style="font-family: 'avertaregular';font-size: 20px;">Hoogah Group Order</h4>
                    </div>


                    <div class="created-by-media" style="margin-top: 50px;margin-left: -300px;margin-bottom: 0px;">
                    <p style="font-family: 'avertaregular';font-size: 17px; color: #C8C8C8">
                        Created by you
                    </p>
                    </div>

                    <div style="margin-top: 6px;margin-right: 27px;">
                    <a data-dismiss="modal" >
                        <p style="font-family: 'avertaregular';color: #58017D; font-size: 17px;">
                            Cancel
                        </p>
                    </a>
                    </div>
                </div>

                <div class="set-order-time" style="margin-left: 92px;">
                    
                    <p style="font-family: 'avertaregular';font-size: 17px">
                        Set order time
                    </p>

                </div>

                <div class="choose-time" style="margin-left: 88px;">
                <label style="margin-left: 3px;font-family: 'avertaregular';color: #707070;margin-top: 27px;margin-bottom: 20px;">When will you like the order to be sent? </label>

                    <div class="form-row" style="margin-left: -2px;margin-right: 40px;">
                        <div class="col">
                            <select type="text" class="form-control text-media" style="font-family: 'avertaregular';border-radius: 10px;border-color: #EEEAEA;margin-right: 40px;width: 300px;padding-left: 23px;font-size: 14px;" name="location">
                            <option style="color: #000000">Choose time</option>
                            </select>
                        </div>

                    </div>
            </div>

            <div class="choose-time" style="margin-left: 90px;">
                <label style="margin-left: 3px;font-family: 'avertaregular';color: #707070;margin-top: 27px;margin-bottom: 20px;">Set minimum order amount (optional)</label>

                    <div class="form-row" style="margin-left: -2px;margin-right: 40px;">
                        <div class="col">
                            <select type="text" class="form-control text-media" style="font-family: 'avertaregular';border-radius: 10px;border-color: #EEEAEA;margin-right: 40px;width: 300px;padding-left: 23px;font-size: 14px;" name="location">
                            <option style="font-family: 'avertaregular';color: #000000">Select amount</option>
                            </select>
                        </div>

                    </div>
            </div>

            <div class="choose-time-button" style="margin-left: 145px;margin-top: 40px;margin-bottom: 21px;background-color: #58007D;width: 180px;height: 40px;border-radius: 25px;">
                <button data-toggle="modal" data-dismiss="modal" data-target="#modalHoganGroupOrder" style="background-color: #ffffff00;color: white;border-style: none;margin-top: 7px; margin-left: 15px;font-family: 'avertaregular';">
                    Create group order
                </button>
            </div>

            </div>

        </div>

    </div>


    <!-- Modal -->
    <div class="modal fade" id="modalSignUp" role="dialog" style="overflow-y: scroll;">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content" style="border-radius: 20px;">
                <div class="modal-header">
                    <button type="button" class="close" style="margin-left: 10px;" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title" style="margin-right: 190px;font-size: 20px;font-family: 'avertaregular';">Sign up</h4>
                </div>
                <form action="#">

                    <label style="margin-left: 55px;color: #707070;margin-top: 20px;font-family: 'avertaregular';">Name</label>

                    <div class="form-row" style="margin-left: 40px;margin-right: 40px;">
                        <div class="col">
                            <input type="text" class="form-control" style="border-radius: 10px;margin-right: 40px;" placeholder="Enter first name" name="email">
                        </div>

                    </div>

                    <label style="margin-left: 55px;color: #707070;margin-top: 20px;font-family: 'avertaregular';">Email</label>

                    <div class="form-row" style="margin-left: 40px;margin-right: 40px;">
                        <div class="col">
                            <input type="text" class="form-control" style="border-radius: 10px;margin-right: 40px;" placeholder="Email" name="email">
                        </div>

                    </div>
                    <label style="margin-left: 55px;color: #707070;margin-top: 20px;font-family: 'avertaregular';">Mobile number</label>

                    <div class="form-row" style="margin-left: 40px;margin-right: 40px;">
                        <div class="col">
                            <input type="text" class="form-control" style="border-radius: 10px;margin-right: 40px;" placeholder="Mobile number" name="email">
                        </div>

                    </div>
                    <div style="margin-left: 170px;padding-bottom: 10px;padding-top: 10px;">
                        <button style="font-family: 'avertaregular';" type="button" class="btn btn-default button-continue" >Continue</button>
                    </div>

                    <div>
                        <h3 style="font-family: 'avertaregular';" class="have-an-account">
                            Already have an account? <a style="font-family: 'avertaregular';color: #58017D;" data-toggle="modal" data-dismiss="modal" data-target="#modalLogin"> <strong> Login </strong></a>
                        </h3>
                    </div>

                </form>

                <div>
                    <hr>
                    <h3 style="font-family: 'avertaregular';" class="have-an-account">or sign up using Social media</h3>
                    <div style="margin-left: 190px;margin-bottom: 30px;">
                        <span class="iconify" data-icon="bx:bxl-facebook-circle" style="font-size: 30px;color: #5757ec;" data-inline="false"></span>
                        <span class="iconify" data-icon="ant-design:google-circle-filled" style="font-size: 30px; color: red;" data-inline="false"></span>
                        <span class="iconify" data-icon="ant-design:twitter-circle-filled" style="font-size: 30px;color: #3a3af7cf;" data-inline="false"></span>
                    </div>
                </div>

                <div style="padding-bottom: 10px;">
                    <h3 class="have-an-account" style="font-family: 'avertaregular';text-align: center;margin-left: -10px;padding-bottom: 10px;">
                        By continuing, you agree to our<br>
                        <a style="font-family: 'avertaregular';color: #58017D;" href="Terms & Conditions.html"> <strong> Terms of use,</strong></a>
                        <a style="font-family: 'avertaregular';color: #58017D;" href="Terms & Conditions.html"> <strong> Privacy policy </strong></a> and 
                        <a style="font-family: 'avertaregular';color: #58017D;"> <strong style="font-family: 'avertaregular';"> Vendor </strong></a>
                    </h3>
                </div>

            </div>

        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="modalAllOrders" role="dialog" style="overflow-y: scroll;">
        <div class="modal-dialog ">

            <!-- Modal content-->
            <div class="modal-content modal-all-order-form">
                <div class="modal-header" style="margin-top: 35px;">
                    <button type="button" class="close" style="margin-left: 10px;background-color: white;border-radius: 25px;width: 15px;height: 15px;margin-top: 3px;" data-dismiss="modal">
                        <h3 style="font-size: 20px;margin-top: -12px;margin-left: -5px;">x</h3>
                    </button>
                    <!-- <h4 class="modal-title" style="margin-right: 115px;font-size: 20px;">Edit profile</h4> -->
                    <div class="modal-div-all-orders-active">
                        <button style="font-family: 'avertaregular';" class="modal-button-all-orders-active">
                            Active orders
                        </button>
                        <button class="modal-button-all-orders-active" style="font-family: 'avertaregular';background-color: white;border-color: white;color: #C8C8C8;">
                            Previous orders
                        </button>
                        <button class="modal-button-all-orders-active" style="font-family: 'avertaregular';background-color: white;border-color: white;color: #C8C8C8;">
                            Group orders
                        </button>
                    </div>

                </div>

                <div style="height: 100px;width: 410px;background-color: white;border-radius: 25px;margin-left: 42px;margin-top: 31px;">

                    <div>
                        <img src="assets/images/siMhabW0AA5BWx.jpg" style="width: 80px;border-radius: 25px;margin-top: 10px;margin-left: 10px;">
                    </div>

                    <div style="width: 275px;margin-left: 115px;margin-top: -77px;">

                        <h4 style="font-family: 'avertaregular';font-size: 15px;padding-bottom: 14px;">
                            Banku and Tilapia
                        </h4>
                        <h4 style="font-family: 'avertaregular';color: #5A5653;font-size: 12px;">
                            Lunch City
                        </h4>
                        <h4 style="font-family: 'avertaregular';color: #5A5653;font-size: 12px;">
                            GHC 35
                        </h4>
                        <h4 style="text-align: right;margin-top: -87px;margin-bottom: 42px;">
                            <i class="fa fa-angle-right"></i>
                        </h4>
                        <h4 style="font-family: 'avertaregular';font-size: 12px;text-align: right;">
                            Picked up by rider
                        </h4>
                    </div>

                </div>

            </div>

        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="modalEditProfile" role="dialog" style="overflow-y: scroll;">
        <div class="modal-dialog modal-sm">

            <!-- Modal content-->
            <div class="modal-content" style="border-radius: 20px;width: 360px;">
                <div class="modal-header">
                    <button type="button" class="close" style="margin-left: 10px;" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title" style="font-family: 'avertaregular';margin-right: 115px;font-size: 20px;">Edit profile</h4>
                </div>
                <form action="#">

                    <label style="margin-left: 55px;color: #707070;margin-top: 20px;">Name</label>

                    <div class="form-row" style="margin-left: 40px;margin-right: 40px;">
                        <div class="col">
                            <input type="text" class="form-control" style="border-radius: 10px;margin-right: 40px;" placeholder="Enter first name" name="email">
                        </div>

                    </div>

                    <label style="margin-left: 55px;color: #707070;margin-top: 20px;font-family: 'avertaregular';">Email</label>

                    <div class="form-row" style="margin-left: 40px;margin-right: 40px;">
                        <div class="col">
                            <input type="text" class="form-control" style="border-radius: 10px;margin-right: 40px;" placeholder="Email" name="email">
                        </div>

                    </div>
                    <label style="margin-left: 55px;color: #707070;margin-top: 20px;font-family: 'avertaregular';">Mobile number</label>

                    <div class="form-row" style="margin-left: 40px;margin-right: 40px;">
                        <div class="col">
                            <input type="text" class="form-control" style="border-radius: 10px;margin-right: 40px;" placeholder="Mobile number" name="email">
                        </div>

                    </div>
                    <div style="margin-left: 115px;padding-bottom: 10px;padding-top: 10px;">
                        <button style="font-family: 'avertaregular';" type="button" class="btn btn-default button-continue">Save</button>
                    </div>

                    <div>
                        <h4 style="font-family: 'avertaregular';color: #01A517;font-size: 12px;margin-left: 96px;padding-bottom: 20px;padding-top: 20px;">
                            Profile successfully updated.
                        </h4>
                    </div>
                </form>

            </div>

        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="modalLogin" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content" style="border-radius: 20px;">
                <div class="modal-header">
                    <button type="button" class="close" style="margin-left: 10px;" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title" style="margin-right: 190px;font-size: 20px;font-family: 'avertaregular';">Log in</h4>
                </div>
                <form action="#">

                    <label style="margin-left: 55px;color: #707070;margin-top: 20px;font-family: 'avertaregular';">Phone number or email</label>

                    <div class="form-row" style="margin-left: 40px;margin-right: 40px;">
                        <div class="col">
                            <input type="text" class="form-control" style="font-family: 'avertaregular';border-radius: 10px;margin-right: 40px;" placeholder="Enter Phone numer or email here...." name="email">
                        </div>

                    </div>

                    <label style="margin-left: 55px;color: #707070;margin-top: 20px;font-family: 'avertaregular';">Password</label>

                    <div class="form-row" style="margin-left: 40px;margin-right: 40px;">
                        <div class="col">
                            <input type="password" class="form-control" style="font-family: 'avertaregular';border-radius: 10px;margin-right: 40px;" placeholder="" name="email">
                        </div>

                    </div>
                    <div style="font-family: 'avertaregular';margin-left: 170px;padding-bottom: 10px;padding-top: 10px;">
                        <button type="button" class="btn btn-default button-continue">Log in</button>
                    </div>

                    <div>
                        <h3 class="have-an-account">
                            <a style="font-family: 'avertaregular';color: #58017D;"> <strong> Forgot Password </strong></a>
                        </h3>
                    </div>
                    <div>
                        <h3 style="font-family: 'avertaregular';" class="have-an-account">
                            Don't have an account <a style="font-family: 'avertaregular';color: #58017D;" data-toggle="modal" data-dismiss="modal" data-target="#modalSignUp" > <strong> Sign Up </strong></a>
                        </h3>
                    </div>

                </form>

            </div>

        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="modalMyRatingHistory" role="dialog" style="overflow-y: scroll;">
        <div class="modal-dialog ">

            <!-- Modal content-->
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" style="margin-left: 10px;" data-dismiss="modal">&times;</button>
                    <div>
                        <h4 style="font-size: 20px;text-align: center;margin-right: 150px;margin-top: 20px;font-family: 'avertaregular';">My ratings history</h4>
                    </div>

                </div>

                <div>
                    <h4 style="font-family: 'avertaregular';font-size: 20px;margin-left: 100px;margin-top: 10px;margin-bottom: 20px;">
                       You have given 117 ratings & reviews
                    </h4>
                </div>

                <div class="row row-media" style="width: 330px;margin-left: 103px;padding-bottom: 20px;">

                    <div class="col-sm-12" style="font-family: 'avertaregular';background-color:#EFEFEF;margin-top: 5px;margin-bottom: 20px;border-radius: 11px;">
                        <h4 style="font-size: 17px;font-family: 'avertaregular';margin-top: 15px;">
                            Hoogah Osu
                        </h4>

                        <div style="margin-top: -34px;margin-left: 200px;">
                            <span class="iconify" data-icon="ant-design:star-filled" data-inline="false" style="color: green"></span>
                            <span class="iconify" data-icon="dashicons:star-filled" data-inline="false" style="color: green"></span>
                            <span class="iconify" data-icon="dashicons:star-filled" data-inline="false" style="color: green"></span>
                            <span class="iconify" data-icon="dashicons:star-filled" data-inline="false" style="color: green"></span>
                            <span class="iconify" data-icon="dashicons:star-filled" data-inline="false" style="color: green"></span>
                        </div>

                        <div style="font-family: 'avertaregular';font-size: 15px;margin-top: 25px;">
                            The quick, brown fox jumps over a lazy dog. DJs flock by when MTV ax quiz prog. Junk MTV quiz graced by fox whelps. Bawds jog, flick quartz
                        </div>

                        <div style="margin-top: 20px;padding-bottom: 10px;">
                            <img src="assets/images/like.svg" style="margin-top: -7px;"> 12
                            <img src="assets/images/dislike.svg" style="padding-left: 10px;margin-top: -6px;"> 2

                            <h4 style="font-family: 'avertaregular';color: #707070;font-size: 15px;margin-left: 210px;margin-top: -20px;">
                            24 Jan 2020
                        </h4>
                        </div>

                    </div>

                    <div class="col-sm-12" style="background-color:#EFEFEF;margin-top: 5px;margin-bottom: 20px;border-radius: 11px;">
                        <h4 style="font-size: 17px;margin-top: 15px;font-family: 'avertaregular';">
                           The Piano Bar
                        </h4>

                        <div style="margin-top: -34px;margin-left: 200px;">
                            <span class="iconify" data-icon="ant-design:star-filled" data-inline="false" style="color: green"></span>
                            <span class="iconify" data-icon="dashicons:star-filled" data-inline="false" style="color: green"></span>
                            <span class="iconify" data-icon="dashicons:star-filled" data-inline="false" style="color: green"></span>
                            <span class="iconify" data-icon="dashicons:star-filled" data-inline="false" style="color: green"></span>
                            <span class="iconify" data-icon="dashicons:star-filled" data-inline="false" style="color: green"></span>
                        </div>

                        <div style="font-size: 15px;margin-top: 25px;font-family: 'avertaregular';">
                            The quick, brown fox jumps over a lazy dog. DJs flock by when MTV ax quiz prog. Junk MTV quiz graced by fox whelps. Bawds jog, flick quartz
                        </div>

                        <div style="margin-top: 20px;padding-bottom: 10px;">
                            <img src="assets/images/like.svg" style="margin-top: -7px;font-family: 'avertaregular';"> 12
                            <img src="assets/images/dislike.svg" style="padding-left: 10px;margin-top: -6px;font-family: 'avertaregular';"> 2

                            <h4 style="color: #707070;font-size: 15px;margin-left: 210px;margin-top: -20px;font-family: 'avertaregular';">
                            24 Jan 2020
                        </h4>
                        </div>

                    </div>

                    <div class="col-sm-12" style="background-color:#EFEFEF;margin-top: 5px;margin-bottom: 20px;border-radius: 11px;">
                        <h4 style="font-size: 17px;margin-top: 15px;font-family: 'avertaregular';">
                            Bloom Bar
                        </h4>

                        <div style="margin-top: -34px;margin-left: 200px;">
                            <span class="iconify" data-icon="ant-design:star-filled" data-inline="false" style="color: green"></span>
                            <span class="iconify" data-icon="dashicons:star-filled" data-inline="false" style="color: green"></span>
                            <span class="iconify" data-icon="dashicons:star-filled" data-inline="false" style="color: green"></span>
                            <span class="iconify" data-icon="dashicons:star-filled" data-inline="false" style="color: green"></span>
                            <span class="iconify" data-icon="dashicons:star-filled" data-inline="false" style="color: green"></span>
                        </div>

                        <div style="font-size: 15px;margin-top: 25px;font-family: 'avertaregular';">
                            The quick, brown fox jumps over a lazy dog. DJs flock by when MTV ax quiz prog. Junk MTV quiz graced by fox whelps. Bawds jog, flick quartz
                        </div>

                        <div style="margin-top: 20px;padding-bottom: 10px;">
                            <img src="assets/images/like.svg" style="margin-top: -7px;font-family: 'avertaregular';"> 12
                            <img src="assets/images/dislike.svg" style="padding-left: 10px;margin-top: -6px;font-family: 'avertaregular';"> 2

                            <h4 style="color: #707070;font-size: 15px;font-family: 'avertaregular';margin-left: 210px;margin-top: -20px;">
                            24 Jan 2020
                        </h4>
                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>

    <!-- Modal -->
    <div class="modal fade" id="modalMyRateRestaurants" role="dialog" style="overflow-y: scroll;">
        <div class="modal-dialog ">

            <!-- Modal content-->
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" style="margin-left: 10px;" data-dismiss="modal">&times;</button>
                    <div>
                        <h4 style="font-size: 20px;text-align: center;margin-right: 200px;margin-top: 20px;font-family: 'avertaregular';">Hoogah</h4>
                    </div>

                </div>

                <div class="rating-media">
                    <h4 style="font-family: 'avertaregular';font-size: 60px;margin-left: 100px;margin-top: 30px;margin-bottom: 20px;">
                       4.6
                    </h4>
                </div>

                <div class="stars-media" style="margin-left: 100px;margin-bottom: 20px;">

                    <span class="iconify" data-icon="ant-design:star-filled" data-inline="false" style="color: green"></span>
                    <span class="iconify" data-icon="dashicons:star-filled" data-inline="false" style="color: green"></span>
                    <span class="iconify" data-icon="dashicons:star-filled" data-inline="false" style="color: green"></span>
                    <span class="iconify" data-icon="dashicons:star-filled" data-inline="false" style="color: green"></span>
                    <span class="iconify" data-icon="dashicons:star-filled" data-inline="false" style="color: green"></span>

                </div>

                <div class="number-rating" style="margin-left: 100px;margin-bottom: 40px;">

                    <h4 style="font-family: 'avertaregular';font-size: 15px;">
                        1,008 ratings
                    </h4>

                </div>

                <div class="more-stars-media" style="margin-left: 318px;margin-top: -200px;margin-bottom: 80px;">

                    <div style="margin-bottom: -27px;margin-left: -30px;">
                        <h4 style="font-family: 'avertaregular';font-size: 10px">
                            67%
                        </h4>
                    </div>

                    <div style="padding-bottom: 10px;">

                        <span class="iconify" data-icon="ant-design:star-filled" data-inline="false" style="color: #C8C8C8"></span>
                        <span class="iconify" data-icon="dashicons:star-filled" data-inline="false" style="color: #C8C8C8"></span>
                        <span class="iconify" data-icon="dashicons:star-filled" data-inline="false" style="color: #C8C8C8"></span>
                        <span class="iconify" data-icon="dashicons:star-filled" data-inline="false" style="color: #C8C8C8"></span>
                        <span class="iconify" data-icon="dashicons:star-filled" data-inline="false" style="color: #C8C8C8"></span>

                    </div>

                    <div style="margin-bottom: -27px;margin-left: -30px;">
                        <h4 style="font-family: 'avertaregular';font-size: 10px">
                            14%
                        </h4>
                    </div>

                    <div style="padding-bottom: 10px;">

                        <span class="iconify" data-icon="ant-design:star-filled" data-inline="false" style="color: #C8C8C8"></span>
                        <span class="iconify" data-icon="dashicons:star-filled" data-inline="false" style="color: #C8C8C8"></span>
                        <span class="iconify" data-icon="dashicons:star-filled" data-inline="false" style="color: #C8C8C8"></span>
                        <span class="iconify" data-icon="dashicons:star-filled" data-inline="false" style="color: #C8C8C8"></span>

                    </div>

                    <div style="margin-bottom: -27px;margin-left: -30px;">
                        <h4 style="font-family: 'avertaregular';font-size: 10px">
                            10%
                        </h4>
                    </div>

                    <div style="padding-bottom: 10px;">

                        <span class="iconify" data-icon="ant-design:star-filled" data-inline="false" style="color: #C8C8C8"></span>
                        <span class="iconify" data-icon="dashicons:star-filled" data-inline="false" style="color: #C8C8C8"></span>
                        <span class="iconify" data-icon="dashicons:star-filled" data-inline="false" style="color: #C8C8C8"></span>

                    </div>

                    <div style="margin-bottom: -27px;margin-left: -30px;">
                        <h4 style="font-family: 'avertaregular';font-size: 10px">
                            5%
                        </h4>
                    </div>

                    <div style="padding-bottom: 10px;">

                        <span class="iconify" data-icon="ant-design:star-filled" data-inline="false" style="color: #C8C8C8"></span>
                        <span class="iconify" data-icon="dashicons:star-filled" data-inline="false" style="color: #C8C8C8"></span>

                    </div>

                    <div style="margin-bottom: -27px;margin-left: -30px;">
                        <h4 style="font-family: 'avertaregular';font-size: 10px">
                            4%
                        </h4>
                    </div>

                    <div style="padding-bottom: 10px;">

                        <span class="iconify" data-icon="ant-design:star-filled" data-inline="false" style="color: #C8C8C8"></span>

                    </div>

                </div>

                <div class="row row-media" style="width: 330px;margin-left: 103px;padding-bottom: 20px;">

                    <div class="col-sm-12" style="font-family: 'avertaregular';background-color:#EFEFEF;margin-top: 5px;margin-bottom: 20px;border-radius: 11px;">
                        <h4 style="font-size: 17px;margin-top: 15px;">
                            Kafui
                        </h4>

                        <div style="margin-top: -34px;margin-left: 200px;">
                            <span class="iconify" data-icon="ant-design:star-filled" data-inline="false" style="color: green"></span>
                            <span class="iconify" data-icon="dashicons:star-filled" data-inline="false" style="color: green"></span>
                            <span class="iconify" data-icon="dashicons:star-filled" data-inline="false" style="color: green"></span>
                            <span class="iconify" data-icon="dashicons:star-filled" data-inline="false" style="color: green"></span>
                            <span class="iconify" data-icon="dashicons:star-filled" data-inline="false" style="color: green"></span>
                        </div>

                        <div style="font-family: 'avertaregular';font-size: 15px;margin-top: 25px;">
                            The quick, brown fox jumps over a lazy dog. DJs flock by when MTV ax quiz prog. Junk MTV quiz graced by fox whelps. Bawds jog, flick quartz
                        </div>

                        <div style="margin-top: 20px;padding-bottom: 10px;">
                            <img src="assets/images/like.svg" style="margin-top: -7px;font-family: 'avertaregular';"> 12
                            <img src="assets/images/dislike.svg" style="padding-left: 10px;font-family: 'avertaregular';margin-top: -6px;"> 2

                            <h4 style="font-family: 'avertaregular';color: #707070;font-size: 15px;margin-left: 210px;margin-top: -20px;">
                            24 Jan 2020
                        </h4>
                        </div>

                    </div>

                    <div class="col-sm-12" style="background-color:#EFEFEF;margin-top: 5px;margin-bottom: 20px;border-radius: 11px;">
                        <h4 style="font-family: 'avertaregular';font-size: 17px;margin-top: 15px;">
                           Kafui
                        </h4>

                        <div style="margin-top: -34px;margin-left: 200px;">
                            <span class="iconify" data-icon="ant-design:star-filled" data-inline="false" style="color: green"></span>
                            <span class="iconify" data-icon="dashicons:star-filled" data-inline="false" style="color: green"></span>
                            <span class="iconify" data-icon="dashicons:star-filled" data-inline="false" style="color: green"></span>
                            <span class="iconify" data-icon="dashicons:star-filled" data-inline="false" style="color: green"></span>
                            <span class="iconify" data-icon="dashicons:star-filled" data-inline="false" style="color: green"></span>
                        </div>

                        <div style="font-family: 'avertaregular';font-size: 15px;margin-top: 25px;">
                            The quick, brown fox jumps over a lazy dog. DJs flock by when MTV ax quiz prog. Junk MTV quiz graced by fox whelps. Bawds jog, flick quartz
                        </div>

                        <div style="margin-top: 20px;padding-bottom: 10px;">
                            <img src="assets/images/like.svg" style="margin-top: -7px;font-family: 'avertaregular';"> 12
                            <img src="assets/images/dislike.svg" style="padding-left: 10px;margin-top: -6px;font-family: 'avertaregular';"> 2

                            <h4 style="font-family: 'avertaregular';color: #707070;font-size: 15px;margin-left: 210px;margin-top: -20px;">
                            24 Jan 2020
                        </h4>
                        </div>

                    </div>

                    <div style="margin-left: 93px;margin-bottom: 20px;margin-top: 25px;">

                        <button data-toggle="modal" data-dismiss="modal" data-target="#modalRateRestaurants" style="font-family: 'avertaregular';background-color: #58007D;border-color: #58007D;border-radius: 25px;color: white;font-size: 15px;width: 140px;height: 40px;">
                            Rate Hoogah
                        </button>

                    </div>

                </div>

            </div>

        </div>

    </div>


    <div class="modal fade" id="modalJoinGroupOrder" role="dialog" style="overflow-y: scroll;">

        <div class="modal-dialog ">

            <!-- Modal content-->
            <div class="modal-content" style="background-color: #F6F6F6;width: 568px;">

                <div style="background-color: white;padding-top: 40px;height: 90px;">
                    
                    <div class="hoogah-div-media" style="font-family: 'avertaregular';margin-left: 185px;">
                        
                        Hoogah Group Order

                    </div>

                    <div class="close-div-media" style="font-family: 'avertaregular';color: #58017D;margin-left: 468px;margin-top: -25px;" data-dismiss="modal">
                        Close
                    </div>



                </div>

                <div class="desc-div-media" style="background-color: #EFDFF6;width: 330px;height: 60px;margin-left: 100px;margin-top: 20px;margin-bottom: 20px;">
                    
                    <p style="font-family: 'avertaregular';color: #58017D;text-align: center;font-size: 17px;padding-left: 21px;padding-right: 21px;">
                        This group order is created by Kafui and will be sent at 12:00pm
                    </p>
                </div>

                <div class="total-order-media" style="margin-bottom: 20px">

                    <div style="padding-top: 20px;">
                        
                        <p style="font-family: 'avertaregular';color: #707070;font-size: 17px;margin-left: 98px;">
                            Total order
                        </p>


                        <p style="font-family: 'avertaregular';color: black;font-size: 17px;margin-top: -28px;margin-left: 377px;">
                            GHC 00
                        </p>
                    </div>
                    

                    <div style="padding-top: 20px;">
                        
                        <p style="font-family: 'avertaregular';color: #707070;font-size: 17px;margin-left: 98px;">
                            Delivery charge
                        </p>


                        <p style="font-family: 'avertaregular';color: black;font-size: 17px;margin-top: -28px;margin-left: 377px;">
                            GHC 00
                        </p>
                    </div>


                    <div style="padding-top: 20px;">
                        
                        <p style="font-family: 'avertaregular';color: #707070;font-size: 17px;margin-left: 98px;">
                            Total price
                        </p>


                        <p style="font-family: 'avertaregular';color: black;font-size: 17px;margin-top: -28px;margin-left: 377px;">
                            GHC 00
                        </p>
                    </div>


                </div>

                <div>
                        

                        <div style="margin-bottom: 50px;">
                        


                        <div class="received-div-media" style="font-family: 'avertaregular';background-color: white;width: 353px;height: 75px;margin-left: 100px;font-size: 17px;border-radius: 21px;padding-left: 51px;padding-top: 14px;">
                            
                            Receive 35% discount on delivery when your order exceeds GHC 265.
                        </div> 

                         <div class="info-media-div" style="margin-left: 113px;margin-top: -48px;">
                            
                            <img src="assets/images/info.svg">
                        </div>

                        </div>               



                </div>


                <div style="margin-bottom: 25px">

                    <div>
                        
                        <hr class="hr-received-media" style="width: 353px;">

                        <div class="name-div-media" style="margin-left: 105px;margin-top: 25px">
                            <div>
                            <p style="font-family: 'avertaregular';font-size: 17px;font-family: 'avertaregular';">
                                Kafui (you)
                            </p>

                            <p style="font-size: 17px;color: #707070;font-family: 'avertaregular';margin-top: -23px;margin-left: 275px;">
                                GHC 59.69
                            </p>
                        </div>

                            <div style="padding-top: 10px">
                                <p style="font-size: 17px; color: #707070">
                                   English Breakfast  <span style="color: #58017D;font-family: 'avertaregular';margin-left: 10px;"> x1 </span> 
                                </p>

                                <p style="font-size: 17px;color: #707070">
                                    Banku and tilapia  <span style="color: #58017D;font-family: 'avertaregular';margin-left: 10px;"> x1 </span> 
                                </p>
                                
                            </div>
                            <div style=" margin-left: 347px;margin-top: -26px;">
                               <i class="fa fa-ellipsis-h"></i>
                            </div>
                        </div>

                    </div>


                    <div style="margin-bottom: 25px">
                        
                        <hr class="hr-received-media" style="width: 353px;">

                        <div class="name-div-media" style="margin-left: 105px;margin-top: 25px">
                            <div>
                            <p style="font-size: 17px;font-family: 'avertaregular';">
                                Henry Armatefio
                            </p>

                            <p style="font-size: 17px;color: #707070;font-family: 'avertaregular';margin-top: -23px;margin-left: 275px;">
                                GHC 47.40
                            </p>
                        </div>

                            <div style="padding-top: 10px">
                                <p style="font-size: 17px; color: #707070">
                                   Banku and tilapia  <span style="color: #58017D;font-family: 'avertaregular';margin-left: 10px;"> x1 </span> 
                                </p>

                                
                                
                            </div>
                            
                        </div>

                    </div>



                    <div style="margin-bottom: 25px">
                        
                        <hr class="hr-received-media" style="width: 353px;">

                        <div class="name-div-media" style="margin-left: 105px;margin-top: 25px">
                            <div>
                            <p style="font-size: 17px;font-family: 'avertaregular';">
                                Nii Osa
                            </p>

                            <p style="font-size: 17px;color: #707070;font-family: 'avertaregular';margin-top: -23px;margin-left: 275px;">
                                GHC 101.33
                            </p>
                        </div>

                            <div style="padding-top: 10px">
                                <p style="font-size: 17px; color: #707070">
                                  Fufu with goat meat  <span style="color: #58017D;font-family: 'avertaregular';margin-left: 10px;"> x1 </span> 
                                </p>

                                <p style="font-size: 17px;color: #707070">
                                   Krusher  <span style="color: #58017D;font-family: 'avertaregular';margin-left: 10px;"> x1 </span> 
                                </p>

                                <p style="font-size: 17px;color: #707070">
                                   Plantain with koobi  <span style="color: #58017D;font-family: 'avertaregular';margin-left: 10px;"> x1 </span> 
                                </p>
                                
                            </div>
                            
                        </div>

                    </div>
                    



                </div>



                <div style="background-color: white">
                    


                    <button class="share-group-media" style="font-family: 'avertaregular';background-color: #58007D;color: white;width: 129px;height: 50px;border-radius: 25px;margin-left: 221px;margin-top: 15px;margin-bottom: 15px;">
                        Share group
                    </button>



                </div>


            

            </div>


        </div>

    </div>


    <div class="modal fade" id="modalDiscount" role="dialog" style="overflow-y: scroll;">

        <div class="modal-dialog ">

            <!-- Modal content-->
            <div class="modal-content" style="background-color: #F6F6F6">


            <div>

                <div style="background-color: white;padding-top: 20px;padding-bottom: 15px;">

                    <div style="margin-left: 35px;" data-dismiss="modal">
                        <img src="assets/images/arrow-left.svg">
                    </div>

                    <div style="margin-top: -22px;margin-left: 160px;font-family: 'avertaregular';">
                         Discounts
                    </div>

                    </div>
                   
                </div>


                <div>
                        
                        <div class="div-paragraph" style="background-color: white;border-radius: 25px;width: 420px;margin-top: 30px;margin-bottom: 30px;margin-left: 35px;padding-bottom: 8px;">

                            <div  style="margin-left: 33px;padding-top: 20px;font-family: 'avertaregular';">
                                GH100
                            </div>

                            <div style="color: #5A5653;font-family: 'avertaregular';padding-right: 30px ! important;padding-left: 30px ! important;">
                                It's been a year together and we thank you for it. No anniversary is complete without gifts. So here's a FREE GHC 100 applied to delivery for your next order. Expires in 7 days so get eating.
                            </div>

                            <div style="background-color: #58017D;border-radius: 25px;width: 100px;height: 40px;margin-bottom: 15px;margin-left: 50px;margin-top: 41px;">
                                <button style="font-family: 'avertaregular';background-color: #ffffff00;border-style: none;color: white;margin-left: 14px;margin-top: 5px;margin-left: 26px;">
                                    Apply
                                </button>
                            </div>
                            

                        </div>

                </div>

                <div>
                    
                    <div class="enter-custom-discount" style="font-family: 'avertaregular';color: #CCCCCC;background-color: white;border-radius: 25px;margin-bottom: 200px;width: 420px;margin-left: 35px;height: 57px;padding-left: 46px;padding-top: 16px;">
                            
                        Enter custom discount code
                            
                    </div>


                </div>


            </div>


        </div>
    </div>


    <!-- Modal -->
    <div class="modal fade" id="modalHoganGroupOrder" role="dialog" style="overflow-y: scroll;">

        <div class="modal-dialog ">

            <!-- Modal content-->
            <div class="modal-content" style="background-color: #F6F6F6">


            <div>
                
                <div class="group-media" style="margin-top: 50px;margin-bottom: 50px;">

                    <div style="margin-left: 160px;font-family: 'avertaregular';">
                         Hoogah Group Order
                    </div>

                    <div style="font-family: 'avertaregular';margin-top: -25px;margin-left: 420px;color: #58017D" data-dismiss="modal">
                        Close
                    </div>
                   
                </div>


                <div class="div-group-div-media" style="background-color: #EFDFF6;width: 370px;text-align: center;margin-left: 60px;margin-bottom: 40px;">
                    
                    <p style="font-family: 'avertaregular';color: #58017D;padding-left: 50px;padding-right: 50px;padding-top: 10px;">
                        This group order is created by Kafui and will be sent at 12:00pm
                    </p>

                </div>

                <div class="total-div-media">
                    
                        <div style="margin-left: 60px;padding-top: 10px;">
                            <h4 style="font-family: 'avertaregular';font-size: 17px;color: #707070">
                                Total order
                            </h4>

                            <h4 style="font-family: 'avertaregular';font-size: 17px;margin-top: -25px;margin-left: 310px;">
                                GHC 00
                            </h4>
                        </div>

                        <div style="margin-left: 60px;padding-top: 10px;">
                            <h4 style="font-family: 'avertaregular';font-size: 17px;color: #707070">
                                Delivery charge
                            </h4>

                            <h4 style="font-family: 'avertaregular';font-size: 17px;margin-top: -25px;margin-left: 310px;">
                                GHC 00
                            </h4>
                        </div>


                        <div style="margin-left: 60px;padding-top: 10px;">
                            <h4 style="font-family: 'avertaregular';font-size: 17px;color: #707070">
                                Total price
                            </h4>

                            <h4 style="font-family: 'avertaregular';font-size: 17px;margin-top: -25px;margin-left: 310px;">
                                GHC 00
                            </h4>
                        </div>

                </div>

                

                <div class="div-receive-media">
                    

                    <div style="background-color: white;width: 370px;height: 69px;border-radius: 25px;margin-top: 55px;margin-left: 62px;margin-bottom: 50px;">
                        <img src="assets/images/info.svg" style="margin-top: 23px;margin-left: 30px;">

                        <p style="font-family: 'avertaregular';margin-left: 74px;margin-top: -34px;">
                            Receive 35% discount on delivery when your order exceeds GHC 265.
                        </p>
                    </div>


                </div>

                <div style="padding-bottom: 30px;">
                    
                    <div class="join-group-media" style="background-color: #58007D;width: 120px;height: 40px;margin-left: 200px;border-radius: 25px;">
                        
                        
                        <button data-toggle="modal" data-dismiss="modal" data-target="#modalJoinGroupOrder" style="font-family: 'avertaregular';background-color: #ffffff00;border-style: none;color: white;text-align: center;margin-left: 15px;margin-top: 6px;">

                            Join group
                            
                        </button>

                    </div>

                </div>

            </div>

        </div>


        </div>

    </div>



    <!-- Modal -->
    <div class="modal fade" id="modalBasket" role="dialog" style="overflow-y: scroll;">
        <div class="modal-dialog ">

            <!-- Modal content-->
            <div class="modal-content" style="background-color: #FFFFFF; border-style: solid; border-width: 1px; border-color: #EEEAEA4D;">

                <div  style="opacity: 1;background-color: white;width: 30px;height: 30px;border-radius: 25px;margin-left: 15px;margin-top: 10px;">
                    <button type="button" class="close" style="margin-top: 1px;margin-right: 6px;" data-dismiss="modal" >&times;</button>

                </div>

                <div class="modal-basket-media">
                    <div style="font-family: 'avertaregular';font-size: 20px;margin-left: 225px;/* margin-bottom: 10px; */margin-top: -22px;">
                        Basket
                    </div>
                    <div>

                        <div style="margin-left: 145px;">
                            <img src="assets/images/hotel-logo.png" style="width: 40px;">
                        </div>

                        <div style="font-family: 'avertaregular';margin-top: -45px;margin-left: 210px;font-size: 17px;">
                        Pan and Cook
                        </div>
                        <div style="font-family: 'avertaregular';color: #707070;margin-left: 210px;">
                            Delivery is in 30 - 35 mins
                        </div>
                    </div>
                </div>


                <div style="background-color: #F6F6F6;height: 400px">

                    <div class="div-media-banku" style="background-color: #FFFFFF;width: 400px;height: 100px;margin-top: 30px;margin-left: 60px;border-radius: 30px;">

                        <div>
                        <img src="assets/images/banku-&-tilapia-small.png" style="width: 85px;border-radius: 25px;margin-top: 8px;margin-left: 15px;">
                        </div>

                        <div class="div-banku-inner-div" style="margin-top: -90px;margin-left: 140px;">
                            <h4 style="font-family: 'avertaregular';font-size: 17px">
                                Banku and Tilapia
                            </h4>

                            <h4 style="font-family: 'avertaregular';color: #58017D;font-size: 14px;">
                                Coke
                            </h4>

                            <h4 style="font-family: 'avertaregular';color: #58017D;font-size: 14px;">
                                1 bottled water
                            </h4>

                            <h4 style="font-family: 'avertaregular';color: #58017D;font-size: 14px;">
                                Orange juice
                            </h4>
                        </div>

                        <div class="amount-media-div" style="margin-top: -90px;margin-left: 329px;">
                            <h4 style="font-family: 'avertaregular';color: #58017D; font-size: 17px">
                                x3
                            </h4>

                            <br>


                            <h4 style="font-family: 'avertaregular';font-size: 17px">
                                GHC 84
                            </h4>
                        </div>
                        
                    </div>


                    <div data-toggle="modal" data-dismiss="modal" data-target="#modalDiscount"  class="available-div-media" style="font-family: 'avertaregular';background-color: #EEEAEA;width: 350px;height: 50px;margin-top: 200px;margin-left: 100px;border-radius: 15px;">

                        <div style="font-family: 'avertaregular';font-size: 17px;padding-top: 15px;padding-left: 25px;">
                            Available Discounts and promos
                        </div>

                        <div style="margin-left: 300px;margin-top: -28px;">
                            <img src="assets/images/arrow-right.svg">
                        </div>
                        
                    </div>
                    

                </div>

                <div class="delivery-div-media">
                    
                    <div style="font-family: 'avertaregular';margin-left: 145px;margin-top: 20px;">
                        Delivery charge
                    </div>
                    <div style="font-family: 'avertaregular';color: #58017D;margin-left: 145px;">
                        Total bill
                    </div>

                    <div style="font-family: 'avertaregular';color: #000000;margin-left: 350px;margin-top: -41px;">
                        
                        GHC 14.78
                    </div>

                    <div style="font-family: 'avertaregular';color: #58017D;margin-left: 270px;margin-left: 350px;">
                        GHC 144.78
                    </div>


                    <div style="background-color: #58017D;width: 140px;height: 40px;border-radius: 25px;padding-top: 5px;padding-left: 10px;margin-left: 190px;margin-bottom: 20px;margin-top: 25px;">
                        
                        <button style="font-family: 'avertaregular';color: white; background-color: #ffffff00; border-style: none;">
                            <a href="check-out.html" style="font-family: 'avertaregular';">Go to checkout</a>
                        </button>

                    </div>

                </div>

            </div>


        </div>

    </div>





    <!-- Modal -->
    <div class="modal fade" id="modalFood" role="dialog" style="overflow-y: scroll;">
        <div class="modal-dialog ">

            <!-- Modal content-->
            <div class="modal-content" style="background-color: #F6F6F6">

                <div class="mbr-overlay" style="opacity: 1;background-color: white;width: 30px;height: 30px;border-radius: 25px;margin-left: 40px;margin-top: 35px;">
                    <button type="button" class="close" style="margin-top: 1px;margin-right: 6px;" data-dismiss="modal" >&times;</button>

                </div>

                <div style="width: 500px;height: 400px;">

                    <img class="image-modal-media" src="assets/images/banku & tilapia@2x.png" style="width: 500px;height: 400px;">

                </div>

                <div class="banku-media-header" style="background-color: ##F6F6F6;width: 430px;margin-left: 29px;margin-bottom: 40px;margin-top: 40px;">

                    <h4 style="font-family: 'avertaregular';font-size: 20px;text-align: left;">
                        Banku and Tilapia
                    </h4>

                    <h4 class="amount-media-banku" style="font-family: 'avertaregular';font-size: 20px;text-align: right;margin-top: -28px;">
                        GHC 35
                    </h4>

                </div>

                <div class="media-p-mark" style="background-color: #F6F6F6;width: 400px;margin-left: 40px;">
                    <p style="font-family: 'avertaregular';color: #707070;font-size: 18px;">
                        Banku and pepper with local appamu tilapia grilled with onion, salt and garlic sauce. and pepper with local appamu tilapia grilled with onion, salt and garlic sauce. and pepper with local appamu tilapia grilled with onion, salt and garlic sauce.
                    </p>
                </div>

                <div class="extras-media" style="background-color: #F6F6F6;width: 400px;margin-left: 40px;">

                    <div style="font-family: 'avertaregular';">
                        Extras
                    </div>

                    <form style="margin-left: -11px; margin-top: 15px;">
                        <label class="container" style="font-family: 'avertaregular';margin-left: 10px;font-size: 15px;color: #58017D">Coke (+ GHC 6)
                            <input type="checkbox" checked="checked">
                            <span class="checkmark"></span>
                        </label>

                        <label class="container" style="font-family: 'avertaregular';margin-left: 10px;font-size: 15px;color: #707070">1 bottled water (+ GHC 5)
                            <input type="checkbox" >
                            <span class="checkmark"></span>
                        </label>

                        <label class="container" style="margin-left: 10px;font-size: 15px; color: #58017D">Orange juice (+ GHC 8)
                            <input type="checkbox"  checked="checked">
                            <span class="checkmark"></span>
                        </label>
                    </form>
                </div>

                <div class="fancy-media" style="background-color: #fff;width: 185px;height: 50px;margin-bottom: 40px;border-radius: 11px;margin-left: 162px;margin-top: 15px;border-style: solid;border-width: 1px;border-color: #C8C8C8;">

                    <div style=" margin-top: 15px;margin-left: 20px;">
                    <i class="fa fa-minus" aria-hidden="true" style="color: #58017D"></i>
                    </div>

                    <div style=" margin-left: 87px;margin-top: -24px;">
                    <h4 style="font-family: 'avertaregular';color: #58017D; font-size: 20px">
                        2
                    </h4>
                    </div>

                    <div style="margin-left: 151px;margin-top: -30px;">

                    <i class="fa fa-plus" aria-hidden="true" style="color: #58017D"></i>
                    </div>
                </div>


                <div class="fancy-media-button" style="background-color: #58017D;width: 235px;height: 45px;margin-bottom: 51px;margin-left: 145px;border-radius: 40px;">

                    <div style="margin-top: 11px;margin-left: 25px;">
                    <button style="font-family: 'avertaregular';background-color: #f0f8ff00;color: white;font-size: 15px;border-style: none;">
                        GHC 119
                    </button>
                    </div>

                    <div style="margin-left: 116px;margin-top: -25px;">

                    <button style="font-family: 'avertaregular';background-color: #f0f8ff00;color: white;font-size: 15px;border-style: none;">
                        Add to basket
                    </button>

                    </div>
                    
                    <div style="background-color: white;width: 2px;height: 32px;margin-left: 105px;margin-top: -28px;">
                        
                    </div>

                    
                </div>

            </div>

        </div>

    </div>

    <!-- Modal -->
    <div class="modal fade" id="modalMenuLogin" role="dialog" style="overflow-y: scroll;">
        <div class="modal-dialog modal-sm">

            <!-- Modal content-->
            <div class="modal-content media-menu-login" style="border-radius: 19px;width: 370px;">

                <div class="row" style="margin-top: 35px;margin-left: 10px;">

                    <div class="col-sm-6">
                        <h4 style="font-family: 'avertaregular';font-size: 15px;">
                    Hope Adoli
                   </h4>
                        <h4 style="font-family: 'avertaregular';font-size: 10px;">
                    0242267686
                   </h4>

                        <h4 style="font-family: 'avertaregular';font-size: 10px;">
                    kafui@loudrsocio.co
                   </h4>
                    </div>

                    <div class="col-sm-6 media-edit-button">
                        <button style="font-family: 'avertaregular';" class="menu-button-edit" data-toggle="modal" data-dismiss="modal" data-target="#modalEditProfile">
                            Edit
                        </button>
                    </div>

                </div>

                <!-- <div class="row current-balance" style="margin-top: 15px;margin-left: 25px;"> -->
                <div class="current-balance-parent-div">

                    <div class="current-balance-child1-div">
                        <h4 style="font-family: 'avertaregular';" class="current-balance-h4-1">
                    Current Balance
                   </h4>
                        <h4 style="font-family: 'avertaregular';font-size: 15px;color: black;">
                    <strong> GHC 200.46</strong>
                   </h4>

                    </div>

                    <div class="curent-balance-child2-div">
                        <button style="font-family: 'avertaregular';" class="button-top-up">
                            Top up
                        </button>
                    </div>

                </div>

                <div class="row" style="
                margin-top: 15px;
                margin-left: 10px;">

                    <div class="col-sm-6">
                        <i class="fa fa-heart-o"></i>

                    </div>

                    <div class="col-sm-6">
                        <h4 class="referFriendId" style="font-family: 'avertaregular';font-size: 15px;margin-left: -130px;">
                          Refer a friend
                             </h4>
                    </div>

                    <hr style="width: 318px;margin-left: 10px;">

                </div>

                <div class="row" style="
                margin-top: 15px;
                margin-left: 10px;">

                    <div class="col-sm-6" style="margin-top: -7px;">
                        <!-- <span class="iconify" data-icon="uil:cart" data-inline="false"></span> -->
                        <img src="assets/images/bag-09.svg" style="font-size: -16px;width: 15px;">

                    </div>

                    <div class="col-sm-3">
                        <h4 style="font-size: 15px;margin-left: -130px;" class="all-order-media">

                          <a style="color: black;font-family: 'avertaregular';" data-toggle="modal" data-dismiss="modal" data-target="#modalAllOrders">All Orders</a>
                             </h4>

                    </div>
                    <div class="col-sm-3 media-number-order" style="margin-top: -9px;">
                        <h4 style="font-family: 'avertaregular';" class="button-all-orders"> 2 </h4>
                    </div>

                    <hr style="width: 318px;margin-left: 10px;">

                </div>

                <div class="row" style="
                margin-top: 15px;
                margin-left: 10px;">

                    <div class="col-sm-6">
                        <span class="iconify" data-icon="dashicons:star-empty" data-inline="false"></span>

                    </div>

                    <div class="col-sm-6">
                        <h4 class="my-rating-media" style="font-size: 15px;margin-left: -130px;">
                            <a style="color: black;font-family: 'avertaregular';" data-toggle="modal" data-dismiss="modal" data-target="#modalMyRatingHistory">
                          My Rating & reviews
                      </a>
                             </h4>
                    </div>

                    <hr style="width: 318px;margin-left: 10px;">

                </div>

                <div class="row" style="margin-top: 15px;margin-left: 10px;margin-bottom: 15px">

                    <div class="col-sm-6">
                        <span class="iconify" data-icon="fa-regular:comment" data-inline="false"></span>

                    </div>

                    <div class="col-sm-6">
                        <h4 class="customer-support" style="font-size: 15px;margin-left: -130px;">
                          <a style="font-family: 'avertaregular';color: black" href="help-and-support.html">Customer support</a>
                             </h4>
                    </div>

                </div>

            </div>

            <div>
                <button type="button" class="close x-button-floating" style="background-color: white;" data-dismiss="modal">&times;</button>
            </div>

        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="modalRestaurantsMenu" role="dialog" style="overflow-y: scroll;">
        <div class="modal-dialog modal-sm">

            <!-- Modal content-->
            <div class="modal-content modal-restaurants-menu" style="border-radius: 19px;width: 302px;margin-top: 85px">

                <div class="row" style="
                margin-top: 30px;
                margin-left: 10px;">

                    <div class="col-sm-6">
                        <img class="icons-media" src="assets/images/Group order icon.svg">

                    </div>

                    <div class="col-sm-6 start-media-group">
                        <h4 style="font-size: 15px;margin-left: -95px;">
                            
                            <a style="font-family: 'avertaregular';color: black;" data-toggle="modal" data-dismiss="modal" data-target="#modalGroupOrder" >
                        <!-- <a style="color: black;" data-toggle="modal" data-dismiss="modal" data-target="#modalRateRestaurants" > -->
                          Start a group order
                          </a>
                             </h4>
                    </div>

                    <!-- <hr style="width: 318px;margin-left: 10px;"> -->

                </div>

                <div class="row" style="
                margin-top: 15px;
                margin-left: 10px;">

                    <div class="col-sm-6">
                        <img class="icons-media" src="assets/images/Add-to-favourites-con.svg">

                    </div>

                    <div class="col-sm-6 add-media-rest">
                        <h4 style="font-family: 'avertaregular';font-size: 15px;margin-left: -95px;">
                            
                          Add to favourite restaurants
                     
                             </h4>
                    </div>

                    <!-- <hr style="width: 318px;margin-left: 10px;"> -->

                </div>

                <div class="row" style="
                margin-top: 15px;
                margin-left: 10px;">

                    <div class="col-sm-6">
                       <img class="icons-media" src="assets/images/Ratings-icon.svg">

                    </div>

                    <div class="col-sm-6 media-rest-rev">
                        <h4 style="font-size: 15px;margin-left: -95px;">
                            <a style="font-family: 'avertaregular';color: black;" data-toggle="modal" data-dismiss="modal" data-target="#modalMyRateRestaurants">
                         Restaurant reviews
                      </a>
                             </h4>
                    </div>

                    <!-- <hr style="width: 318px;margin-left: 10px;"> -->

                </div>

                <div class="row" style="
                margin-top: 15px;
                margin-left: 10px;">

                    <div class="col-sm-6">
                        <img class="icons-media" src="assets/images/Reportrestaurant-icon.svg">

                    </div>

                    <div class="col-sm-6 media-report-rest">
                        <h4 style="font-family: 'avertaregular';font-size: 15px;margin-left: -95px;">
                            
                         Report this restaurant
                      
                             </h4>
                    </div>

                    <!-- <hr style="width: 318px;margin-left: 10px;"> -->

                </div>

                <div class="row" style="margin-top: 15px;margin-left: 10px;margin-bottom: 30px">

                    <div class="col-sm-6">
                        <img class="icons-media" src="assets/images/export.svg">

                    </div>

                    <div class="col-sm-6 media-share-rest">
                        <h4 style="font-family: 'avertaregular';font-size: 15px;margin-left: -95px;">
                          <a >Share this restaurant</a>
                             </h4>
                    </div>

                </div>

            </div>

            <div>
                <button type="button" class="close x-button-floating" style="background-color: white;margin-right: 123px;" data-dismiss="modal">&times;</button>
            </div>

        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="modalMenuNotLogin" role="dialog" style="overflow-y: scroll;">
        <div class="modal-dialog modal-sm">

            <!-- Modal content-->
            <div class="modal-content" style="border-radius: 19px;width: 350px;">

                <div class="row" style="margin-top: 35px;margin-left: 10px;">

                    <button style="font-family: 'avertaregular';color: white;background-color: #58017D;width: 90px;height: 40px;border-radius: 50px;margin-left: 100px;font-size: 12px;" data-toggle="modal" data-dismiss="modal" data-target="#modalSignUp">
                        Sign up
                    </button>

                    <hr style="width: 350px;margin-left: 10px;">

                </div>

                <div class="row" style="
                margin-top: 15px;
                margin-left: 10px;">

                    <div class="col-sm-6">
                        <i class="fa fa-heart-o"></i>

                    </div>

                    <div class="col-sm-6">
                        <h4 style="font-family: 'avertaregular';font-size: 15px;margin-left: -130px;">
                          Refer a friend
                             </h4>
                    </div>

                    <hr style="width: 350px;margin-left: 10px;">

                </div>

                <div class="row" style="
                margin-top: 15px;
                margin-left: 10px;">

                    <div class="col-sm-6">
                        <span class="iconify" data-icon="uil:cart" data-inline="false"></span>

                    </div>

                    <div class="col-sm-3">
                        <h4 style="font-family: 'avertaregular';font-size: 15px;margin-left: -130px;">
                          All Orders
                             </h4>

                    </div>
                    <div class="col-sm-3">
                        <h4 style="font-family: 'avertaregular';" class="button-all-orders"> 2 </h4>
                    </div>

                    <hr style="width: 350px;margin-left: 10px;">

                </div>

                <div class="row" style="
                margin-top: 15px;
                margin-left: 10px;">

                    <div class="col-sm-6">
                        <span class="iconify" data-icon="dashicons:star-empty" data-inline="false"></span>

                    </div>

                    <div class="col-sm-6">
                        <h4 style="font-family: 'avertaregular';font-size: 15px;margin-left: -130px;">
                          My Rating & reviews
                             </h4>
                    </div>

                    <hr style="width: 350px;margin-left: 10px;">

                </div>

                <div class="row" style="
                 margin-top: 15px;
                 margin-left: 10px;">

                    <div class="col-sm-6">
                        <span class="iconify" data-icon="fa-regular:comment" data-inline="false"></span>

                    </div>

                    <div class="col-sm-6">
                        <h4 style="font-size: 15px;margin-left: -130px;">
                          <a style="font-family: 'avertaregular';color: #000000" href="help-and-support.html">Customer support</a>
                             </h4>
                    </div>

                    <hr style="width: 350px;margin-left: 10px;">

                </div>

                <div class="row" style="
                 margin-top: 15px;
                 margin-left: 10px;padding-bottom: 20px;">

                    <div class="col-sm-6">
                        <span class="iconify" data-icon="bytesize:settings" data-inline="false"></span>

                    </div>

                    <div class="col-sm-6">
                        <h4 style="font-family: 'avertaregular';font-size: 15px;margin-left: -130px;">
                          Setting
                             </h4>
                    </div>

                    <!-- <hr style="width: 350px;margin-left: 10px;"> -->

                </div>

            </div>

            <div>
                <button type="button" class="close x-button-floating" style="background-color: white;" data-dismiss="modal">&times;</button>
            </div>

        </div>
    </div>